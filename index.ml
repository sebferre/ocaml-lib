
let marshal x = Marshal.to_string x []
let unmarshal x = Marshal.from_string x 0

class type ['a,'b] index =
  object
    method to_string : string (* marshalling *)
    method from_string : string -> unit (* unmarshalling *)
    method get : 'a -> 'b
    method set : 'a -> 'b -> unit
    method update : 'a -> ('b -> 'b) -> unit
    method reset : 'a -> unit
    method reset_all : unit
    method iter : ('a -> 'b -> unit) -> unit
    method fold : 'c . ('a -> 'b -> 'c -> 'c) -> 'c -> 'c
  end

class ['a,'b] hashtbl (size : int) (init : 'a -> 'b) : ['a,'b] index =
  object
    val mutable ht : ('a,'b) Hashtbl.t = Hashtbl.create size

    method to_string = marshal ht

    method from_string s = ht <- unmarshal s

    method get k =
      try Hashtbl.find ht k
      with Not_found ->
	let v = init k in
	Hashtbl.add ht k v;
	v

    method set k v =
      Hashtbl.replace ht k v

    method update k f =
      let old = (* inlining of get *)
	try Hashtbl.find ht k
	with Not_found -> init k in
      Hashtbl.replace ht k (f old)

    method reset k =
      Hashtbl.remove ht k

    method reset_all = Hashtbl.clear ht

    method iter f = Hashtbl.iter f ht

    method fold : 'c . ('a -> 'b -> 'c -> 'c) -> 'c -> 'c =
      fun f e -> Hashtbl.fold f ht e
  end

class ['a] vector (size : int) (null : 'a) (init : int -> 'a) : [int,'a] index =
  object
    val mutable ar : 'a array = Array.make size null

    method to_string = marshal ar

    method from_string s = ar <- unmarshal s

    method get i =
      if ar.(i) = null then ar.(i) <- init i;
      ar.(i)

    method set i v =
      ar.(i) <- v

    method update i f =
      if ar.(i) = null then ar.(i) <- init i;
      ar.(i) <- f ar.(i)

    method reset i =
      ar.(i) <- null

    method reset_all =
      for i = 0 to size - 1 do
	ar.(i) <- null
      done

    method iter f =
      for i = 0 to size - 1 do
	if ar.(i) <> null
	then f i ar.(i)
      done

    method fold : 'c . (int -> 'a -> 'c -> 'c) -> 'c -> 'c =
      fun f e ->
	let res = ref e in
	for i = 0 to size - 1 do
	  if ar.(i) <> null
	  then res := f i ar.(i) !res
	done;
	!res
  end

class ['a] vector_opt (size : int) (init : int -> 'a) : [int,'a] index =
  object
    val mutable ar : 'a option array = Array.make size None

    method to_string = marshal ar

    method from_string s = ar <- unmarshal s

    method get i =
      match ar.(i) with
      | None ->
	  let v = init i in
	  ar.(i) <- Some v;
	  v
      | Some v ->
	  v

    method set i v =
      ar.(i) <- Some v

    method update i f =
      match ar.(i) with
      | None ->
	  ar.(i) <- Some (f (init i))
      | Some v ->
	  ar.(i) <- Some (f v)

    method reset i =
      ar.(i) <- None

    method reset_all =
      for i = 0 to size - 1 do
	ar.(i) <- None
      done

    method iter f =
      for i = 0 to size - 1 do
	match ar.(i) with
	| None -> ()
	| Some v -> f i v
      done

    method fold : 'c . (int -> 'a -> 'c -> 'c) -> 'c -> 'c =
      fun f e ->
	let res = ref e in
	for i = 0 to size - 1 do
	  match ar.(i) with
	  | None -> ()
	  | Some v -> res := f i v !res
	done;
	!res
  end

class ['a] var (null : 'a) (init : unit -> 'a) : [unit,'a] index =
  object
    val mutable x : 'a = null

    method to_string = marshal x

    method from_string s = x <- unmarshal s

    method get () =
      if x = null then x <- init ();
      x

    method set () v = x <- v

    method update () f = x <- f x

    method reset () = x <- null

    method reset_all = x <- null

    method iter f = if x <> null then f () x

    method fold : 'c . (unit -> 'a -> 'c -> 'c) -> 'c -> 'c =
      fun f e -> if x <> null then f () x e else e
  end

class ['a] var_opt (init : unit -> 'a) : [unit,'a] index =
  object
    val mutable x : 'a option = None

    method to_string = marshal x

    method from_string s = x <- unmarshal s

    method get () =
      match x with
      | None ->
	  let v = init () in
	  x <- Some v;
	  v
      | Some v -> v

    method set () v = x <- Some v

    method update () f =
      match x with
      | None ->
	  x <- Some (f (init ()))
      | Some v ->
	  x <- Some (f v)

    method reset () = x <- None

    method reset_all = x <- None

    method iter f =
      match x with
      | None -> ()
      | Some v -> f () v

    method fold : 'c . (unit -> 'a -> 'c -> 'c) -> 'c -> 'c =
      fun f e ->
	match x with
	| None -> e
	| Some v -> f () v e
  end

type 'a va = VarCons of 'a va * int * 'a array | VarNil

class ['a] varray (size : int) (null : 'a) (init : int -> 'a) : [int,'a] index =
  object (self)
    val mutable var : 'a va = VarCons (VarNil, 0, Array.make size null)

    method to_string = marshal var

    method from_string s = var <- unmarshal s

    method get i =
      let rec aux = function
	| VarCons (l,s,r) as var1 ->
	    if i >= s
	    then
	      let lr = Array.length r in
	      let i' = i - s in
	      if i' < lr
	      then begin
		if r.(i') = null then r.(i') <- init i;
		r.(i') end
	      else begin
		assert (var1 == var);  (* we know that the current VarCons is var because i is out of bounds *)
		let v = init i in
		let s' = s + lr in
		let lr' = max s' (i - s' + 1) in
		let r' = Array.make lr' null in
		r'.(i - s') <- v;
		var <- VarCons (var, s', r');
		v end
	    else aux l
	| VarNil ->
	    raise (Invalid_argument "negative index") in
      aux var

    method set i v =
      let rec aux = function
	| VarCons (l,s,r) ->
	    if i >= s
	    then
	      let lr = Array.length r in
	      let i' = i - s in
	      if i' < lr
	      then
		r.(i') <- v
	      else begin
		let s' = s + lr in
		let lr' = max s' (i - s' + 1) in
		let r' = Array.make lr' null in
		r'.(i - s') <- v;
		var <- VarCons (var, s', r')
	      end
	    else aux l
	| VarNil ->
	    raise (Invalid_argument "negative index") in
      aux var

    method update i f =
      self # set i (f (self # get i))

    method reset i =
      let rec aux = function
	| VarCons (l,s,r) ->
	    if i >= s
	    then
	      let i' = i - s in
	      if i' < Array.length r
	      then r.(i') <- null
	      else ()
	    else aux l
	| VarNil ->
	    raise (Invalid_argument "negative index") in
      aux var

    method reset_all =
      let rec aux = function
	| VarCons (l,s,r) ->
	    for i' = 0 to Array.length r - 1 do
	      r.(i') <- null
	    done;
	    aux l
	| VarNil -> () in
      aux var

    method iter f =
      let rec aux = function
	| VarCons (l,s,r) ->
	    for i' = Array.length r - 1 downto 0 do
	      if r.(i') <> null
	      then f (s + i') r.(i')
	    done;
	    aux l
	| VarNil -> () in
      aux var

    method fold : 'c . (int -> 'a -> 'c -> 'c) -> 'c -> 'c =
      fun f e ->
	let rec aux acc = function
	  | VarCons (l,s,r) ->
	      let res = ref acc in
	      for i' = Array.length r - 1 downto 0 do
		if r.(i') <> null
		then res := f (s + i') r.(i') !res
	      done;
	      aux !res l
	  | VarNil -> acc in
	aux e var
  end

class ['a] varray_opt (size : int) (init : int -> 'a) : [int,'a] index =
  object (self)
    val mutable var : 'a option va = VarCons (VarNil, 0, Array.make size None)

    method to_string = marshal var

    method from_string s = var <- unmarshal s

    method get i =
      let rec aux = function
	| VarCons (l,s,r) as var1 ->
	    if i >= s
	    then
	      let lr = Array.length r in
	      let i' = i - s in
	      if i' < lr
	      then
		match r.(i') with
		| None ->
		    let v = init i in
		    r.(i') <- Some v;
		    v
		| Some v -> v
	      else begin
		assert (var1 == var);  (* we know that the current VarCons is var because i is out of bounds *)
		let v = init i in
		let s' = s + lr in
		let lr' = max s' (i - s' + 1) in
		let r' = Array.make lr' None in
		r'.(i - s') <- Some v;
		var <- VarCons (var, s', r');
		v end
	    else aux l
	| VarNil ->
	    raise (Invalid_argument "negative index") in
      aux var

    method set i v =
      let rec aux = function
	| VarCons (l,s,r) ->
	    if i >= s
	    then
	      let lr = Array.length r in
	      let i' = i - s in
	      if i' < lr
	      then
		r.(i') <- Some v
	      else begin
		let s' = s + lr in
		let lr' = max s' (i - s' + 1) in
		let r' = Array.make lr' None in
		r'.(i - s') <- Some v;
		var <- VarCons (var, s', r')
	      end
	    else aux l
	| VarNil ->
	    raise (Invalid_argument "negative index") in
      aux var

    method update i f =
      self # set i (f (self # get i))

    method reset i =
      let rec aux = function
	| VarCons (l,s,r) ->
	    if i >= s
	    then
	      let i' = i - s in
	      if i' < Array.length r
	      then r.(i') <- None
	      else ()
	    else aux l
	| VarNil ->
	    raise (Invalid_argument "negative index") in
      aux var

    method reset_all =
      let rec aux = function
	| VarCons (l,s,r) ->
	    for i' = 0 to Array.length r - 1 do
	      r.(i') <- None
	    done;
	    aux l
	| VarNil -> () in
      aux var

    method iter f =
      let rec aux = function
	| VarCons (l,s,r) ->
	    for i' = Array.length r - 1 downto 0 do
	      match r.(i') with
	      | None -> ()
	      | Some v -> f (s + i') v
	    done;
	    aux l
	| VarNil -> () in
      aux var

    method fold : 'c . (int -> 'a -> 'c -> 'c) -> 'c -> 'c =
      fun f e ->
	let rec aux acc = function
	  | VarCons (l,s,r) ->
	      let res = ref acc in
	      for i' = Array.length r - 1 downto 0 do
		match r.(i') with
		| None -> ()
		| Some v -> res := f (s + i') v !res
	      done;
	      aux !res l
	  | VarNil -> acc in
	aux e var
  end
