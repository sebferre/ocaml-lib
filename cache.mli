(** Access to a Dbm-like database via a cache.

    Arbitrary types can be used as keys and data at the cache level, while only strings can be used at the level of the database.
    Several caches can be created over a same database, and a different key prefix can be specified for each cache
    in order to avoid confusion.

    By default, all loaded data is kept in the cache, i.e. in memory. The function [unget] can be used to 
    withdraw data from memory back to the database. The database is not synchronized with the cache
    except upon explicit request, call to [unget], and program termination.
*)

(** Type of the useful interface to a database of type ['t]. *)
type 't db_mod = {
    find : 't -> string -> string;
    (** [find db key] retrieves the data associated to [key] in the database [db]. *)
    replace : 't -> string -> string -> unit;
    (** [replace db key data] replaces the data associated to [key] by [data] in the database [db].
        If the key does not exist yet, it is created. *)
    remove : 't -> string -> unit
    (** [remove db key] removes the entry [key] from the database [db]. *)
  }
(** Type of cache over a database of type ['t]. The keys have type ['a], and the data have type ['b]. *)
type ('t,'a,'b) t

val sync : ('t,'a,'b) t -> unit
(** [sync ch] forces the synchronization of the database with the cache [ch]. *)
val create : 't db_mod -> 't -> string -> int -> ('t,'a,'b) t
(** [create db_mod db keyprefix size] creates a new cache over the opened database [db], to which [db_mod] provides access.
    [keyprefix] is used as a prefix to all keys of the cache in the database.
    [size] is an estimate of the number of keys (this will expand as necessary). *) 
val mem : ('t,'a,'b) t -> 'a -> bool
(** [mem ch key] tests whether the key [key] is present in the cache. *)
val get : ('t,'a,'b) t -> 'a -> 'b
(** [get ch key] returns the data associated to the key [key] by first looking in the cache [ch], 
    and then in the database if necessary. In the latter case, this data is kept in the cache. *)
val unget : ('t,'a,'b) t -> 'a -> unit
(** [unget ch key] removes the key [key] and its data from the cache, if it is present. *)
val fold : ('a -> 'b -> 'c -> 'c) -> ('t,'a,'b) t -> 'c -> 'c
(** [fold f ch e] is a regular folding over the keys and data present in the cache.
    This enables the implementatin of different cache unload strategy. *)
val add : ('t,'a,'b) t -> 'a -> 'b -> unit
(** [add ch key data] adds a new pair [(key,data)] in the cache.
    If there is already one, it is overwritten.
    If it is not in the database, it will be added to it at the next synchronization. *)
val put : ('t,'a,'b) t -> 'a -> unit
(** [put ch key] forces the synchronization of the data associated to the key [key] in the database. *)
val remove : ('t,'a,'b) t -> 'a -> unit
(** [remove ch key] removes the entry [key] in both the cache and the database. *)
