(* Polymorphic printing - same code for print_x and string_of_x functions *)

class virtual t = (* class type of printers *)
  object (self)
    method virtual string : string -> unit
    method virtual int : int -> unit
    method virtual float : float -> unit
    method virtual float_1 : float -> unit (* float with precision 1 *)
    method virtual float_2 : float -> unit (* ... precision 2 *)
    method virtual float_3 : float -> unit (* ... precision 3 *)
  end

type 'a xp = t -> 'a -> unit (* type of polymorphic printings *)
        
let pr_stdout = (* printer to stdout *)
  object
    inherit t
    method string s = print_string s
    method int i = print_int i
    method float f = print_float f
    method float_1 f = Printf.printf "%.1f" f
    method float_2 f = Printf.printf "%.2f" f
    method float_3 f = Printf.printf "%.3f" f
  end

(* stdout-printing from polymorphic printing *)
let to_stdout (xp : 'a xp) : 'a -> unit =
  fun x ->
  xp pr_stdout x

class pr_buffer = (* printer to buffer, to get a string *)
  object
    val buf = Buffer.create 1003
    method contents = Buffer.contents buf
                    
    inherit t
    method string s = Buffer.add_string buf s
    method int i = Buffer.add_string buf (string_of_int i)
    method float f = Buffer.add_string buf (string_of_float f)
    method float_1 f = Buffer.add_string buf (Printf.sprintf "%.1f" f)
    method float_2 f = Buffer.add_string buf (Printf.sprintf "%.2f" f)
    method float_3 f = Buffer.add_string buf (Printf.sprintf "%.3f" f)
  end

(* string-printing from a polymorphic printing *)
(* LIMIT: single argument for xp *)
let to_string (xp : 'a xp) : 'a -> string =
  fun x ->
  let pr = new pr_buffer in
  xp (pr :> t) x;
  pr#contents

  
(* combinators for polymorphic printings *)
  
let sep_list (sep : string) (xp : 'a xp) : 'a list xp =
  fun pr l ->
  match l with
  | [] -> ()
  | [x] -> xp pr x
  | x::ys ->
     xp pr x;
     ys |> List.iter (fun y -> pr#string sep; xp pr y)

let sep_array (sep : string) (xp : 'a xp) : 'a array xp =
  fun pr ar ->
  Array.iteri
    (fun i x ->
      if i > 0 then pr#string sep;
      xp pr x)
    ar

let bracket (left, right : string * string) (xp : 'a xp) : 'a xp =
  fun pr x ->
  pr#string left;
  xp pr x;
  pr#string right

let infix (op : string) (xp : 'a xp) : ('a * 'a) xp =
  fun pr (x1,x2) ->
  xp pr x1;
  pr#string op;
  xp pr x2
