
type typ =
  | V (* void *)
  | I (* int *)
  | D (* double *)
  | Z (* boolean *)
  | L of string (* class path *)
  | Array of typ (* array *)

let rec string_of_typ = function
  | V -> "V"
  | I -> "I"
  | D -> "D"
  | Z -> "Z"
  | L path -> "L" ^ path ^ ";"
  | Array t -> "[" ^ string_of_typ t

let signature (args : typ list) (res : typ) : string =
  "(" ^ String.concat "" (List.map string_of_typ args) ^ ")" ^ string_of_typ res

module JString =
  struct
    let path = "java/lang/String"

    class obj (this : Jni.obj) =
      object
	method to_string = Common.prof "Owlapi.JString.obj#to_string" (fun () ->
	  Jni.string_from_java this)
      end

    let create (s : string) = Common.prof "Owlapi.JString.create" (fun () ->
      new obj (Jni.string_to_java s))
  end

module JObject =
  struct
    let path = "java/lang/Object"
    let c = lazy (
      try Jni.find_class path
      with _ -> failwith "JNI: class Object could not be found")
    let m_toString = lazy (
      try Jni.get_methodID (Lazy.force c)
	    "toString" (signature [] (L JString.path))
      with _ -> failwith "JNI: method Object.toString could not be found")

    class obj (this : Jni.obj) =
      object
	method jni : Jni.obj = this

	method to_string : string = Common.prof "JObject.obj#to_string" (fun () ->
	  Jni.string_from_java
	    (try Jni.call_object_method this (Lazy.force m_toString) [||]
	    with _ -> failwith "JObject#to_string: unknown error"))
      end

    let is_same_object o1 o2 = Common.prof "JObject.is_same_object" (fun () ->
      Jni.is_same_object o1#jni o2#jni)
  end

let debug f =
  try f ()
  with Jni.Exception o ->
    failwith (new JObject.obj o)#to_string

type clazz = Jni.clazz Lazy.t
type methodID = Jni.methodID Lazy.t

let find_class path : clazz = lazy (debug (fun () -> Jni.find_class path))
let get_methodID c name s : methodID = lazy (debug (fun () -> Jni.get_methodID (Lazy.force c) name s))
let get_static_methodID c name s : methodID = lazy (debug (fun () -> Jni.get_static_methodID (Lazy.force c) name s))
let call_static_object_method c m args = debug (fun () -> Jni.call_static_object_method (Lazy.force c) (Lazy.force m) args)
let alloc_object c = debug (fun () -> Jni.alloc_object (Lazy.force c))
let call_nonvirtual_void_method this c m_init args = debug (fun () -> Jni.call_nonvirtual_void_method this (Lazy.force c) (Lazy.force m_init) args)
let call_object_method this m args = debug (fun () -> Jni.call_object_method this (Lazy.force m) args)
let call_void_method this m args = debug (fun () -> Jni.call_void_method this (Lazy.force m) args)
let call_boolean_method this m args = debug (fun () -> Jni.call_boolean_method this (Lazy.force m) args)
let call_camlint_method this m args = debug (fun () -> Jni.call_camlint_method this (Lazy.force m) args)
let call_double_method this m args = debug (fun () -> Jni.call_double_method this (Lazy.force m) args)

