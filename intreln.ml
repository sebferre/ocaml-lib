
module Intset = Intset.Intmap
module Intrel2 = Intrel2.Intmap

module Mask =
  struct
    type t = bool list
    let dim bs = List.length bs
    let size bs = List.fold_left (fun res b -> if b then res + 1 else res) 0 bs
    let rec is_prefix = function
      | [] -> true
      | true::bs -> is_prefix bs
      | false::bs -> not (List.exists (fun b -> b) bs)
  end

module type T =
  sig
    type t
    exception Invalid_dimension of string
    val of_intset : Intset.t -> t
    val of_intrel2 : Intrel2.t -> t
    val to_intset : t -> Intset.t (* raises Invalid_dimension *)
    val to_intrel2 : t -> Intrel2.t (* raises Invalid_dimension *)
    val dim : t -> int
    val empty : int -> t
    val is_empty : t -> bool
    val cardinal : t -> int
    val mem : int list -> t -> bool
    val matches : int option list -> t -> bool
    val singleton : int list -> t
    val add : int list -> t -> t
    val remove : int list -> t -> t
    val subset : t -> t -> bool
    val union : t -> t -> t
    val inter : t -> t -> t
    val diff : t -> t -> t
    val union_r : t list -> t
    val inter_r : t list -> t
    val fold : ('a -> int list -> 'a) -> 'a -> t -> 'a
    val filter : (int list -> bool) -> t -> t
    val fold_mu : (string * int) list -> string list -> ('a -> (string * int) list -> 'a) -> 'a -> t -> 'a
    val fold_restr : (string * int) list -> string list -> ('a -> (string * int) list -> 'a) -> 'a -> t -> 'a
    val filter_restr : (string * int) list -> string list -> ((string * int) list -> bool) -> t -> t
    val iter : (int list -> unit) -> t -> unit
    val to_seq : ?restr:int option list -> t -> int list Myseq.t

    (* functions for accessing a relation as an association/map, for rels with dim>0 *)
    val mem_assoc : int -> t -> bool
    val assoc : int -> t -> t
    val add_assoc : int -> t (* n-1 *) -> t (* n *) -> t (* n *)
    val keys : t -> Intset.t
    val fold_assoc : ('a -> int -> t -> 'a) -> 'a -> t -> 'a
    val map_assoc : (int -> t (* n-1 *) -> t option (* n-1 *)) -> t (* n *) -> t (* n *)
    val inter_assoc : Intset.t -> t -> t

    val domains : t -> Intset.t list
    val project : Mask.t -> t -> t
    val apply : int option list -> t -> t (* filter on columns with defined int-values, and project on other columns: a kind of function application *)
    val group_by : Mask.t -> t -> t
    val restriction : int -> ?filter:(int list -> t -> bool) -> t -> t
	(* restriction k (int^(n-k) -> t_k -> bool) t_n -> t_(n-k) *)
    val extension : int -> (int list -> t) -> t -> t
	(* extension k (int^n -> t_k) -> t_n -> t_(n+k) *)

    val memory_size : t -> int
  end

module Intmap : T =
  struct
    module M = Intmap.M

    type t = {dim : int; data : Obj.t}

    exception Invalid_dimension of string
	
    (* utilities *)

    let intmap d = (Obj.obj d : Obj.t M.t)
    let data m = Obj.repr m

    type obj = R0 of bool | R1 of unit M.t | Rn of int * Obj.t M.t
	
    let repr = function
      | R0 b -> if b then Obj.repr () else Obj.repr M.empty
      | R1 s -> Obj.repr s
      | Rn (_,m) -> Obj.repr m
	    
    let obj n d =
      if n = 0 then R0 (d = Obj.repr ())
      else if n = 1 then R1 (Obj.obj d : unit M.t)
      else Rn (n, (Obj.obj d : Obj.t M.t))
	  
    let split xs = List.hd xs, List.tl xs

    (* public interface *)

    let of_intset set = {dim = 1; data = Obj.repr set}
      
    let of_intrel2 rel2 = {dim = 2; data = Obj.repr rel2}

    let to_intset rel =
      if rel.dim <> 1 then raise (Invalid_dimension "to_intset");
      (Obj.obj rel.data : Intset.t)
    let to_intrel2 rel =
      if rel.dim <> 2 then raise (Invalid_dimension "to_intrel2");
      (Obj.obj rel.data : Intrel2.t)
 
    let dim r = r.dim
	
    let rec empty n =
      {dim = n; data = repr (empty_obj n)}
    and empty_obj n =
      if n = 0 then R0 false
      else if n = 1 then R1 M.empty
      else Rn (n,M.empty)
	  
    let rec is_empty r =
      is_empty_obj (obj r.dim r.data) (*r.data = Obj.repr M.empty*)
    and is_empty_obj = function
      | R0 b -> not b
      | R1 s -> M.is_empty s
      | Rn (n,m) -> M.is_empty m
	
    let rec cardinal r = Common.prof "Intreln.cardinal" (fun () ->
      cardinal_obj (obj r.dim r.data))
    and cardinal_obj = function
      | R0 b -> if b then 1 else 0
      | R1 s -> M.cardinal s
      | Rn (n, m) -> M.fold (fun res x d1 -> res + cardinal_obj (obj (n-1) d1)) 0 m
	    
    let rec mem xs r = (* QUICK *)
      if List.length xs = r.dim
      then mem_obj xs (obj r.dim r.data)
      else raise (Invalid_dimension "mem")
    and mem_obj xs = function
      | R0 b -> b
      | R1 s -> M.mem (List.hd xs) s
      | Rn (n, m) ->
	  let x, xs1 = split xs in
	  try mem_obj xs1 (obj (n-1) (M.get x m))
	  with Not_found -> false
			      
    let rec matches xos r = Common.prof "Intreln.matches" (fun () ->
      matches_obj xos (obj r.dim r.data))
    and matches_obj xos o =
	match xos with
	  | [] -> true
	  | None::xos1 ->
	    ( match o with
	      | R0 b -> b
	      | R1 s -> not (M.is_empty s)
	      | Rn (n, m) ->
		let n1 = n-1 in
		M.fold
		  (fun res x1 m1 -> res || matches_obj xos1 (obj n1 m1))
		  false
		  m )
	  | Some x::xos1 ->
	    ( match o with
	      | R0 b -> b
	      | R1 s -> M.mem x s
	      | Rn (n, m) ->
		try matches_obj xos1 (obj (n-1) (M.get x m))
		with Not_found -> false )

    let rec singleton xs = Common.prof "Intreln.singleton" (fun () ->
      let n = List.length xs in
      {dim = n; data = repr (singleton_obj n xs)})
    and singleton_obj n = function
      | [] -> R0 true
      | [x] -> R1 (M.singleton x)
      | x::xs1 -> Rn (n, M.set x (repr (singleton_obj (n-1) xs1)) M.empty)
	      
    let rec add xs r = (* QUICK *)
      if List.length xs = r.dim
      then {r with data = repr (add_obj xs (obj r.dim r.data))}
      else raise (Invalid_dimension "add")
    and add_obj xs = function
      | R0 _ -> R0 true
      | R1 s -> R1 (M.add (List.hd xs) s)
      | Rn (n,m) ->
	  let n1 = n-1 in
	  let x, xs1 = split xs in
	  let d1 = try obj n1 (M.get x m) with Not_found -> empty_obj n1 in
	  Rn (n, M.set x (repr (add_obj xs1 d1)) m)

    let rec remove xs r = Common.prof "Intreln.remove" (fun () ->
      if List.length xs = r.dim
      then {r with data = repr (remove_obj xs (obj r.dim r.data))}
      else raise (Invalid_dimension "remove"))
    and remove_obj xs = function
      | R0 _ -> R0 false
      | R1 s -> R1 (M.remove (List.hd xs) s)
      | Rn (n,m) ->
	  let x, xs1 = split xs in
	  try
	    let d1' = remove_obj xs1 (obj (n-1) (M.get x m)) in
	    if is_empty_obj d1'
	    then Rn (n, M.remove x m)
	    else Rn (n, M.set x (repr d1') m)
	  with Not_found ->
	    Rn (n,m)

    let rec subset r1 r2 = Common.prof "Intreln.subset" (fun () ->
      if r1.dim = r2.dim
      then subset_obj (obj r1.dim r1.data) (obj r2.dim r2.data)
      else raise (Invalid_dimension "subset"))
    and subset_obj d1 d2 =
      match d1, d2 with
      | R0 b1, R0 b2 -> not b1 || b2
      | R1 s1, R1 s2 -> M.subset s1 s2
      | Rn (n,m1), Rn (_,m2) ->
	 M.fold
	   (fun b x1 d1 ->
	    b &&
	      (try
		  let d2 = M.get x1 m2 in
		  subset_obj (obj n d1) (obj n d2)
		with Not_found -> false))
	   true m1
      | _ -> assert false

    let rec union r1 r2 = Common.prof "Intreln.union" (fun () ->
      if r1.dim = r2.dim
      then {dim = r1.dim; data = repr (union_obj (obj r1.dim r1.data) (obj r2.dim r2.data))}
      else raise (Invalid_dimension "union"))
    and union_obj d1 d2 =
      match d1, d2 with
      | R0 b1, R0 b2 -> R0 (b1 || b2)
      | R1 s1, R1 s2 -> R1 (M.domain_union s1 s2)
      | Rn (n,m1), Rn (_,m2) ->
	  let n1 = n-1 in
	  Rn (n,
	      M.map_union
		(fun x d1_opt d2_opt ->
		  match d1_opt, d2_opt with
		  | None, None -> None
		  | Some _, None -> d1_opt
		  | None, Some _ -> d2_opt
		  | Some d1, Some d2 -> Some (repr (union_obj (obj n1 d1) (obj n1 d2))))
		m1 m2)
      | _, _ -> assert false

    let rec inter r1 r2 = Common.prof "Intreln.inter" (fun () ->
      if r1.dim = r2.dim
      then {dim = r1.dim; data = repr (inter_obj (obj r1.dim r1.data) (obj r2.dim r2.data))}
      else raise (Invalid_dimension "inter"))
    and inter_obj d1 d2 =
      match d1, d2 with
      | R0 b1, R0 b2 -> R0 (b1 && b2)
      | R1 s1, R1 s2 -> R1 (M.domain_inter s1 s2)
      | Rn (n,m1), Rn (_,m2) ->
	  let n1 = n-1 in
	  Rn (n,
	      M.map_inter
		(fun x d1 d2 ->
		  let d = inter_obj (obj n1 d1) (obj n1 d2) in
		  if is_empty_obj d
		  then None
		  else Some (repr d))
		m1 m2)
      | _, _ -> assert false

    let rec diff r1 r2 = Common.prof "Intreln.diff" (fun () ->
      if r1.dim = r2.dim
      then {dim = r1.dim; data = repr (diff_obj (obj r1.dim r1.data) (obj r2.dim r2.data))}
      else raise (Invalid_dimension "diff"))
    and diff_obj d1 d2 =
      match d1, d2 with
      | R0 b1, R0 b2 -> R0 (b1 && not b2)
      | R1 s1, R1 s2 -> R1 (M.domain_diff s1 s2)
      | Rn (n,m1), Rn (_,m2) ->
	  let n1 = n-1 in
	  Rn (n,
	      M.map_diff
		(fun x d1 d2_opt ->
		  match d2_opt with
		  | None -> Some d1
		  | Some d2 ->
		      let d = diff_obj (obj n1 d1) (obj n1 d2) in
		      if is_empty_obj d
		      then None
		      else Some (repr d))
		m1 m2)
      | _, _ -> assert false

    let union_r = function
      | [] -> invalid_arg "Intreln.Intmap.union_r: empty list of relations"
      | r::rs -> List.fold_left union r rs

    let inter_r = function
      | [] -> invalid_arg "Intreln.Intmap.inter_r : empty list of relations"
      | r::rs -> List.fold_left inter r rs

    let rec fold f init r = Common.prof "Intreln.fold" (fun () ->
      fold_obj f init [] (obj r.dim r.data))
    and fold_obj f acc xs = function
      | R0 b -> if b then f acc [] else acc
      | R1 s -> M.fold (fun res x _ -> f res (List.rev (x::xs))) acc s
      | Rn (n,m) ->
	  let n1 = n-1 in
	  M.fold
	    (fun res x d1 ->
	      fold_obj f res (x::xs) (obj n1 d1))
	    acc m

    let rec filter f r = Common.prof "Intreln.filter" (fun () ->
      (*Printf.printf "filter: dim=%d  card=%d\n" r.dim (cardinal r); flush stdout;*)
      {dim = r.dim; data = repr (filter_obj f [] (obj r.dim r.data))})
    and filter_obj f rev_xs = function
      | R0 b -> R0 (b && f (List.rev rev_xs))
      | R1 s ->
	 R1 (M.domain
	       ~filter:(fun x _ -> f (List.rev (x::rev_xs)))
	       s)
      | Rn (n,m) ->
	 let n1 = n-1 in
	 Rn (n,
	     M.map
	       (fun x d1 ->
		let d1' = filter_obj f (x::rev_xs) (obj n1 d1) in
		if is_empty_obj d1'
		then None
		else Some (repr d1'))
	       m)

    let rec fold_mu mu vs f init r = Common.prof "Intreln.fold_mu" (fun () ->
         (* "" variables must be ignored in generated mappings *)
      let rec aux = function ""::l -> aux l | l -> l in
      let vs_prefix = List.rev (aux (List.rev vs)) in
      fold_mu_obj f init mu vs_prefix (obj r.dim r.data))
    and fold_mu_obj f acc mu vs = function
      | R0 b ->
	  if b then f acc mu else acc
      | R1 s ->
	  ( match vs with
	  | [] -> f acc mu
	  | v::_ ->
	      (try
		let x = List.assoc v mu in
		if M.mem x s
		then f acc mu
		else acc
	      with Not_found ->
		M.fold
		  (fun res x _ ->
		    let mu' = if v="" then mu else (v,x)::mu in
		    f res mu')
		  acc s))
      | Rn (n,m) ->
	  let n1 = n-1 in
	  ( match vs with
	  | [] ->
	      if M.is_empty m
	      then acc
	      else f acc mu
	  | v::vs1 ->
	      (try
		let x = List.assoc v mu in
		try
		  let d1 = M.get x m in
		  fold_mu_obj f acc mu vs1 (obj n1 d1)
		with Not_found ->
		  acc
	      with Not_found ->
		M.fold
		  (fun res x d1 ->
		    let mu' = if v="" then mu else (v,x)::mu in
		    fold_mu_obj f res mu' vs1 (obj n1 d1))
		  acc m))

    let rec project ps r = Common.prof "Intreln.project" (fun () ->
      if Mask.dim ps = r.dim
      then
	let size = Mask.size ps in
	if size = r.dim
	then r
	else {dim = size;
	      data = repr (project_obj size ps (obj r.dim r.data))}
      else raise (Invalid_dimension "project"))
    and project_obj size ps = function
      | R0 b -> R0 b
      | R1 s ->
	  if List.hd ps
	  then R1 s
	  else R0 (not (M.is_empty s))
      | Rn (n,m) ->
	 if size = n (* no more projection *)
	 then Rn (n,m)
	 else
	  let n1 = n-1 in
	  let p, ps1 = split ps in
	  if p
	  then 
	    if size = 1
	    then R1 (M.domain m)
	    else Rn (size, M.mapx (fun d1 -> repr (project_obj (size-1) ps1 (obj n1 d1))) m)
	  else 
	    if size = 0
	    then R0 (not (M.is_empty m))
	    else
	      M.fold
		(fun ores _x d1 ->
		 project_obj_fold ps1 ores (obj n1 d1))
		(empty_obj size) m
    and project_obj_fold ps ores o =
      match ores, o with (* ores not larger than o *)
      | R0 bres, R0 b -> R0 (bres || b)
      | R0 bres, R1 s -> R0 (bres || not (M.is_empty s))
      | R1 sres, R1 s -> R1 (M.domain_union sres s)
      | R0 bres, Rn (n,m) -> R0 (bres || not (M.is_empty m))
      | R1 sres, Rn (n,m) ->
	 let p, ps1 = split ps in
	 if p
	 then R1 (M.domain_union sres m)
	 else
	   let n1 = n-1 in
	   M.fold
	     (fun ores x d1 -> project_obj_fold ps1 ores (obj n1 d1))
	     ores m
      | Rn (nres,mres), Rn (n,m) ->
	 let p, ps1 = split ps in
	 if p
	 then
	   let nres1 = nres-1 in
	   let n1 = n-1 in
	   let mres =
	     M.fold
	       (fun mres x d1 ->
		let ores1 = try obj nres1 (M.get x mres) with _ -> empty_obj nres1 in
		let ores1 = project_obj_fold ps1 ores1 (obj n1 d1) in
		M.set x (repr ores1) mres)
	       mres m in
	   Rn (nres, mres)
	 else
	   let n1 = n-1 in
	   M.fold
	     (fun ores _x d1 -> project_obj_fold ps1 ores (obj n1 d1))
	     ores m
      | _ -> assert false

    let rec apply (args : int option list) r = Common.prof "Intreln.apply" (fun () ->
      if List.length args = r.dim
      then
	let dim_res = Common.list_count (fun arg -> arg=None) args in
	if dim_res = r.dim
	then r
	else
	  match apply_obj args dim_res (obj r.dim r.data) with
	  | Some d -> {dim = dim_res; data = d}
	  | None -> empty dim_res
      else raise (Invalid_dimension "apply"))
    and apply_obj args dim_res o =
      match o with
      | R0 b -> Some (repr o)
      | R1 s ->
	( match args with
	| None::_ -> Some (repr o)
	| Some arg::_ -> if M.mem arg s then Some (repr (R0 true)) else None
	| _ -> assert false)
      | Rn (n,m) ->
	if n = dim_res (* only None-args from there *)
	then Some (repr o)
	else
	  let n1 = n-1 in
	  let arg_opt, args1 = split args in
	  ( match arg_opt with
	  | None -> (* result column *)
	    Some (repr (Rn (dim_res,
			    M.map
			      (fun x d1 -> apply_obj args1 (dim_res-1) (obj n1 d1))
			      m)))
	  | Some arg -> (* arg column *)
	    (try
	       let d1 = M.get arg m in
	       apply_obj args1 dim_res (obj n1 d1)
	     with Not_found -> None) )


    let rec fold_restr mu vs f init r = Common.prof "Intreln.fold_restr" (fun () ->
         (* "" variables must be ignored in generated mappings *)
      let r' = project (List.map ((<>) "") vs) r in (* done to avoid doublons when folding *)
      let vs' = List.filter ((<>) "") vs in
      fold_restr_obj f init mu vs' (obj r'.dim r'.data))
    and fold_restr_obj f acc mu vs = function
      | R0 b -> if b then f acc mu else acc
      | R1 s ->
	  let v = List.hd vs in
	  (try
	    let x = List.assoc v mu in
	    if M.mem x s
	    then f acc mu
	    else acc
	  with Not_found ->
	    M.fold (fun res x _ -> f res ((v,x)::mu)) acc s)
      | Rn (n,m) ->
	  let n1 = n-1 in
	  let v = List.hd vs in
	  (try
	    let x = List.assoc v mu in
	    try
	      let d1 = M.get x m in
	      fold_restr_obj f acc mu (List.tl vs) (obj n1 d1)
	    with Not_found ->
	      acc
	  with Not_found ->
	    M.fold
	      (fun res x d1 ->
		fold_restr_obj f res ((v,x)::mu) (List.tl vs) (obj n1 d1))
	      acc m)

    let rec filter_restr mu vs f r = Common.prof "Intreln.filter_restr" (fun () ->
         (* "" variables must be ignored in generated mappings *)
      let r' = project (List.map ((<>) "") vs) r in (* done to avoid doublons when folding *)
      let vs' = List.filter ((<>) "") vs in
      {dim = List.length vs'; data = repr (filter_restr_obj f mu vs' (obj r'.dim r'.data))})
    and filter_restr_obj f mu vs = function
      | R0 b -> R0 (b && f mu)
      | R1 s ->
	  let v = List.hd vs in
	  (try
	    let x = List.assoc v mu in
	    if M.mem x s && f mu
	    then R1 s
	    else R1 M.empty
	  with Not_found ->
	    R1 (M.domain ~filter:(fun x _ -> f ((v,x)::mu)) s))
      | Rn (n,m) ->
	  let n1 = n-1 in
	  let v = List.hd vs in
	  (try
	    let x = List.assoc v mu in
	    try
	      let d1 = M.get x m in
	      let d1' = filter_restr_obj f mu (List.tl vs) (obj n1 d1) in
	      if is_empty_obj d1'
	      then Rn (n, M.remove x m)
	      else Rn (n, M.set x (repr d1') m)
	    with Not_found ->
	      Rn (n, m)
	  with Not_found ->
	    Rn (n,
		M.map
		  (fun x d1 ->
		    let d1' = filter_restr_obj f ((v,x)::mu) (List.tl vs) (obj n1 d1) in
		    if is_empty_obj d1'
		    then None
		    else Some (repr d1'))
		  m))

    let rec iter f r = Common.prof "Intreln.iter" (fun () ->
      iter_obj f [] (obj r.dim r.data))
    and iter_obj f xs = function
      | R0 b -> if b then f [] else ()
      | R1 s -> M.iter (fun x _ -> f (List.rev (x::xs))) s
      | Rn (n,m) ->
	  let n1 = n-1 in
	  M.iter
	    (fun x d1 ->
	      iter_obj f (x::xs) (obj n1 d1))
	    m

    let rec to_seq ?(restr : int option list = []) r = Common.prof "Intreln.to_seq" (fun () ->
      to_seq_obj [] (restr, obj r.dim r.data))
    and to_seq_obj xs = function
      | [], R0 b ->
	 if b then Myseq.return (List.rev xs) else Myseq.empty
      | [None], R1 s ->
	 M.to_seq s |> Myseq.map (fun (x,_) -> List.rev (x::xs))
      | [Some x], R1 s ->
	 if M.mem x s then Myseq.return (List.rev (x::xs)) else Myseq.empty
      | None::xol, Rn (n,m) ->
	 let n1 = n-1 in
	 M.to_seq m
	 |> Myseq.flat_map
	      (fun (x,d1) -> to_seq_obj (x::xs) (xol, obj n1 d1))
      | Some x::xol, Rn (n,m) ->
	 ( try
	     let d1 = M.get x m in
	     let n1 = n-1 in
	     to_seq_obj (x::xs) (xol, obj n1 d1)
	   with _ -> Myseq.empty )
      | _ -> failwith "Intreln.to_seq: Invalid restriction"

	    
    let rec mem_assoc x r = Common.prof "Intreln.mem_assoc" (fun () ->
      if r.dim = 0
      then raise (Invalid_dimension "mem_assoc")
      else M.mem x (intmap r.data))

    let rec assoc x r = Common.prof "Intreln.assoc" (fun () ->
      if r.dim = 0
      then raise (Invalid_dimension "assoc")
      else {dim = r.dim-1; data = repr (assoc_obj x (obj r.dim r.data))})
    and assoc_obj x = function
      | R0 _ -> assert false
      | R1 s -> if M.mem x s then R0 true else raise Not_found
      | Rn (n,m) -> obj (n-1) (M.get x m)

    let add_assoc x rx r = Common.prof "Intreln.add_assoc" (fun () ->
      if r.dim = 0 || rx.dim <> r.dim-1
      then raise (Invalid_dimension "add_assoc")
      else {dim = r.dim; data = data (M.set x rx.data (intmap r.data))})
	
    let keys r = Common.prof "Intreln.keys" (fun () ->
      if r.dim = 0
      then raise (Invalid_dimension "keys")
      else M.domain (intmap r.data))

    let fold_assoc f init r = Common.prof "Intreln.fold_assoc" (fun () ->
      if r.dim = 0
      then raise (Invalid_dimension "fold_assoc")
      else M.fold (fun res x d -> f res x {dim=r.dim-1; data=d}) init (intmap r.data))

    let map_assoc f r = Common.prof "Intreln.map_assoc" (fun () ->
      if r.dim = 0
      then raise (Invalid_dimension "map_assoc")
      else
	let dim1 = r.dim-1 in
	{dim = r.dim; data = data
	  (M.map
	     (fun x dx ->
	       match f x {dim=dim1; data=dx} with
	       | None -> None
	       | Some rx2 ->
		 if rx2.dim <> dim1 then raise (Invalid_dimension "map_assoc/rx2");
		 Some rx2.data)
	     (intmap r.data)) })
      
    let rec inter_assoc (oids : Intset.t) r = Common.prof "Intreln.inter_assoc" (fun () ->
      if r.dim = 0
      then raise (Invalid_dimension "inter_assoc")
      else {dim=r.dim; data = data (M.map_inter (fun x d _ -> Some d) (intmap r.data) oids)})

    let domains (r : t) : Intset.t list = Common.prof "Intreln.domains" (fun () ->
      let rec aux dim rel =
	if dim = 0
	then []
	else
	  let d1 = dim-1 in
	  fold_assoc
	    (fun domains x subrel ->
	      let domains_x = Intset.singleton x :: aux d1 subrel in
	      List.map2 Intset.union domains domains_x)
	    (List.map (fun _ -> Intset.empty) (Common.list_n dim))
	    rel
      in
      aux r.dim r)
      
    let rec group_by ps r = Common.prof "Intreln.group_by" (fun () ->
      if Mask.dim ps = r.dim
      then
	if Mask.is_prefix ps
	then r
	else
	  let size = Mask.size ps in
	  {dim = r.dim;
	   data = repr (group_by_obj size ps (obj r.dim r.data))}
      else raise (Invalid_dimension "group_by"))
    and group_by_obj size ps o = (* size = Mask.size ps *)
      match o with
      | R0 b -> o
      | R1 s -> o
      | Rn (n,m) ->
	  let n1 = n-1 in
	  let p, ps1 = split ps in
	  if p
	  then
	    let size1 = size-1 in
	    Rn (n, M.map (fun x d1 -> Some (repr (group_by_obj size1 ps1 (obj n1 d1)))) m)
	  else
	    let size1 = size in
	    M.fold
	      (fun ores x d1 -> group_by_obj_fold size1 ps1 [x] ores (obj n1 d1))
	      (empty_obj n) m
    and group_by_obj_fold size ps rev_xs ores o =
      if size = 0 (* o can be reused entirely *)
      then group_by_obj_insert ores (List.rev rev_xs) o
      else
	match ores, o, rev_xs with (* ores.dim = o.dim + |rev_xs| *)
	| R0 bres, R0 b, [] -> R0 (bres || b)
	| R1 sres, R0 b, [x] ->
	   if b
	   then R1 (M.add x sres)
	   else ores
	| Rn _, R0 b, _ ->
	   if b
	   then add_obj (List.rev rev_xs) ores
	   else ores
	| R1 sres, R1 s, [] -> R1 (M.domain_union sres s)
	| Rn (nres, mres), R1 s, _ ->
	   if List.hd ps
	   then
	     let nres1 = nres-1 in
	     let mres =
	       M.fold
		 (fun mres x _d1 ->
		  let ores1 = try obj nres1 (M.get x mres) with _ -> empty_obj nres1 in
		  let ores1 = add_obj (List.rev rev_xs) ores1 in
		  M.set x (repr ores1) mres)
		 mres s in
	     Rn (nres, mres)
	   else
	     M.fold
	       (fun ores x _d1 -> add_obj (List.rev (x::rev_xs)) ores)
	       ores s
	| Rn (nres, mres), Rn (n,m), _ ->
	   let n1 = n-1 in
	   let p, ps1 = split ps in
	   if p
	   then
	     let nres1 = nres-1 in
	     let size1 = size-1 in
	     let mres =
	       M.fold
		 (fun mres x d1 ->
		  let ores1 = try obj nres1 (M.get x mres) with _ -> empty_obj nres1 in
		  let ores1 = group_by_obj_fold size1 ps1 rev_xs ores1 (obj n1 d1) in
		  M.set x (repr ores1) mres)
		 mres m in
	     Rn (nres, mres)
	   else
	     let size1 = size in
	     M.fold
	       (fun ores x d1 -> group_by_obj_fold size1 ps1 (x::rev_xs) ores (obj n1 d1))
	       ores m
	| _ -> assert false
    and group_by_obj_insert ores xs o =
      (* ores.dim = |xs| + o.dim *)
      (* hypothesis: ores[xs] is missing or empty *)
      match ores, xs with
      | _, [] -> assert (is_empty_obj ores); o
      | R1 sres, [x] -> R1 (M.add x sres) (* o = R0 _ *)
      | Rn (nres, mres), x::xs1 ->
	 let mres =
	   let nres1 = nres-1 in
	   let ores1 = try obj nres1 (M.get x mres) with _ -> empty_obj nres1 in
	   let ores1 = group_by_obj_insert ores1 xs1 o in
	   M.set x (repr ores1) mres in
	 Rn (nres, mres)
      | _ -> assert false
	     
    let rec restriction k ?(filter = fun xs r_k -> true) r = Common.prof "Intreln.restriction" (fun () ->
      if k >= 0 && k <= r.dim
      then {dim = r.dim - k; data = repr (restriction_obj k filter [] (obj r.dim r.data))}
      else raise (Invalid_dimension "restriction"))
    and restriction_obj k f xs = function
      | R0 b ->
	  if b
	  then R0 (f (List.rev xs) {dim=0; data=Obj.repr ()})
	  else R0 false
      | R1 s ->
	  if k = 1 then R0 (f (List.rev xs) {dim=1; data=Obj.repr s})
	  else (* k = 0 *)
	    R1 (M.domain ~filter:(fun x d1 -> f (List.rev (x::xs)) {dim=0; data=Obj.repr ()}) s)
      | Rn (n,m) ->
	  if k = n then R0 (f (List.rev xs) {dim=n; data=Obj.repr m})
	  else if k+1 = n then R1 (M.domain ~filter:(fun x d1 -> f (List.rev (x::xs)) {dim=k; data=d1}) m)
	  else
	    let n1 = n-1 in
	    Rn (n-k, M.map (fun x d1 -> Some (repr (restriction_obj k f (x::xs) (obj n1 d1)))) m)

    let rec extension k f r = Common.prof "Intreln.extension" (fun () ->
      (*Printf.printf "extension: dim=%d  card=%d  k=%d\n" r.dim (cardinal r) k; flush stdout;*)
      if k >= 0
      then {dim = r.dim + k; data = repr (extension_obj k f [] (obj r.dim r.data))}
      else raise (Invalid_dimension "extension"))
    and extension_obj k f xs = function
      | R0 b ->
	  if b
	  then
	    let r_k = f xs in
	    if r_k.dim <> k then raise (Invalid_dimension "extension_obj/1");
	    if is_empty r_k
	    then empty_obj k
	    else obj r_k.dim r_k.data
	  else empty_obj k
      | R1 s ->
	  if k = 0
	  then R1 (M.domain
		     ~filter:(fun x _ -> 
		       let r_k = f (List.rev (x::xs)) in
		       if r_k.dim <> k then raise (Invalid_dimension "extension_obj/2");
		       not (is_empty r_k))
		     s)
	  else Rn (1+k,
		   M.map
		     (fun x _ ->
		       let r_k = f (List.rev (x::xs)) in
		       if r_k.dim <> k then raise (Invalid_dimension "extension_obj/3");
		       if is_empty r_k
		       then None
		       else Some r_k.data)
		     s)
      | Rn (n,m) ->
	  let n1 = n-1 in
	  Rn (n + k,
	      M.map
		(fun x d1 ->
		  let d1_k = extension_obj k f (x::xs) (obj n1 d1) in
		  if is_empty_obj d1_k
		  then None
		  else Some (repr d1_k))
		m)

    let rec memory_size r =
      3 (* reference + tag + dim *)
	+ memory_size_obj (obj r.dim r.data)
    and memory_size_obj = function
      | R0 _ -> 1
      | R1 s -> M.memory_size s
      | Rn (n,m) -> M.memory_size ~f:(fun d1 -> memory_size_obj (obj (n-1) d1)) m

  end
