(*
 * Msg: parsing message module.
 * Copyright (C) 2006
 * Dmitri Boulytchev, St.Petersburg State University
 * 
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License version 2, as published by the Free Software Foundation.
 * 
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * See the GNU Library General Public License version 2 for more details
 * (enclosed in the file COPYING).
 *)

open Printf

module Coord =
  struct

    type t = int * int

    let toString (x, y) = sprintf "(%d:%d)" x y 
   
  end

module Locator =
  struct

    type t = No | Point of Coord.t | Interval of Coord.t * Coord.t | Set of t list
    and  l = t

    let rec toString = function
        | No              -> ""
        | Point x         -> Coord.toString x
        | Interval (x, y) -> sprintf "%s-%s" (Coord.toString x) (Coord.toString y)
        | Set      x      -> String.concat ", " (List.map toString x)

    let rec start = function
      | No -> (1,1)
      | Point coord -> coord
      | Interval (c1,c2) -> c1
      | Set [] -> (1,1)
      | Set (x::_) -> start x

  end

type t = {phrase: string; args: string array; loc: Locator.t} 

let make      phrase args loc = {phrase=phrase; args=args; loc=loc}
let phrase    phrase          = make phrase [||] Locator.No
let orphan    phrase args     = make phrase args Locator.No

let string t = 
  let parmExpr = Str.regexp "%\\([0-9]+\\)" in
  Str.global_substitute 
    parmExpr  
    (fun s ->
     match int_of_string (Str.replace_matched "\\1" s) with
     | exception (Failure _) ->
       raise (Failure 
		(sprintf "invalid integer parameter specification in message phrase \"%s\"" s)
             )
     | i ->
	match t.args.(i) with
	| exception (Invalid_argument _) ->
          raise (Failure 
                   (sprintf "index out of bound while accessing message parameter in \"%s\"" s)
                )
	| x -> x
    )
    t.phrase
    
let toString t =
  let message = string t in
    match Locator.toString t.loc with
    | ""  -> message
    | loc -> message ^ " at " ^ loc
      
let augment msg loc = match msg.loc with Locator.No -> {msg with loc = loc} | _ -> msg
let augmentList msgs loc = List.map (fun x -> augment x loc) msgs
