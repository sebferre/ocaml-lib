
let trace = ref false

let get_trace () = !trace
let set_trace b = trace := b

let union_msgs msgs1 msgs2 = LSet.union msgs1 msgs2

module Locator = Msg.Locator
type  msg = {exn : exn; loc : Locator.t}

type ('cursor, 'ctx, 'res) result = Parsed of 'ctx * 'res * 'cursor | Failed of msg LSet.t

let msgs_exn exn cursor =
  [{exn; loc = Locator.Point cursor#coord}]

let msgs_failure str cursor =
  if str = ""
  then []
  else msgs_exn (Failure str) cursor


class type ['ctx, 'res, 'cursor] p =
  object
    method next : ('cursor, 'ctx, 'res) result
  end

type ('ctx, 'res, 'cursor) parse = 'ctx -> 'cursor -> ('ctx, 'res, 'cursor) p

class ['ctx,'res,'cursor] fail (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    method next : ('cursor, 'ctx, 'res) result = Failed []
  end

let fail = new fail

class ['ctx,'res,'cursor] raise_exn (exn : exn) (ctx : 'ctx) (cursor : 'cursor) =
object (self)
  method next : ('cursor,'ctx,'res) result = Failed (msgs_exn exn cursor)
end

let raise_exn exn = new raise_exn exn

class ['ctx, 'cursor] rise (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    val mutable state = `Begin

    method next =
      match state with
      | `Begin ->
	  state <- `End;
	  Parsed (ctx, cursor, cursor)
      | `End ->
	  Failed []
  end

let rise = new rise

class ['ctx, 'res0, 'res, 'cursor] map (parse : ('ctx, 'res0, 'cursor) parse) (f : 'res0 -> 'res) (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    val mutable state = `Begin (parse ctx cursor)

    method next =
      match state with
      | `Begin p ->
	  ( match p # next with
	  | Parsed (ctx1, res0, cursor') ->
	      Parsed (ctx1, f res0, cursor')
	  | Failed msgs ->
	      state <- `End msgs;
	      self#next)
      | `End msgs -> Failed msgs
  end

let map = new map
let (-->) = map

class ['ctx, 'res, 'cursor] ret (res : 'res) (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    val mutable state = `Begin

    method next : ('cursor,'ctx,'res) result =
      match state with
      | `Begin ->
	  state <- `End;
	  Parsed (ctx, res, cursor)
      | `End ->
	  Failed []
  end

let ret = new ret

(* syntaxe ideale
   param = p --> expr ==> map p (fun param -> expr)
   --> expr ==> ret expr
*)

class ['ctx, 'res, 'cursor] alt (parse1 : ('ctx, 'res, 'cursor) parse) (parse2 : ('ctx, 'res, 'cursor) parse) (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    val mutable state = `First (parse1 ctx cursor)

    method next =
      match state with
      | `First p1 ->
	  ( match p1 # next with
	  | Parsed _ as x -> x
	  | Failed msgs1 ->
	      state <- `Second (msgs1, parse2 ctx cursor);
	      self # next)
      | `Second (msgs, p2) ->
	  ( match p2 # next with
	  | Parsed _ as x -> x
	  | Failed msgs2 ->
	      state <- `End (union_msgs msgs2 msgs);
	      self # next)
      | `End msgs -> Failed msgs
  end

let alt = new alt
let (<|>) = alt

(* syntaxe ideale
   p1 | p2 ==> alt p1 p2
*)

class ['ctx, 'res1, 'res, 'cursor] seq (parse1 : ('ctx,'res1,'cursor) parse) (parse2 : 'res1 -> ('ctx,'res,'cursor) parse) (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    val mutable state = `First ([], parse1 ctx cursor)

    method next =
      match state with
      | `First (msgs, p1) ->
	  ( match p1 # next with
	  | Parsed (ctx1, res1, cursor1) ->
	      state <- `Second (msgs, p1, parse2 res1 ctx1 cursor1);
	      self # next
	  | Failed msgs1 ->
	      state <- `End (union_msgs msgs1 msgs);
	      self # next)
      | `Second (msgs, p1,p2) ->
	  ( match p2 # next with
	  | Parsed _ as x -> x
	  | Failed msgs2 ->
	      state <- `First (union_msgs msgs2 msgs, p1);
	      self # next)
      | `End msgs -> Failed msgs
  end

let seq = new seq
let (|>) = seq
let (|>>) parse1 parse2 = parse1 |> fun _ -> parse2

(* syntaxe ideale
   p1; p2 ==> seq p1 (fun _ -> p2)
   param = p1; p2 ==> seq p1 (fun param -> p2)
*)

class ['ctx,'res1,'res,'cursor] cut (parse1 : ('ctx,'res1,'cursor) parse) (parse2 : 'res1 -> ('ctx,'res,'cursor) parse) (parse3 : ('ctx,'res,'cursor) parse) (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    val mutable state = `First (parse1 ctx cursor)

    method next =
      match state with
      | `First p1 ->
	  ( match p1 # next with
	  | Parsed (ctx1, res1, cursor1) ->
	      if cursor#at_init then cursor1#init; (* forgetting the past *)
	      state <- `Second (parse2 res1 ctx1 cursor1);
	      self # next
	  | Failed msgs1 ->
	      state <- `Third (msgs1, parse3 ctx cursor);
	      self # next)
      | `Second p2 ->
	  ( match p2 # next with
	  | Parsed _ as x -> x
	  | Failed msgs2 ->
	      state <- `End msgs2;
	      self#next)
      | `Third (msgs, p3) ->
	  ( match p3 # next with
	  | Parsed _ as x -> x
	  | Failed msgs3 ->
	      state <- `End (union_msgs msgs3 msgs);
	      self#next)
      | `End msgs -> Failed msgs
  end

let cut = new cut
let (<!>) = cut
let (<!>>) parse1 parse2 parse3 = cut parse1 (fun _ -> parse2) parse3
let (</>) f x = f x
    (* parse1 <!> fun _ -> parse2 </> parse3 *)
    (* parse1 <!>> parse2 </> parse3 *)

(* syntaxe ideale
   if param = p1 then p2 else p3 ==> cut p1 (fun param -> p2) p3
   if p1 then p2 else p3 ==> cut p1 (fun _ -> p2) p3
*)

class ['ctx,'res,'cursor] guard (parse1 : ('ctx,'res,'cursor) parse) (name : string) (pred : 'res -> bool) (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    val mutable state = `Begin ([], parse1 ctx cursor)

    method next =
      match state with
      | `Begin (msgs, p1) ->
	  ( match p1 # next with
	  | Parsed (ctx1, res1, cursor1) as x ->
	      if pred res1
	      then x
	      else begin
		let msgs1 = msgs_failure name cursor in
		state <- `Begin (union_msgs msgs1 msgs, p1);
		self # next
	      end
	  | Failed msgs1 ->
	      state <- `End (union_msgs msgs1 msgs);
	      self#next)
      | `End msgs -> Failed msgs
  end

let guard = new guard
let (<&>) = guard
    (* parse <&> name </> pred *)

(* syntaxe ideale
   param = p1 when expr_bool else expr_string ==> guard p1 expr_string (fun param -> expr_bool)
*)

class ['ctx,'cursor] check (name : string) (pred : 'ctx -> 'cursor -> bool) (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    val mutable state = `Begin

    method next =
      match state with
      | `Begin ->
	  state <- `End;
	  if pred ctx cursor
	  then
	    Parsed (ctx, (), cursor)
	  else
	    Failed (msgs_failure name cursor)
      | `End -> Failed []
  end

let check = new check

class ['ctx,'res,'cursor] trial (f : unit -> 'res) (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    val mutable state = `Begin

    method next =
      match state with
      | `Begin ->
	  state <- `End;
	  begin
	    try
	      let x = f () in
	      Parsed (ctx, x, cursor)
	    with exn ->
	      Failed (msgs_exn exn cursor)
	  end
      | `End -> Failed []
  end

let trial = new trial

class ['ctx,'res,'cursor] enum (name : string) (f : unit -> 'res list) (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    val mutable state : 'res list option = None

    method next =
      match state with
      | None ->
	  state <- Some (f ());
	  if state = Some []
	  then Failed (msgs_failure name cursor)
	  else self#next
      | Some (x::xs) ->
	  state <- Some xs;
	  Parsed (ctx, x, cursor)
      | Some [] -> Failed []
  end

let enum = new enum

class ['ctx,'cursor] get_context (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    val mutable state = `Begin

    method next =
      match state with
      | `Begin ->
	  state <- `End;
	  Parsed (ctx, ctx, cursor)
      | `End -> Failed []
  end

let get_context = new get_context

class ['ctx,'cursor] set_context (ctx1 : 'ctx) (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    val mutable state = `Begin

    method next =
      match state with
      | `Begin ->
	  state <- `End;
	  Parsed (ctx1, (), cursor)
      | `End -> Failed []
  end

let set_context = new set_context

class ['ctx,'cursor] check_context (name : string) (pred : 'ctx -> bool) (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    val mutable state = `Begin

    method next =
      match state with
      | `Begin ->
	  state <- `End;
	  if pred ctx
	  then
	    Parsed (ctx, ctx, cursor)
	  else
	    Failed (msgs_failure name cursor)
      | `End -> Failed []
  end

let check_context = new check_context

class ['ctx,'cursor] enum_context (name : string) (f : 'ctx -> 'ctx list) (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    val mutable state : 'ctx list option = None

    method next =
      match state with
      | None ->
	  state <- Some (f ctx);
	  if state = Some []
	  then Failed (msgs_failure name cursor)
	  else self#next
      | Some (ctx1::l1) ->
	  state <- Some l1;
	  Parsed (ctx1, (), cursor)
      | Some [] -> Failed []
  end
  
let enum_context = new enum_context

(* repetitions, favoring longest ones *)
class ['ctx, 'elt, 'cursor] many (parse : ('ctx,'elt,'cursor) parse) (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    val mutable state = `Loop (ctx, cursor, [], parse ctx cursor, `End), []

    method next =
      match state with
	| (`Loop (ctx, cursor, rev_le, p, loop1) as loop), msgs1 ->
	  ( match p # next with
	    | Parsed (ctx1, elt, cursor1) ->
	        state <- `Loop (ctx1, cursor1, elt::rev_le, parse ctx1 cursor1, loop), msgs1;
	        self # next
	    | Failed msgs ->
	        state <- loop1, union_msgs msgs msgs1;
	        Parsed (ctx, List.rev rev_le, cursor) )
	| `End, msgs -> Failed msgs
  end

let many = new many
let some parse = parse |> fun x -> many parse --> (fun xs -> x::xs)

(* non-empty repetitions with separators, favoring longest repetitions *)
class ['ctx, 'elt, 'sep, 'cursor] list1 (p_elt : ('ctx,'elt,'cursor) parse) (p_sep : ('ctx,'sep,'cursor) parse) (ctx : 'ctx) (cursor : 'cursor) =
  object (self)
    val mutable state = `Elt (ctx, cursor, [], p_elt ctx cursor, `End), []

    method next =
      match state with
	| (`Elt (ctx, cursor, rev_le, p, loop1) as loop), msgs1 ->
	  ( match p # next with
	    | Parsed (ctx1, e, cursor1) ->
	        state <- `Sep (ctx1, cursor1, e::rev_le, p_sep ctx1 cursor1, loop), msgs1;
	        self # next
	    | Failed msgs ->
	        state <- loop1, union_msgs msgs msgs1;
	        self # next )
	| (`Sep (ctx, cursor, rev_le, p, loop1) as loop), msgs1 ->
	  ( match p # next with
	    | Parsed (ctx1, _, cursor1) ->
	        state <- `Elt (ctx1, cursor1, rev_le, p_elt ctx1 cursor1, loop), msgs1;
	        self # next
	    | Failed msgs ->
	        state <- loop1, union_msgs msgs msgs1;
	        Parsed (ctx, List.rev rev_le, cursor) )
	| `End, msgs -> Failed msgs
  end

let list1 p_elt p_sep = new list1 p_elt p_sep (* p_elt |> fun x -> list1_aux p_elt p_sep --> (fun xs -> x::xs) *)
let list0 p_elt p_sep = (list1 p_elt p_sep) <|> (ret [])


(* combinators *)

let opt parse x = parse <|> (ret x)
let (<?>) = opt

(* syntaxe ideale
   p ? ==> opt p
*)

let rec star parse = (parse |> fun x -> star parse --> (fun xs -> x::xs)) <|> (ret [])
let (<*>) = star

(* syntaxe ideale
   p * ==> star p
*)

let plus parse = parse |> fun x -> star parse --> (fun xs -> x::xs)
let (<+>) = plus

(* syntaxe ideale
   p + ==> some p
*)

(*
let rec list1 p_elt p_sep =
  p_elt |> fun x -> list1_aux p_elt p_sep --> fun xs -> x::xs
and list1_aux p_elt p_sep =
  (p_sep |>> p_elt |> fun x -> list1_aux p_elt p_sep --> fun xs -> x::xs) <|> (ret [])

let list0 p_elt p_sep =
  (list1 p_elt p_sep) <|> (ret [])
*)

exception SyntaxError of int * int * exn list
    (* line, column, exceptions *)

(* returns one parsing result *)
let once parse ctx cursor = Common.prof "Dcg.once" (fun () ->
  let p = parse ctx cursor in
  match p # next with
  | Parsed (ctx, x, _) -> ctx, x
  | Failed [] ->
      raise (SyntaxError (1,1,[]))
  | Failed (msg::msgs) ->
      let (line, col), best_exns =
	List.fold_left
	  (fun (best_coord, best_exns) msg ->
	    let coord = Locator.start msg.loc in
	    if best_coord < coord then (coord, [msg.exn])
	    else if best_coord = coord then (best_coord, (msg.exn :: best_exns))
	    else (best_coord, best_exns))
	  (Locator.start msg.loc, [msg.exn])
	  msgs in
      raise (SyntaxError (line, col, best_exns)))

(* returns a list of "once" parsing result *)
let once_fold f init parse ctx cursor = Common.prof "Dcg.once_fold" (fun () ->
  let rec aux ctx cursor acc =
    if cursor#eof
    then ctx, acc
    else
      let p = parse ctx cursor in
      match p # next with
      | Parsed (ctx1, x, cursor1) ->
	if cursor#at_init then cursor1#init;
	aux ctx1 cursor1 (f acc x)
      | Failed [] ->
	raise (SyntaxError (1,1,[]))
      | Failed (msg::msgs) ->
	let (line, col), best_exns =
	  List.fold_left
	    (fun (best_coord, best_exns) msg ->
	      let coord = Locator.start msg.loc in
	      if best_coord < coord then (coord, [msg.exn])
	      else if best_coord = coord then (best_coord, (msg.exn :: best_exns))
	      else (best_coord, best_exns))
	    (Locator.start msg.loc, [msg.exn])
	    msgs in
	raise (SyntaxError (line, col, best_exns))
  in
  aux ctx cursor init)

(* returns all parsing results *)
let all parse ctx cursor = Common.prof "Dcg.all" (fun () ->
  let p = parse ctx cursor in
  let res = ref [] in
  begin try while true do
    match p # next with
    | Parsed (ctx, x, _) -> res := (ctx, x) :: !res
    | Failed _ -> raise Not_found
  done with Not_found -> () end;
  !res)
