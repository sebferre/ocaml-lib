
let skip = Str.regexp "[ \\\n\\\t\\\r]+"

let p_ident = Matcher.get "identifier" (Str.regexp "[a-zA-Z_]\\([a-zA-Z_0-9]\\)*") (fun s -> Matcher.Token.repr s)
let p_nat = Matcher.get "natural" (Str.regexp "[0-9]+") (fun s -> int_of_string (Matcher.Token.repr s))
let p_float = Matcher.get "float" (Str.regexp "[0-9]+\\.[0-9]*") (fun s -> float_of_string (Matcher.Token.repr s))


type t = Atom of string | Not of t | And of t * t | Or of t * t


let p = dcg [< x = p_ident when x.[0] = 'a' ?? "x"; "and"; y = p_ident when y.[0] = 'b' ?? "b" >] -> And (Atom x, Atom y)

let rec parse = dcg
  | [< p = parse_or; f = parse_aux >] -> f p
and parse_aux = dcg
  | [< "and"; q = parse >] -> (fun p -> And (p, q))
  | [<>] -> (fun p -> p)
and parse_or = dcg
  | [< p = parse_not; f = parse_or_aux >] -> f p
and parse_or_aux = dcg
  | [< "or"; q = parse_or >] -> (fun p -> Or (p,q))
  | [<>] -> (fun p -> p)
and parse_not = dcg
  | [< "not"; p = parse_atom >] -> Not p
  | [< p = parse_atom >] -> p
and parse_atom = dcg
  | [< "("; p = parse; ")" >] -> p
  | [< x = p_ident >] -> Atom x

let f = Dcg.once parse (Matcher.str_of_string skip "x and (y or not z)")
