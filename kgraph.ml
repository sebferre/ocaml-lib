
type node = int
type args = node list
type 'a t = ('a * args) list

type args_ctx = node list * node list
type 'a descr = ('a * args_ctx) list

type nodeset = int Bintree.t (* allow for negative integers *)
				
let args_of_ctx (x : node) (ll,rr : args_ctx) : args = List.rev ll @ x :: rr
let ctx_of_args (lr : args) : (node * args_ctx) list =
  let rec aux ll = function
    | [] -> []
    | x::rr -> (x,(ll,rr)) :: aux (x::ll) rr
  in
  aux [] lr

let nodeset (g : 'a t) : nodeset =
  List.fold_left
    (fun res (pred,args) ->
     List.fold_left
       (fun res arg -> Bintree.add arg res)
       res args)
    Bintree.empty g
      
let index_by_pred (g : 'a t) : ('a, args list) Hashtbl.t =
  let ht = Hashtbl.create 13 in
  List.iter
    (fun (pred,args) ->
     let l_args = try Hashtbl.find ht pred with Not_found -> [] in
     Hashtbl.replace ht pred (args::l_args))
    g;
  ht

let index_by_node (g : 'a t) : (node, 'a descr) Hashtbl.t =
  let ht = Hashtbl.create 13 in
  List.iter
    (fun (pred,args) ->
     List.iter
       (fun (arg,args_ctx) ->
	let descr = try Hashtbl.find ht arg with Not_found -> [] in
	Hashtbl.replace ht arg ((pred,args_ctx)::descr))
       (ctx_of_args args))
    g;
  ht

let subgraph (nodes : nodeset) (g : 'a t) : 'a t =
  List.filter
    (fun (pred,args) ->
     List.for_all (fun x -> Bintree.mem x nodes) args)
    g

(* deprecated: memory leak *)
(*
let core_nodes ?(init_domains : (node * nodeset) list option) (graph : 'a t) : nodeset * (node * nodeset) list = Common.prof "Kgraph.core_nodes" (fun () ->
  let all_nodes = nodeset graph in
  let get_args =
    let ht = index_by_pred graph in
    fun pred -> try Hashtbl.find ht pred with _ -> []
  in
  let get_descr =
    let ht = index_by_node graph in
    fun x -> try Hashtbl.find ht x with _ -> []
  in
  let domains : (node, nodeset) Hashtbl.t = Hashtbl.create 31 in
  let get_domain x =
    try Hashtbl.find domains x
    with _ -> assert false in
  let set_domain x nodes =
    Hashtbl.replace domains x nodes in
  let core_nodes = ref Bintree.empty in
  let undet_nodes = ref all_nodes in
  let init () =
    all_nodes |> Bintree.iter (fun v -> Hashtbl.add domains v all_nodes);
    ( match init_domains with
      | Some doms -> doms |> List.iter (fun (v,dom) -> Hashtbl.add domains v dom)
      | None -> () ) in
  let changed_nodes = ref all_nodes in
  let propagate_edge_constraints () =
    Common.prof
      "Kgraph.core_nodes/propagate_edge_constraints"
      (fun () ->
       while not (Bintree.is_empty !changed_nodes) do
	 let x = Bintree.choose !changed_nodes in
	 changed_nodes := Bintree.remove x !changed_nodes;
	 let descr = get_descr x in
	 descr |>
	   List.iter
	     (fun (pred,ctx) ->
	      let x_args = args_of_ctx x ctx in
	      let arity = List.length x_args in
	      let l_args = get_args pred in
	      let new_domains =
		List.map (fun arg -> Bintree.empty) x_args in
	      let new_domains =
		l_args |>
		  List.fold_left
		    (fun new_domains args ->
		     let matching =
		       List.length args = arity &&
			 List.for_all2
			   (fun arg x_arg ->
			    match arg, x_arg with
			    | y, x_y -> Bintree.mem y (get_domain x_y))
			   args x_args in
		     if matching
		     then
		       List.map2
			 (fun arg new_domain ->
			  match arg with
			  | y -> Bintree.add y new_domain)
			 args new_domains
		     else new_domains)
		    new_domains in
	      List.iter2
		(fun x_arg new_domain ->
		 match x_arg with
		 | v ->
		    if Bintree.subset (get_domain v) new_domain
		    then () (* no change *)
		    else
		      begin
			assert (Bintree.mem v new_domain);
			set_domain v new_domain;
			changed_nodes := Bintree.add v !changed_nodes
		      end)
		x_args new_domains)
       done) in
  let rec loop () =
    propagate_edge_constraints ();
    !undet_nodes |>
      Bintree.iter
	(fun v ->
	 if Bintree.cardinal (get_domain v) = 1
	 then
	   begin
	     core_nodes := Bintree.add v !core_nodes;
	     undet_nodes := Bintree.remove v !undet_nodes
	   end);
    !undet_nodes |>
      Bintree.iter
	(fun v ->
	 if not (Bintree.is_empty (Bintree.inter (get_domain v) !core_nodes))
	 then undet_nodes := Bintree.remove v !undet_nodes);
    if not (Bintree.is_empty !undet_nodes) then
      begin
	let v = Bintree.choose !undet_nodes in
	assert (Bintree.mem v (get_domain v));
	set_domain v (Bintree.singleton v);
	changed_nodes := Bintree.add v !changed_nodes;
	core_nodes := Bintree.add v !core_nodes;
	undet_nodes := Bintree.remove v !undet_nodes;
	loop ()
      end
  in
  init ();
  loop ();
  let final_domains =
    Hashtbl.fold (fun v dom res -> (v,dom)::res) domains [] in
  !core_nodes, final_domains)
 *)

    
module Core =
  struct
    module Intmap = Intmap.M
    module Intset = Intset.Intmap
		      
    module CoreState =
      struct
	type cat = Core | NonCore | Undet
	type domain = Intset.t (* nodes *)
	type node_state =
	  { cat : cat;
	    card : int;
	    domain : domain; (* nodes *)
	  }
	type t =
	  { map : node_state Intmap.t; (* node -> node_state *)
	    core_nodes : Intset.t;
	    core_card : int;
	  }

	let cat_of_node (v : node) (card : int) (domain : domain) : cat =
	  if card=1
	  then if Intset.mem v domain then Core else NonCore
	  else Undet

	let node_state_of_domain (v : node) (domain : domain) : node_state =
	  let card = Intset.cardinal domain in
	  let cat = cat_of_node v card domain in
	  {cat; card; domain}
	     
	let create ~(init_domains : (node * domain) list) (nodes : Intset.t) : t =
	  Intset.fold
	    (fun cstate v ->
	     let domain = try List.assoc v init_domains with _ -> nodes in
	     let vstate = node_state_of_domain v domain in
	     if vstate.cat = Core
	     then
	       { map = Intmap.set v vstate cstate.map;
		 core_nodes = Intset.add v cstate.core_nodes;
		 core_card = 1 + cstate.core_card }
	     else
	       { cstate with map = Intmap.set v vstate cstate.map })
	    { map = Intmap.empty;
	      core_nodes = Intset.empty;
	      core_card = 0 }
	    nodes

	let create_identity (nodes : Intset.t) : t =
	  let init_domains =
	    Intset.fold (fun res v -> (v, Intset.singleton v)::res) [] nodes in
	  create ~init_domains nodes
	
	let set_domain (v : node) (domain : domain) (cstate : t) : t =
	  let vstate = node_state_of_domain v domain in
	  if vstate.cat = Core && not (Intset.mem v cstate.core_nodes)
	  then { map = Intmap.set v vstate cstate.map;
		 core_nodes = Intset.add v cstate.core_nodes;
		 core_card = 1 + cstate.core_card }
	  else { cstate with map = Intmap.set v vstate cstate.map }

	let get_node_state (v : node) (cstate : t) : node_state =
	  try Intmap.get v cstate.map
	  with _ -> assert false

	let get_domain (v : node) (cstate : t) : domain =
	  try (Intmap.get v cstate.map).domain
	  with _ -> assert false

	let get_cat (v : node) (cstate : t) : cat =
	  try (Intmap.get v cstate.map).cat
	  with _ -> assert false

	let get_core (cstate : t) : int * Intset.t =
	  assert (Intset.cardinal cstate.core_nodes = cstate.core_card);
	  cstate.core_card, cstate.core_nodes
      (*Intmap.fold
	(fun (card,nodes as res) v v_state ->
	 if v_state.cat = Core
	 then (card+1, Intset.add v nodes)
	 else res)
	(0,Intset.empty) cstate*)

	let fold (f : 'a -> node -> node_state -> 'a) (init : 'a) (cstate : t) : 'a =
	  Intmap.fold f init cstate.map
      end
    
    exception Empty_domain
	    
    let propagate_edge_constraints graph_state (cstate : CoreState.t) (changed_nodes : Intset.t) : CoreState.t (* raises Empty_domain *)
  = Common.prof "Kgraph.Core.propagate_edge_constraints" (fun () ->
      let changed_nodes = ref changed_nodes in
      let res = ref cstate in
      while not (Intset.is_empty !changed_nodes) do
	let x = Intset.choose !changed_nodes in
	changed_nodes := Intset.remove x !changed_nodes;
	let descr = graph_state#get_descr x in
    descr |>
      List.iter
	(fun (a,ctx) ->
	 let x_args = args_of_ctx x ctx in
	 let arity = List.length x_args in
	 let l_args = graph_state#get_args_list a in
	 let new_domains =
	   List.map (fun arg -> Intset.empty) x_args in
	 let new_domains =
	   l_args |>
	     List.fold_left
	       (fun new_domains args ->
		let matching =
		  List.length args = arity &&
		    List.for_all2
		      (fun arg x_arg ->
		       Intset.mem arg (CoreState.get_domain x_arg !res))
		      args x_args in
		if matching
		then List.map2 Intset.add args new_domains
		else new_domains)
	       new_domains in
	 List.iter2
	   (fun x_arg new_domain ->
	    let v = x_arg in
	    let domain_v = CoreState.get_domain v !res in
	    if Intset.is_empty domain_v then raise Empty_domain
	    else if Intset.subset domain_v new_domain then () (* no change *)
	    else (
	      changed_nodes := Intset.add v !changed_nodes;
	      res := CoreState.set_domain v new_domain !res))
	   x_args new_domains)
      done;
      assert (Intset.is_empty !changed_nodes);
      !res)

  
    let core_nodes ?(init_domains : (node * Intset.t) list = []) (graph : 'a t) : Intset.t * (node * node) list = Common.prof "Kgraph.Core.core_nodes" (fun () ->
      let all_nodes = nodeset graph in
      let set_all_nodes = Bintree.fold Intset.add all_nodes Intset.empty in
      let graph_state =
	let ht_args_list = index_by_pred graph in
	let ht_descr = index_by_node graph in
	object
	  method get_args_list pred =
	    try Hashtbl.find ht_args_list pred with _ -> []
	  method get_descr x =
	    try Hashtbl.find ht_descr x with _ -> []
	end
      in
      let cstate = CoreState.create ~init_domains set_all_nodes in
      let changed_nodes = set_all_nodes in
      let rec search cstate changed_nodes (best : int * Intset.t * CoreState.t) : int * Intset.t * CoreState.t = (* best: core size, core nodes, cstate = mapping *)
	let cstate_opt =
	  try Some (propagate_edge_constraints graph_state cstate changed_nodes)
	  with Empty_domain -> None in
	match cstate_opt with
	| Some cstate ->
	   let card, core = CoreState.get_core cstate in
	   let card0, core0, cstate0 = best in
	   if card >= card0
	   then best (* looking for smallest retract *)
	   else
	     let undet_choice : (int * node * Intset.t) option = (* choosing least undetermined node *)
	       cstate
	       |> CoreState.fold
		    (fun res v v_state ->
		     let open CoreState in
		     if v_state.cat = Undet
		     then
		       match res with
		       | None -> Some (v_state.card, v, v_state.domain)
		       | Some (card0, v0, domain_v0) ->
			  if v_state.card < card0
			  then Some (v_state.card, v, v_state.domain)
			  else res
		     else res)
		    None in
	     ( match undet_choice with
	       | None -> (* no undetermined node *)
		  if card < card0
		  then card, core, cstate
		  else best
	       | Some (_,v,domain_v) ->
		  let core_nodes, undet_nodes =
		    domain_v
		    |> Intset.fold
			 (fun (core_nodes, undet_nodes) x ->
			  match CoreState.get_cat x cstate with
			  | CoreState.Core -> (x::core_nodes, undet_nodes)
			  | CoreState.Undet -> (core_nodes, x::undet_nodes)
			  | CoreState.NonCore -> (core_nodes, undet_nodes))
			 ([],[]) in
		  let best =
		    core_nodes
		    |> List.fold_left
			 (fun best x ->
			  let cstate = CoreState.set_domain v (Intset.singleton x) cstate in
			  let changed_nodes = Intset.singleton v in
			  search cstate changed_nodes best)
			 best in
		  let best =
		    undet_nodes
		    |> List.fold_left
			 (fun best x ->
			  let cstate = CoreState.set_domain v (Intset.singleton x) cstate in
			  let cstate = CoreState.set_domain x (Intset.singleton x) cstate in
			  let changed_nodes = Intset.add x (Intset.singleton v) in
			  search cstate changed_nodes best)
			 best in
		  best )
	| None -> best
      in
      let core_card, core_nodes, core_cstate =
	let cstate_id = CoreState.create_identity set_all_nodes in
	search cstate changed_nodes (Intset.cardinal set_all_nodes, set_all_nodes, cstate_id) in
      let map =
	CoreState.fold
	  (fun res v v_state ->
	   let domain = v_state.CoreState.domain in
	   assert (Intset.cardinal domain = 1);
	   (v, Intset.choose domain)::res)
	  [] core_cstate in
      core_nodes, map)

  end
