
open Pcaml

EXTEND
  GLOBAL: expr ctyp;

  ctyp: LEVEL "simple"
      [ [ a = SELF; "u_monad"; es_opt = OPT [ "with"; es = ctyp_args -> es ] ->
         let e, s = match es_opt with Some es -> es | None -> <:ctyp< _ >>, <:ctyp< _ >> in
         <:ctyp< LogicMonad.t $a$ $e$ $s$ >>
	] ];

  ctyp_args:
    [ [ "u_state"; "="; s = ctyp; eo = OPT [ "and"; "u_error"; "="; e = ctyp -> e ] ->
        let e = match eo with Some e -> e | None -> <:ctyp< _ >> in
	e, s
      | "u_error"; "="; e = ctyp; so = OPT [ "and"; "u_state"; "="; s = ctyp -> s ] ->
        let s = match so with Some s -> s | None -> <:ctyp< _ >> in
	e, s
      ] ];

  expr: LEVEL "top"
      [ [ "u_def"; m = monad -> m
	| "u_run"; s = state; m = monad -> <:expr< LogicMonad.run $m$ $s$ >>
	| "u_test"; s = state; m = monad -> <:expr< LogicMonad.test $m$ $s$ >>
	] ];

  state:
    [ [ s_opt = OPT [ "with"; "u_state"; "="; s = expr LEVEL "top"; ";" -> s ] ->
      match s_opt with None -> <:expr< () >> | Some s -> s ] ];

  monad:
    [ [ m = atom -> <:expr< let open LogicMonad.PA_atom in $m$ >> ] ];

  block:
      [ [ "begin"; OPT "|"; m = alt; "end" -> m
	| "("; OPT "|"; m = alt; ")" -> m 
	] ];
    
  alt:
      [ [ m1 = seq; m2o = OPT [ "|"; m2 = alt -> m2 ] ->
        match m2o with
	| None -> m1
	| Some m2 -> <:expr< LogicMonad.mplus $m1$ $m2$ >>
	] ];

  seq:
    [ [ m1 = atom; m2o = OPT [ ";"; m2 = seq -> m2 ] ->
      match m2o with
	| None -> m1
	| Some m2 -> <:expr< LogicMonad.bind $m1$ (fun _ -> $m2$) >>
        ] ];

  atom:
    [ [ m = block -> m
      | "if"; x = ipatt; "="; m1 = alt; "then"; m2 = atom; m3o = OPT [ "else"; m3 = atom -> m3 ] ->
        let k2 = <:expr< fun $x$ -> $m2$ >> in
        let m3 = match m3o with Some m3 -> m3 | None -> <:expr< LogicMonad.mzero >> in
        <:expr< LogicMonad.ifte $m1$ $k2$ $m3$ >>
      | "let"; (x,m1) = quantif; "in"; m2 = seq ->
        <:expr< LogicMonad.bind $m1$ (fun $x$ -> $m2$) >>
      | "match"; e = expr LEVEL "top"; "with"; OPT "|"; k = conts ->
        <:expr< fun s -> $k$ $e$ s >>
      | "try"; m = alt; "with"; OPT "|"; k = conts ->
        <:expr< LogicMonad.catch $m$ $k$ >>
      | "raise"; e = expr LEVEL "top" -> <:expr< LogicMonad.raise_ $e$ >>
      | "u_state"; "<-"; e = expr LEVEL "top" ->
        <:expr< LogicMonad.put $e$ >>
      | "u_state" ->
        <:expr< LogicMonad.get >>
      | "#"; id = LIDENT; le = LIST0 [ e = expr LEVEL "top" -> e ] ->
        let app = List.fold_left (fun res arg -> <:expr< $res$ $arg$ >>) <:expr< u_state # $lid:id$ >> le in 
	<:expr< fun u_state -> $app$ u_state >>
      | m = expr LEVEL "top" -> m ] ];

  quantif:
      [ [ x = ipatt; "="; m = alt -> x, m ] ];

  conts:
    [ [ k1 = cont; k2o = OPT [ "|"; k2 = conts -> k2 ] ->
      match k2o with
	| None -> k1
	| Some k2 -> <:expr< fun x -> LogicMonad.mplus ($k1$ x) ($k2$ x) >>
      ] ];

  cont:
    [ [ x = ipatt; "->"; m = seq -> <:expr< function $x$ -> $m$ | _ -> LogicMonad.mzero >> ] ];
 
END;
