
module Intset = Intset.Intmap

module type T =
  sig
    type t
    val empty : t
    val is_empty : t -> bool
    val cardinal : t -> int
    val domain_cardinal : t -> int
    val mem : int -> int -> t -> bool
    val choose : t -> int * int
    val singleton : int -> int -> t
    val add : int -> int -> t -> t
    val remove : int -> int -> t -> t
    val union : t -> t -> t
    val inter : t -> t -> t
    val inter_domain : t -> Intset.t -> t
    val inter_range : t -> Intset.t -> t
    val diff : t -> t -> t
    val diff_domain : t -> Intset.t -> t
    val diff_range : t -> Intset.t -> t
    val union_r : t list -> t
    val inter_r : t list -> t
    val prod : Intset.t -> Intset.t -> t
    val to_seq : ?restr:int option * int option -> t -> (int * int) Myseq.t
    val fold : ('a -> int -> int -> 'a) -> 'a -> t -> 'a
(*
    val fold_mu : (string * int) list -> string list -> ('a -> (string * int) list -> 'a) -> 'a -> t -> 'a
    val fold_restr : (string * int) list -> string list -> ('a -> (string * int) list -> 'a) -> 'a -> t -> 'a
    val filter_restr : (string * int) list -> string list -> ((string * int) list -> bool) -> t -> t
*)
    val iter : (int -> int -> unit) -> t -> unit

    (* functions for accessing a relation as an association/map *)
    val mem_assoc : int -> t -> bool
    val assoc : int -> t -> Intset.t
    val keys : t -> Intset.t
    val map_assoc : (int -> Intset.t -> Intset.t option) -> t -> t
    val fold_assoc : ('a -> int -> Intset.t -> 'a) -> 'a -> t -> 'a

    val memory_size : t -> int
  end


module Intmap : T =
  struct
    module Intmap = Intmap.M

    type t = Intset.t Intmap.t

    let empty = Intmap.empty

    let is_empty r = Intmap.is_empty r

    let cardinal r = Common.prof "Intrel2.cardinal" (fun () ->
      Intmap.fold
	(fun acc x oids -> acc + Intset.cardinal oids)
	0 r)

    let domain_cardinal r = Intmap.cardinal r

    let mem x y r =
      try Intset.mem y (Intmap.get x r)
      with _ -> false

    let choose r =
      let x = Intmap.choose r in
      let y = Intset.choose (Intmap.get x r) in
      (x,y)
	
    let singleton x y =
      Intmap.set x (Intset.singleton y) Intmap.empty

    let add x y r =
      try
	let oids = Intmap.get x r in
	Intmap.set x (Intset.add y oids) r
      with Not_found ->
	Intmap.set x (Intset.singleton y) r

    let remove x y r =
      try
	let oids = Intmap.get x r in
	let oids' = Intset.remove y oids in
	if Intset.is_empty oids'
	then Intmap.remove x r
	else Intmap.set x oids' r
      with _ -> r

    let union r1 r2 = Common.prof "Intrel2.union" (fun () ->
      Intmap.map_union
	(fun x oids1_opt oids2_opt ->
	  match oids1_opt, oids2_opt with
	  | None, None -> None
	  | None, Some _ -> oids2_opt
	  | Some _, None -> oids1_opt
	  | Some oids1, Some oids2 -> Some (Intset.union oids1 oids2))
	r1 r2)

    let inter r1 r2 = Common.prof "Intrel2.inter" (fun () ->
      Intmap.map_inter
	(fun x oids1 oids2 ->
	  let oids = Intset.inter oids1 oids2 in
	  if Intset.is_empty oids
	  then None
	  else Some oids)
	r1 r2)

    let inter_domain r oids = Common.prof "Intrel2.inter_domain" (fun () ->
      Intmap.map_inter
	(fun x oids_x () -> Some oids_x)
	  (*if Intset.mem x oids
	  then Some oids_x
	    else None)*)
	r oids)

    let inter_range r oids = Common.prof "Intrel2.inter_range" (fun () ->
      Intmap.map
	(fun x oids_x ->
	  let new_oids_x = Intset.inter oids_x oids in
	  if Intset.is_empty new_oids_x
	  then None
	  else Some new_oids_x)
	r)

    let diff r1 r2 = Common.prof "Intrel2.diff" (fun () ->
      Intmap.map_diff
	(fun x oids1 oids2_opt ->
	  match oids2_opt with
	  | None -> Some oids1
	  | Some oids2 ->
	      let oids = Intset.diff oids1 oids2 in
	      if Intset.is_empty oids
	      then None
	      else Some oids)
	r1 r2)

    let diff_domain r oids = Common.prof "Intrel2.diff_domain" (fun () ->
      Intmap.map
	(fun x oids_x ->
	  if Intset.mem x oids
	  then None
	  else Some oids_x)
	r)

    let diff_range r oids = Common.prof "Intrel2.diff_range" (fun () ->
      Intmap.map
	(fun x oids_x ->
	  let new_oids_x = Intset.diff oids_x oids in
	  if Intset.is_empty new_oids_x
	  then None
	  else Some new_oids_x)
	r)

    let union_r = function
      | [] -> invalid_arg "Intreln.Intmap.union_r: empty list of relations"
      | r::rs -> List.fold_left union r rs

    let inter_r = function
      | [] -> invalid_arg "Intreln.Intmap.inter_r : empty list of relations"
      | r::rs -> List.fold_left inter r rs

    let prod dom ran =
      Intmap.mapx
	(fun () -> ran)
	dom
				
    let fold f init r = Common.prof "Intrel2.fold" (fun () ->
      Intmap.fold
	(fun acc x oids ->
	  Intset.fold
	    (fun acc y ->
	      f acc x y)
	    acc oids)
	init r)

    let iter f r = Common.prof "Intrel2.iter" (fun () ->
      Intmap.iter
	(fun x oids ->
	  Intset.iter
	    (fun y -> f x y)
	    oids)
	r)

    let to_seq ?(restr : int option * int option = None, None) r =
      Common.prof "Intrel2.to_seq" (fun () ->
      match restr with
      | None, None ->
	 Intmap.to_seq r
	 |> Myseq.flat_map
	      (fun (x,oids) ->
	       Intset.to_seq oids
	       |> Myseq.map (fun y -> (x,y)))
      | None, Some y ->
	 Intmap.to_seq r
	 |> Myseq.filter_map
	      (fun (x,oids) ->
	       if Intset.mem y oids
	       then Some (x,y)
	       else None)
      | Some x, None ->
	 ( try
	     let oids = Intmap.get x r in
	     Intset.to_seq oids
	     |> Myseq.map (fun y -> (x,y))
	   with _ -> Myseq.empty )
      | Some x, Some y ->
	 if mem x y r
	 then Myseq.return (x,y)
	 else Myseq.empty)
	   
    let mem_assoc x r =
      Intmap.mem x r

    let assoc x r =
      try Intmap.get x r
      with Not_found -> Intset.empty

    let keys r = Common.prof "Intrel2.keys" (fun () ->
      Intmap.domain r)

    let map_assoc f r = Common.prof "Intrel2.map_assoc" (fun () ->
      Intmap.map f r)
      
    let fold_assoc f init r = Common.prof "Intrel2.fold_assoc" (fun () ->
      Intmap.fold f init r)

    let memory_size r =
      Intmap.memory_size ~f:Intset.memory_size r

  end
