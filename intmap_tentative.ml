
module type T =
  sig (* set operations refer to the keys of the map *)
    type 'a t
    val cardinal : 'a t -> int
    val empty : 'a t
    val is_empty : 'a t -> bool
    val singleton : int -> unit t
    val mem : int -> 'a t -> bool
    val choose : 'a t -> int
    val get : int -> 'a t -> 'a (* raise Not_found *)
    val add : int -> unit t -> unit t
    val set : int -> 'a -> 'a t -> 'a t
    val remove : int -> 'a t -> 'a t

    val fold : ('b -> int -> 'a -> 'b) -> 'b -> 'a t -> 'b
    val iter : (int -> 'a -> unit) -> 'a t -> unit
    val domain : ?filter:(int -> 'a -> bool) -> 'a t -> unit t
    val map : (int -> 'a -> 'b option) -> 'a t -> 'b t
(*    val equal : ?filter:(int -> 'a -> 'b -> bool) -> 'a t -> 'b t -> bool *)
    val subset : ?filter:(int -> 'a -> 'b -> bool) -> 'a t -> 'b t -> bool
    val fold_inter : ('c -> int -> 'a -> 'b -> 'c) -> 'c -> 'a t -> 'b t -> 'c
    val domain_inter : ?filter:(int -> 'a -> 'b -> bool) -> 'a t -> 'b t -> unit t
    val map_inter : (int -> 'a -> 'b -> 'c option) -> 'a t -> 'b t -> 'c t
    val domain_union : ?filter:(int -> 'a option -> 'b option -> bool) -> 'a t -> 'b t -> unit t
    val map_union : (int -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t
    val domain_diff : ?filter:(int -> 'a -> 'b option -> bool) -> 'a t -> 'b t -> unit t
    val map_diff : (int -> 'a -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t
    val memory_size : ?f:('a -> int) -> 'a t -> int (* in words, the argument function gives the memory size of a value *)
  end

let unit = (Obj.magic () : 'a)

(* implementation based on Map for testing *)

module Std : T =
  struct
    module Map = Map.Make (struct type t = int let compare = Pervasives.compare end)

    type 'a t = 'a Map.t

    let cardinal m = Map.fold (fun _ _ res -> 1 + res) m 0
    let empty = Map.empty
    let is_empty = Map.is_empty
    let choose m =
      if Map.is_empty m
      then raise Not_found
      else Map.fold (fun x _ _ -> x) m 0
    let singleton x = Map.add x unit Map.empty
    let mem = Map.mem
    let get = Map.find
    let add x m = Map.add x unit m
    let set = Map.add
    let remove = Map.remove
    let fold f init m = Map.fold (fun x v res -> f res x v) m init
    let iter = Map.iter

    let domain ?(filter = fun _ _ -> true) m =
      Map.fold
	(fun x v res -> if filter x v then Map.add x unit res else res)
	m Map.empty

    let map f m =
      Map.fold
	(fun x v res -> match f x v with None -> res | Some v' -> Map.add x v' res)
	m Map.empty

    let subset ?(filter = fun _ _ _ -> true) m1 m2 =
      Map.fold
	(fun x v1 res -> res && try filter x v1 (Map.find x m2) with Not_found -> false)
	m1 true

    let fold_inter f init m1 m2 =
      Map.fold
	(fun x v1 res -> try f res x v1 (Map.find x m2) with Not_found -> res)
	m1 init

    let domain_inter ?(filter = fun _ _ _ -> true) m1 m2 =
      Map.fold
	(fun x v1 res -> try if filter x v1 (Map.find x m2) then Map.add x unit res else res with Not_found -> res)
	m1 Map.empty

    let map_inter f m1 m2 =
      Map.fold
	(fun x v1 res -> try match f x v1 (Map.find x m2) with None -> res | Some v -> Map.add x v res with Not_found -> res)
	m1 Map.empty

    let domain_union ?(filter = fun _ _ _ -> true) m1 m2 =
      let m0 =
	Map.fold
	  (fun x v1 res ->
	    if filter x (Some v1) (try Some (Map.find x m2) with Not_found -> None)
	    then Map.add x unit res
	    else res)
	  m1 Map.empty in
      Map.fold
	(fun x v2 res ->
	  if Map.mem x m1
	  then res
	  else
	    if filter x (Some unit) (Some v2)
	    then Map.add x unit res
	    else res)
	m2 m0
      
    let map_union f m1 m2 =
      let m0 =
	Map.fold
	  (fun x v1 res ->
	    match f x (Some v1) (try Some (Map.find x m2) with Not_found -> None) with
	    | None -> res
	    | Some v -> Map.add x v res)
	  m1 Map.empty in
      Map.fold
	(fun x v2 res ->
	  if Map.mem x m1
	  then res
	  else
	    match f x (Some unit) (Some v2) with
	    | None -> res
	    | Some v -> Map.add x v res)
	m2 m0
      
    let domain_diff ?(filter = fun _ _ v2_opt -> v2_opt = None) m1 m2 =
      Map.fold
	(fun x v1 res ->
	  let v2_opt = try Some (Map.find x m2) with Not_found -> None in
	  if filter x v1 v2_opt
	  then Map.add x unit res
	  else res)
	m1 Map.empty

    let map_diff f m1 m2 =
      Map.fold
	(fun x v1 res ->
	  let v2_opt = try Some (Map.find x m2) with Not_found -> None in
	  match f x v1 v2_opt with
	  | None -> res
	  | Some v -> Map.add x v res)
	m1 Map.empty

    let memory_size ?(f = fun _ -> 0) m = Map.fold (fun x v res -> 5 + f v + res) m 0
  end


(* table giving powers of [base] *)

let base = Sys.word_size - 1
let max_pow = if base = 31 then 6 else 10

let exp_base =
  let res = Array.make (max_pow+1) 1 in
  for i = 1 to max_pow do
    res.(i) <- base * res.(i-1)
  done;
  res

let log_base x = Common.prof "Intmap.log_base" (fun () ->
  let res = ref 1 in
  while !res <= max_pow && x >= exp_base.(!res) do
    incr res
  done;
  !res - 1)

let split x l =
  let bound = exp_base.(l) in
  x / bound, x mod bound

module Bitmap_base =
  struct
    let empty = 0 (* only 0s *)

    let is_empty bmp = (bmp = empty)

    let full = -1 (* only 1s *)

    let is_full bmp = (bmp = full)

    (* computing efficiently number of 1-bits in bytes and words *)

    let chunk_size = 8
    let chunk_nb = 1 lsl chunk_size
    let chunk_filter = chunk_nb - 1

    let card_byte =
      let t = Array.make chunk_nb 0 in
      for i = 0 to chunk_nb - 1 do
	for j = 0 to chunk_size - 1 do
	  if (i lsr j) land 1 <> 0
	  then t.(i) <- t.(i) + 1
	done
      done;
      t

    let cardinal bmp =
      let rec aux bmp acc =
	if bmp = 0
	then acc
	else aux (bmp lsr chunk_size) (acc + card_byte.(bmp land chunk_filter))
      in
      aux bmp 0

    let singleton x = 1 lsl x

    let mem x bmp = (bmp lsr x) land 1 <> 0

    let choose bmp =
      if bmp = empty
      then raise Not_found
      else begin
	let x = ref 0 in
	while not (mem !x bmp) do
	  incr x
	done;
	!x end

    let add x bmp = bmp lor (1 lsl x)

    let remove x bmp = bmp land (lnot (1 lsl x))

    let subset bmp1 bmp2 = bmp1 land (lnot bmp2) = 0

    let compl bmp = full land (lnot bmp)

    let union bmp1 bmp2 = bmp1 lor bmp2

    let inter bmp1 bmp2 = bmp1 land bmp2

    let diff bmp1 bmp2 = bmp1 land (lnot bmp2)

    let fold f init bmp = Common.prof "Intmap.Bitmap_base.fold" (fun () ->
      let res = ref init in
      let bmp1 = ref bmp in
      let x1 = ref 0 in
      while !bmp1 <> 0 do
	if !bmp1 land 1 <> 0 then res := f !res !x1;
	bmp1 := !bmp1 lsr 1;
	incr x1
      done;
      !res)

    let iter f bmp =
      for x1 = 0 to base - 1 do
	if mem x1 bmp
	then f x1
      done

    let filter f bmp = fold (fun res x -> if f x then res else remove x res) bmp bmp
  end

(* --------------------------------- *)

module M : T =
  struct
    type 'a t = Obj.t
	  (* 'a = unit for sets *)

(*
   type 'a repr =
   | Empty (* empty map, empty set *)
   | Full of int (* full set at some level *)
   | Single of int (* singleton set *)
   | Bitmap_base of int (* base-bounded set *)
   | Block of 'a t
   (* other cases, i.e. blocks whose tag is level,
      field 0 is mask, and other fields contain lower-level maps/sets for level > 0,
      and values for level = 0 *)
 *)

(*
   let obj level0 (m : 'a t) : 'a repr =
   (* [level0] is true if [m] is the field of a block whose level is 1. *)
   (* hence [m] is a set/map at level0, and hence the multiples tests (level=1) *)
   if Obj.is_int m
   then
   let i = (Obj.obj m : int) in
   if level0
   then Bitmap_base i
   else
   if i = -1 then Empty
   else if i < -1 then Full (-i - 1)
   else Single i (* the value has type unit *)
   else
   Block m
 *)
    let switch level0 (m : 'a t) ~empty ~full ~single ~bitmap ~block =
      (* [level0] is true if [m] is the field of a block whose level is 1. *)
      (* hence [m] is a set/map at level0, and hence the multiples tests (level=1) *)
      if Obj.is_int m
      then
	let i = (Obj.obj m : int) in
	if level0
	then bitmap i
	else
	  if i = -1 then empty ()
	  else if i < -1 then full (-i - 1)
	  else single i (* the value has type unit *)
      else
	block m

(*
   let rec repr level0 (m : 'a repr) : 'a t =
   match m with
   | Empty ->
   if level0
   then Obj.repr Bitmap_base.empty
   else Obj.repr (-1)
   | Full l ->
   if level0
   then Obj.repr Bitmap_base.full
   else Obj.repr (-l - 1)
   | Single x ->
   if level0
   then Obj.repr (Bitmap_base.singleton x)
   else Obj.repr x
   | Bitmap_base bmp ->
   if level0
   then Obj.repr bmp
   else
   if Bitmap_base.is_empty bmp then repr level0 Empty
   else if Bitmap_base.is_full bmp then repr level0 (Full 1)
   else if Bitmap_base.cardinal bmp = 1 then repr level0 (Single (Bitmap_base.fold (fun res x -> x) 0 bmp))
   else begin
   let block = Obj.new_block 1 2 in
   Obj.set_field block 0 (Obj.repr (Bitmap_base.singleton 0));
   Obj.set_field block 1 (Obj.repr bmp);
   block
   end
   | Block b ->
   let l = Obj.tag b in
   let n = Obj.size b in
   if n = 1 (* no fields *) then repr level0 Empty
   else if block_is_full b l then repr level0 (Full (1 + l))
   else if l > 0 && Obj.field b 0 = Obj.repr 1 then repr level0 (obj (l=1) (Obj.field b 1))
   else b
 *)
    let rec repr_empty level0 () =
      if level0
      then Obj.repr Bitmap_base.empty
      else Obj.repr (-1)
    and repr_full level0 l =
      if level0
      then Obj.repr Bitmap_base.full
      else Obj.repr (-l - 1)
    and repr_single level0 x =
      if level0
      then Obj.repr (Bitmap_base.singleton x)
      else Obj.repr x
    and repr_bitmap level0 bmp =
      if level0
      then Obj.repr bmp
      else
	if Bitmap_base.is_empty bmp then repr_empty level0 ()
	else if Bitmap_base.is_full bmp then repr_full level0 1
	else if Bitmap_base.cardinal bmp = 1 then repr_single level0 (Bitmap_base.fold (fun res x -> x) 0 bmp)
	else begin
	  let block = Obj.new_block 1 2 in
	  Obj.set_field block 0 (Obj.repr (Bitmap_base.singleton 0));
	  Obj.set_field block 1 (Obj.repr bmp);
	  block
	end
    and repr_block level0 b =
      let l = Obj.tag b in
      let n = Obj.size b in
      if n = 1 (* no fields *) then repr_empty level0 ()
      else if block_is_full b l then repr_full level0 (1 + l)
      else if l > 0 && Obj.field b 0 = Obj.repr 1 then
	switch (l=1) (Obj.field b 1)
	  ~empty:(repr_empty level0)
	  ~full:(repr_full level0)
	  ~single:(repr_single level0)
	  ~bitmap:(repr_bitmap level0)
	  ~block:(repr_block level0)
      else b
    and block_is_full b l = (* l must be the level of b *)
      l > 0 &&
      Obj.size b = 32 &&
      let full_l = repr_full (l=1) l in
      let res = ref true in
      for i = 1 to base do
	res := !res && Obj.field b i = full_l
      done;
      !res

(*
   let is_empty_repr = function (* low-level 'is_empty' test *)
   | Empty -> true
   | Full _ -> false
   | Single _ -> false
   | Bitmap_base bmp -> Bitmap_base.is_empty bmp
   | Block b -> Obj.size b = 1
 *)
    let is_empty level0 (m : 'a t) =
      switch level0 m
	~empty:(fun () -> true)
	~full:(fun _ -> false)
	~single:(fun _ -> false)
	~bitmap:(fun bmp -> Bitmap_base.is_empty bmp)
	~block:(fun b -> Obj.size b = 1)

(*
   let rec is_full_repr = function
   | Empty -> false
   | Full _ -> true
   | Single _ -> false
   | Bitmap_base bmp -> Bitmap_base.is_full bmp
   | Block b -> block_is_full b (Obj.tag b)
 *)
    let is_full level0 (m : 'a t) =
      switch level0 m
	~empty:(fun _ -> false)
	~full:(fun _ -> true)
	~single:(fun _ -> false)
	~bitmap:(fun bmp -> Bitmap_base.is_full bmp)
	~block:(fun b -> block_is_full b (Obj.tag b))

    module Block =
      struct
	let level block =
	  let l = Obj.tag block in
	  l, l=1 (* returns level, level0 *)

	let mask block = (Obj.obj (Obj.field block 0) : int)

	let locate block x1 =
	  let b = (mask block) lsr x1 in
	  if b land 1 = 0 (* not present *)
	  then false, Bitmap_base.cardinal b + 1
	  else true, Bitmap_base.cardinal b

	let get_field block x1 =
	  assert (x1 < base);
	  let level, level0 = level block in
	  assert (level > 0);
	  let present, i = locate block x1 in
	  if present
	  then Obj.field block i
	  else repr_empty level0 ()

	let get_value block x =
	  assert (x < base);
	  assert (level block = (0, false));
	  let present, i = locate block x in
	  if present
	  then (Obj.obj (Obj.field block i) : 'a)
	  else raise Not_found

	let one_field level x1 m =
	  let block = Obj.new_block level 2 in
	  Obj.set_field block 0 (Obj.repr (Bitmap_base.singleton x1));
	  Obj.set_field block 1 m;
	  block

	let one_value x v =
	  let block = Obj.new_block 0 2 in
	  Obj.set_field block 0 (Obj.repr (Bitmap_base.singleton x));
	  Obj.set_field block 1 (Obj.repr v);
	  block

	let two_fields level (x1,m) (y1,n) =
	  let ix, iy = if x1 < y1 then 2, 1 else 1, 2 in
	  let block = Obj.new_block level 3 in
	  Obj.set_field block 0 (Obj.repr (Bitmap_base.add x1 (Bitmap_base.singleton y1)));
	  Obj.set_field block ix m;
	  Obj.set_field block iy n;
	  block

	let two_values (x,v) (y,w) =
	  let ix, iy = if x < y then 2, 1 else 1, 2 in
	  let block = Obj.new_block 0 3 in
	  Obj.set_field block 0 (Obj.repr (Bitmap_base.add x (Bitmap_base.singleton y)));
	  Obj.set_field block ix (Obj.repr v);
	  Obj.set_field block iy (Obj.repr w);
	  block

	let full level =
	  let block = Obj.new_block level (base+1) in
	  Obj.set_field block 0 (Obj.repr Bitmap_base.full);
	  for i = 1 to base do
	    Obj.set_field block i (repr_full (level=1) level)
	  done;
	  block

	let remove_field block x1 =
	  let present, i = locate block x1 in
	  if not present
	  then block
	  else begin
	    let n = Obj.size block in
	    let level, level0 = level block in
	    assert (level > 0);
	    let block' = Obj.new_block level (n-1) in
	    Obj.set_field block' 0 (Obj.repr (Bitmap_base.remove x1 (mask block)));
	    for k = 1 to i-1 do
	      Obj.set_field block' k (Obj.field block k)
	    done;
	    for k = i to n-2 do
	      Obj.set_field block' k (Obj.field block (k+1))
	    done;
	    block' end

	let remove_value block x1 =
	  let present, i = locate block x1 in
	  if not present
	  then block
	  else begin
	    let n = Obj.size block in
	    assert (level block = (0, false));
	    let block' = Obj.new_block 0 (n-1) in
	    Obj.set_field block' 0 (Obj.repr (Bitmap_base.remove x1 (mask block)));
	    for k = 1 to i-1 do
	      Obj.set_field block' k (Obj.field block k)
	    done;
	    for k = i to n-2 do
	      Obj.set_field block' k (Obj.field block (k+1))
	    done;
	    block' end

	let replace_field block x1 f =
	  let level, level0 = level block in
	  assert (level > 0);
	  let present, i = locate block x1 in
	  if not present
	  then
	    let m' = f (repr_empty level0 ()) in
	    if is_empty level0 m'
	    then block
	    else begin
	      let n = Obj.size block in
	      let block' = Obj.new_block level (n+1) in
	      Obj.set_field block' 0 (Obj.repr (Bitmap_base.add x1 (mask block)));
	      for k = n downto i+1 do
		Obj.set_field block' k (Obj.field block (k-1))
	      done;
	      Obj.set_field block' i m';
	      for k = i-1 downto 1 do
		Obj.set_field block' k (Obj.field block k)
	      done;
	      block' end
	  else
	    let m' = f (Obj.field block i) in (* new field *)
	    if is_empty level0 m'
	    then remove_field block x1
	    else begin
	      let block' = Obj.dup block in
	      Obj.set_field block' i m';
	      block'
	    end

	let replace_value block x1 f =
	  assert (level block = (0, false));
	  let present, i = locate block x1 in
	  if not present
	  then begin
	    let v' = f unit in
	    let n = Obj.size block in
	    let block' = Obj.new_block 0 (n+1) in
	    Obj.set_field block' 0 (Obj.repr (Bitmap_base.add x1 (mask block)));
	    for k = n downto i+1 do
	      Obj.set_field block' k (Obj.field block (k-1))
	    done;
	    Obj.set_field block' i (Obj.repr v');
	    for k = i-1 downto 1 do
	      Obj.set_field block' k (Obj.field block k)
	    done;
	    block' end
	  else begin
	    let v' = f (Obj.obj (Obj.field block i) : 'a) in
	    let block' = Obj.dup block in
	    Obj.set_field block' i (Obj.repr v');
	    block' end

	let fields_from_mask (level, level0) bmp f = Common.prof "Bitmap.Block.fields_from_mask" (fun () ->
	  let bmp, l =
	    Bitmap_base.fold
	      (fun (bmp,l) x1 ->
		match f x1 with
		| None -> (Bitmap_base.remove x1 bmp), l
		| Some m ->
		    if is_empty level0 m
		    then (Bitmap_base.remove x1 bmp), l
		    else bmp, m::l)
	      (bmp, [])
	      bmp in
	  let block = Obj.new_block level (1 + List.length l) in
	  Obj.set_field block 0 (Obj.repr bmp);
	  let i = ref 1 in
	  List.iter (fun m -> Obj.set_field block !i m; incr i) l;
	  block)

	let mapfilter_fields f block =
	  fields_from_mask (level block) (mask block)
	    (fun x1 -> f x1 (get_field block x1))

	let values_from_mask bmp f = Common.prof "Bitmap.Block.values_from_mask" (fun () ->
	  let bmp, l =
	    Bitmap_base.fold
	      (fun (bmp,l) x -> match f x with None -> (Bitmap_base.remove x bmp), l | Some v -> bmp, (v :: l))
	      (bmp,[])
	      bmp in
	  let block = Obj.new_block 0 (1 + List.length l) in
	  Obj.set_field block 0 (Obj.repr bmp);
	  let i = ref 1 in
	  List.iter (fun v -> Obj.set_field block !i (Obj.repr v); incr i) l;
	  block)

	let mapfilter_values f block =
	  values_from_mask (mask block)
	    (fun x -> f x (get_value block x))

	let fold_fields f init block =
	  let level, level0 = level block in
	  assert (level > 0);
	  let n = Obj.size block in
	  let res = ref init in
	  for i = 1 to n-1 do
	    res := f !res (Obj.field block i)
	  done;
	  !res

	let fold_values f init block =
	  assert (level block = (0, false));
	  let n = Obj.size block in
	  let res = ref init in
	  for i = 1 to n-1 do
	    res := f !res (Obj.obj (Obj.field block i) : 'a)
	  done;
	  !res
      end


(* --------------------- *)

    let empty : 'a t = repr_empty false ()

    let is_empty (m : 'a t) : bool =
      is_empty false m

    let singleton (x : int) : unit t = repr_single false x

    let rec cardinal ?(level0 = false) (m : 'a t) : int = Common.prof "Intmap.cardinal" (fun () ->
      switch level0 m
	~empty:(fun _ -> 0)
	~full:(fun level -> exp_base.(level))
	~single:(fun _ -> 1)
	~bitmap:(fun bmp -> Bitmap_base.cardinal bmp)
	~block:(fun b ->
	  let level, level0 = Block.level b in
	  if level = 0
	  then Bitmap_base.cardinal (Block.mask b)
	  else Block.fold_fields (fun res m -> res + cardinal ~level0 m) 0 b))

    let rec mem ?(level0 = false) (x : int) (m : 'a t) : bool =
      switch level0 m
	~empty:(fun () -> false)
	~full:(fun l -> x < exp_base.(l))
	~single:(fun y -> x = y)
	~bitmap:(fun bmp -> x < base && Bitmap_base.mem x bmp)
	~block:(fun b ->
	  let level, level0 = Block.level b in
	  let x1, x2 = split x level in
	  x1 < base &&
	  try
	    if level = 0
	    then begin ignore (Block.get_value b x1); true end
	    else mem ~level0 x2 (Block.get_field b x1)
	  with Not_found -> false)

    let rec choose ?(level0 = false) ?(pos = 0) (m : 'a t) : int = Common.prof "Intmap.choose" (fun () ->
      switch level0 m
	~empty:(fun _ -> raise Not_found)
	~full:(fun l -> pos + 0)
	~single:(fun y -> pos + y)
	~bitmap:(fun bmp -> pos + Bitmap_base.choose bmp)
	~block:(fun b ->
	  let level, level0 = Block.level b in
	  let x1 = Bitmap_base.choose (Block.mask b) in
	  if level = 0
	  then pos + x1
	  else
	    let bound = exp_base.(level) in
	    choose ~level0 ~pos:(pos + x1*bound) (Block.get_field b x1)))

    let rec get ?(level0 = false) (x : int) (m : 'a t) : 'a = Common.prof "Intmap.get" (fun () ->
      switch level0 m
	~empty:(fun _ ->
	  raise Not_found)
	~full:(fun l ->
	  if x < exp_base.(l)
	  then unit
	  else raise Not_found)
	~single:(fun y ->
	  if x = y
	  then unit
	  else raise Not_found)
	~bitmap:(fun bmp ->
	  if x < base && Bitmap_base.mem x bmp
	  then unit
	  else raise Not_found)
	~block:(fun b ->
	  let level, level0 = Block.level b in
	  let x1, x2 = split x level in
	  if x1 < base
	  then
	    if level = 0
	    then Block.get_value b x1
	    else get ~level0 x2 (Block.get_field b x1)
	  else raise Not_found))
	
    let rec add ?(level0 = false) x m = Common.prof "Intmap.add" (fun () ->
      switch level0 m
	~empty:(fun _ -> repr_single level0 x)
	~full:(fun l ->
	  let lev = log_base x in
	  let lev0 = (lev=1) in
	  if l <= lev (* x is out of Full l range *)
	  then
	    let x1, x2 = split x lev in
	    repr_block level0 (Block.two_fields lev (0,m) (x1, repr_single lev0 x2))
	  else m)
	~single:(fun y ->
	  let lev = log_base (max x y) in
	  let lev0 = (lev=1) in
	  if lev = 0
	  then repr_bitmap level0 (Bitmap_base.add x (Bitmap_base.singleton y))
	  else
	    let bound = exp_base.(lev) in
	    let x1, x2 = x / bound, x mod bound in
	    let y1, y2 = y / bound, y mod bound in
	    if x1 = y1
	    then repr_block level0 (Block.one_field lev x1 (add ~level0:lev0 x2 (repr_single lev0 y2)))
	    else repr_block level0 (Block.two_fields lev (x1, repr_single lev0 x2) (y1, repr_single lev0 y2)))
	~bitmap:(fun bmp ->
	  let lev = log_base x in
	  let lev0 = (lev=1) in
	  if lev = 0 (* x < base *)
	  then repr_bitmap level0 (Bitmap_base.add x bmp)
	  else
	    let x1, x2 = split x lev in
	    repr_block level0 (Block.two_fields lev (0,m) (x1, repr_single lev0 x2)))
	~block:(fun b ->
	  let b_level, b_level0 = Block.level b in
	  let lev = log_base x in
	  let lev0 = (lev=1) in
	  if lev <= b_level
	  then
	    let x1, x2 = split x b_level in
	    repr_block level0 (Block.replace_field b x1 (fun m_x1 -> add ~level0:b_level0 x2 m_x1))
	  else
	    let x1, x2 = split x lev in
	    repr_block level0 (Block.two_fields lev (0, repr_block lev0 b) (x1, repr_single lev0 x2))))
	
    let rec set ?(level0 = false) x v m = Common.prof "Intmap.set" (fun () ->
      switch level0 m
	~empty:(fun _ ->
	  let lev = log_base x in
	  let lev0 = (lev=1) in
	  if lev = 0 (* x < base *)
	  then repr_block level0 (Block.one_value x v)
	  else
	    let x1, x2 = split x lev in
	    repr_block level0 (Block.one_field lev x1 (set ~level0:lev0 x2 v (repr_empty lev0 ()))))
	~full:(fun _ -> add ~level0 x m)
	~single:(fun _ -> add ~level0 x m)
	~bitmap:(fun _ -> add ~level0 x m)
	~block:(fun b ->
	  let b_level, b_level0 = Block.level b in
	  let lev = log_base x in
	  let lev0 = (lev=1) in
	  if lev <= b_level
	  then
	    let x1, x2 = split x b_level in
	    if b_level = 0
	    then repr_block level0 (Block.replace_value b x1 (fun _ -> v))
	    else repr_block level0 (Block.replace_field b x1 (set ~level0:b_level0 x2 v))
	  else
	    let x1, x2 = split x lev in
	    repr_block level0 (Block.two_fields lev (0, repr_block lev0 b) (x1, set ~level0:lev0 x2 v (repr_empty lev0 ())))))

    let rec remove ?(level0 = false) (x : int) (m : 'a t) : 'a t = Common.prof "Intmap.remove" (fun () ->
      switch level0 m
	~empty:(fun _ -> repr_empty level0 ())
	~full:(fun l ->
	  if x >= exp_base.(l)
	  then m
	  else
	    let m_full =
	      if l = 1
	      then repr_bitmap level0 Bitmap_base.full
	      else repr_block level0 (Block.full (l-1)) in
	    remove ~level0 x m_full)
	~single:(fun y ->
	  if x = y
	  then repr_empty level0 ()
	  else m)
	~bitmap:(fun bmp ->
	  if x < base
	  then repr_bitmap level0 (Bitmap_base.remove x bmp)
	  else m)
	~block:(fun b ->
	  let b_level, b_level0 = Block.level b in
	  let lev = log_base x in
	  if lev > b_level
	  then m
	  else
	    let x1, x2 = split x b_level in
	    if b_level = 0
	    then repr_block level0 (Block.remove_value b x1)
	    else repr_block level0 (Block.replace_field b x1 (fun m_x1 -> remove ~level0:b_level0 x2 m_x1))))

    let rec fold ?(level0 = false) ?(pos = 0) f acc (m : 'a t) = Common.prof "Intmap.fold" (fun () ->
      switch level0 m
	~empty:(fun _ -> acc)
	~full:(fun l ->
	  let res = ref acc in
	  for x = 0 to exp_base.(l) - 1 do
	    res := f !res (pos+x) unit
	  done;
	  !res)
	~single:(fun y ->
	  f acc (pos+y) unit)
	~bitmap:(fun bmp ->
	  Bitmap_base.fold (fun res x -> f res (pos+x) unit) acc bmp)
	~block:(fun b ->
	  let level, level0 = Block.level b in
	  if level = 0
	  then
	    Bitmap_base.fold
	      (fun res x -> f res (pos+x) (Block.get_value b x))
	      acc
	      (Block.mask b)
	  else
	    let bound = exp_base.(level) in
	    Bitmap_base.fold
	      (fun res x1 -> fold ~level0 f res ~pos:(pos + x1 * bound) (Block.get_field b x1))
	      acc
	      (Block.mask b)))

    let rec iter ?(level0 = false) ?(pos = 0) f (m : 'a t) = Common.prof "Intmap.iter" (fun () ->
      switch level0 m
	~empty:(fun _ -> ())
	~full:(fun l ->
	  for x = 0 to exp_base.(l) - 1 do
	    f (pos+x) unit
	  done)
	~single:(fun y ->
	  f (pos+y) unit)
	~bitmap:(fun bmp ->
	  Bitmap_base.iter (fun x -> f (pos+x) unit) bmp)
	~block:(fun b ->
	  let level, level0 = Block.level b in
	  if level = 0
	  then
	    Bitmap_base.iter
	      (fun x -> f (pos+x) (Block.get_value b x))
	      (Block.mask b)
	  else
	    let bound = exp_base.(level) in
	    Bitmap_base.iter
	      (fun x1 -> iter ~level0 f ~pos:(pos + x1 * bound) (Block.get_field b x1))
	      (Block.mask b)))

    let rec domain ?(level0 = false) ?filter ?(pos = 0) m = Common.prof "Intmap.domain" (fun () ->
      switch level0 m
	~empty:(fun _ -> m)
	~full:(fun l ->
	  match filter with
	  | None -> m
	  | Some f ->
	      let res = ref (repr_empty level0 ()) in
	      for x = 0 to exp_base.(l) - 1 do
		if f (pos+x) unit
		then res := add ~level0 x !res
	      done;
	      !res)
	~single:(fun y ->
	  match filter with
	  | None -> m
	  | Some f ->
	      if f (pos+y) unit
	      then m
	      else repr_empty level0 ())
	~bitmap:(fun bmp ->
	  match filter with
	  | None -> m
	  | Some f ->
	      repr_bitmap level0 (Bitmap_base.filter (fun x -> f (pos+x) unit) bmp))
	~block:(fun b ->
	  let b_level, b_level0 = Block.level b in
	  if b_level = 0
	  then
	    match filter with
	    | None -> repr_bitmap level0 (Block.mask b)
	    | Some f ->
		repr_bitmap level0 (Bitmap_base.filter (fun x -> f (pos+x) (Block.get_value b x)) (Block.mask b))
	  else
	    let bound = exp_base.(b_level) in
	    repr_block level0
	      (Block.mapfilter_fields
		 (fun x1 m_x1 -> Some (domain ~level0:b_level0 ?filter ~pos:(pos + x1 * bound) m_x1))
		 b)))

    let rec map ?(level0 = false) f ?(pos = 0) m = Common.prof "Intmap.map" (fun () ->
      switch level0 m
	~empty:(fun _ -> repr_empty level0 ())
	~full:(fun l ->
	  let res = ref (repr_empty level0 ()) in
	  for x = 0 to exp_base.(l) - 1 do
	    match f (pos+x) unit with
	    | None -> ()
	    | Some v' -> res := set ~level0 x v' !res
	  done;
	  !res)
	~single:(fun y ->
	  match f (pos+y) unit with
	  | None -> repr_empty level0 ()
	  | Some v' -> set ~level0 y v' (repr_empty level0 ()))
	~bitmap:(fun bmp ->
	  repr_block level0
	    (Block.values_from_mask bmp (fun x -> f (pos+x) unit)))
	~block:(fun b ->
	  let b_level, b_level0 = Block.level b in
	  if b_level = 0
	  then
	    repr_block level0 (Block.mapfilter_values (fun x v -> f (pos+x) v) b)
	  else
	    let bound = exp_base.(b_level) in
	    repr_block level0
	      (Block.mapfilter_fields
		 (fun x1 m_x1 -> Some (map ~level0:b_level0 f ~pos:(pos + x1 * bound) m_x1))
		 b)))

    let rec subset ?(level0 = false) ?filter ?(pos = 0) m1 m2 = Common.prof "Intmap.subset" (fun () ->
      let all1 ~level0 ~pos m1 =
	match filter with
	| None -> true
	| Some f -> fold ~level0 (fun res x v1 -> res && f x v1 unit) true ~pos m1
      and all12 ~level0 ~pos m1 m2 =
	match filter with
	| None -> true
	| Some f -> fold ~level0 (fun res x v1 -> res && try f x v1 (get ~level0 x m2) with Not_found -> false) true ~pos m1 in
      switch level0 m1
	~empty:(fun _ -> true)
(*      | _, Empty -> is_empty_repr m1 *)
	~full:(fun l1 ->
	  switch level0 m2
	    ~empty:(fun _ -> is_empty ~level0 m1)
	    ~full:(fun l2 -> l1 <= l2 && all1 ~level0 ~pos m1)
	    ~single:(fun y2 -> false)
	    ~bitmap:(fun bmp2 -> l1 = 1 && Bitmap_base.is_full bmp2 && all1 ~level0 ~pos m1)
	    ~block:(fun b2 ->
	      let l2, _ = Block.level b2 in
	      if l2 >= l1 then subset pos m1 (Block.get_field b2 0)
	      else if l2 = l1-1 then block_is_full b2 l2 && all12 pos m1 m2
	      else false

      | Single y1, Full l2 -> y1 < exp_base.(l2) && all1 pos m1
      | Bitmap_base bmp1, Full l2 -> all1 pos m1
      | Block b1, Full l2 ->
	  let l1 = Block.level b1 in
	  l1 < l2 && all1 pos m1
      | Single y1, Single y2 -> y1 = y2 && all1 pos m1
      | Single y1, Bitmap_base bmp2 -> y1 < base && Bitmap_base.mem y1 bmp2 && all1 pos m1
      | Single y1, Block b2 -> mem_repr y1 m2 && all12 pos m1 m2
      | Bitmap_base bmp1, Single y2 -> y2 < base && bmp1 = Bitmap_base.singleton y2 && all1 pos m1
      | Block b1, Single y2 -> cardinal_repr m1 = 1 && mem_repr y2 m1 && all1 pos m1
      | Bitmap_base bmp1, Bitmap_base bmp2 -> Bitmap_base.subset bmp1 bmp2 && all1 pos m1
      | Bitmap_base bmp1, Block b2 ->
	  let l2 = Block.level b2 in
	  if l2 = 0
	  then
	    Bitmap_base.subset bmp1 (Block.mask b2) &&
	    all12 pos m1 m2
	  else subset_repr pos m1 (Block.get_field b2 0)
      | Block b1, Bitmap_base bmp2 ->
	  let l1 = Block.level b1 in
	  if l1 = 0
	  then
	    Bitmap_base.subset (Block.mask b1) bmp2 &&
	    all1 pos m1
	  else Block.mask b1 = 1 && subset_repr pos (Block.get_field b1 0) m2
      | Block b1, Block b2 ->
	  let l1, l2 = Block.level b1, Block.level b2 in
	  if l1 < l2 then subset_repr pos m1 (Block.get_field b2 0)
	  else if l1 > l2 then Block.mask b1 = 1 && subset_repr pos (Block.get_field b1 0) m2
	  else (* l1 = l2 *)
	    let mask1 = Block.mask b1 in
	    Bitmap_base.subset mask1 (Block.mask b2) &&
	    if l1 = 0
	    then all12 pos m1 m2
	    else
	      let bound = exp_base.(l1) in
	      Bitmap_base.fold (fun res x1 -> res && subset_repr (pos + x1 * bound) (Block.get_field b1 x1) (Block.get_field b2 x1)) true mask1

    let fold_inter f init (m1 : 'a t) (m2 : 'b t) = Common.prof "Intmap.fold_inter" (fun () ->
      let fold1 acc pos m1 = fold_repr (fun res x v1 -> f res x v1 unit) acc pos m1 in
      let fold2 acc pos m2 = fold_repr (fun res x v2 -> f res x unit v2) acc pos m2 in
      let rec fold_inter_repr acc pos m1 m2 =
	match m1, m2 with
	| Empty, _ -> acc
	| _, Empty -> acc
	| Full l1, Full l2 ->
	    fold1 acc pos (Full (min l1 l2))
	| Full l1, Single y2 ->
	    if y2 < exp_base.(l1)
	    then fold2 acc pos m2
	    else acc
	| Single y1, Full l2 ->
	    if y1 < exp_base.(l2)
	    then fold1 acc pos m1
	    else acc
	| Full l1, Bitmap_base bmp2 ->
	    fold2 acc pos m2
	| Bitmap_base bmp1, Full l2  ->
	    fold1 acc pos m1
	| Full l1, Block b2 ->
	    let l2 = Block.level b2 in
	    if l1 > l2
	    then fold2 acc pos m2
	    else fold_inter_repr acc pos m1 (Block.get_field b2 0)
	| Block b1, Full l2 ->
	    let l1 = Block.level b1 in
	    if l1 < l2
	    then fold1 acc pos m1
	    else fold_inter_repr acc pos (Block.get_field b1 0) m2
	| Single y1, Single y2 ->
	    if y1 = y2
	    then fold1 acc pos m1
	    else acc
	| Single y1, Bitmap_base bmp2 ->
	    if y1 < base && Bitmap_base.mem y1 bmp2
	    then fold1 acc pos m1
	    else acc
	| Bitmap_base bmp1, Single y2 ->
	    if y2 < base && Bitmap_base.mem y2 bmp1
	    then fold2 acc pos m2
	    else acc
	| Single y1, Block b2  ->
	    (try f acc (pos+y1) unit (get_repr y1 m2)
	    with Not_found -> acc)
	| Block b1, Single y2 ->
	    (try f acc (pos+y2) (get_repr y2 m1) unit
	    with Not_found -> acc)
	| Bitmap_base bmp1, Bitmap_base bmp2 ->
	    fold1 acc pos (Bitmap_base (Bitmap_base.inter bmp1 bmp2))
	| Bitmap_base bmp1, Block b2 ->
	    let l2 = Block.level b2 in
	    if l2 = 0
	    then
	      let mask = Bitmap_base.inter bmp1 (Block.mask b2) in
	      Bitmap_base.fold (fun res x -> f res (pos+x) unit (Block.get_value b2 x)) acc mask
	    else fold_inter_repr acc pos m1 (Block.get_field b2 0)
	| Block b1, Bitmap_base bmp2 ->
	    let l1 = Block.level b1 in
	    if l1 = 0
	    then
	      let mask = Bitmap_base.inter (Block.mask b1) bmp2 in
	      Bitmap_base.fold (fun res x -> f res (pos+x) (Block.get_value b1 x) unit) acc mask
	    else fold_inter_repr acc pos (Block.get_field b1 0) m2
	| Block b1, Block b2 ->
	    let l1, l2 = Block.level b1, Block.level b2 in
	    if l1 < l2 then fold_inter_repr acc pos m1 (Block.get_field b2 0)
	    else if l1 > l2 then fold_inter_repr acc pos (Block.get_field b1 0) m2
	    else (* l1 = l2 *)
	      let mask = Bitmap_base.inter (Block.mask b1) (Block.mask b2) in
	      if l1 = 0
	      then
		Bitmap_base.fold (fun res x -> f res (pos+x) (Block.get_value b1 x) (Block.get_value b2 x)) acc mask
	      else
		let bound = exp_base.(l1) in
		Bitmap_base.fold
		  (fun res x1 -> fold_inter_repr res (pos + x1 * bound) (Block.get_field b1 x1) (Block.get_field b2 x1))
		  acc mask
      in
      fold_inter_repr init 0 (obj false m1) (obj false m2))


    let domain_inter ?filter (m1 : 'a t) (m2 : 'b t) = Common.prof "Intmap.domain_inter" (fun () ->
      let filter1 pos m1 = match filter with None -> m1 | Some f -> domain_repr ~filter:(fun x v -> f x v unit) pos m1 in
      let filter2 pos m2 = match filter with None -> m2 | Some f -> domain_repr ~filter:(fun x v -> f x unit v) pos m2 in
      let rec domain_inter_repr pos m1 m2 =
	match m1, m2 with
	| Empty, _ -> Empty
	| _, Empty -> Empty
	| Full l1, Full l2 ->
	    filter1 pos (Full (min l1 l2))
	| Full l1, Single y2 ->
	    if y2 < exp_base.(l1)
	    then filter2 pos m2
	    else Empty
	| Single y1, Full l2 ->
	    if y1 < exp_base.(l2)
	    then filter1 pos m1
	    else Empty
	| Full l1, Bitmap_base bmp2 ->
	    filter2 pos m2
	| Bitmap_base bmp1, Full l2  ->
	    filter1 pos m1
	| Full l1, Block b2 ->
	    let l2 = Block.level b2 in
	    if l1 > l2
	    then filter2 pos m2
	    else domain_inter_repr pos m1 (Block.get_field b2 0)
	| Block b1, Full l2 ->
	    let l1 = Block.level b1 in
	    if l1 < l2
	    then filter1 pos m1
	    else domain_inter_repr pos (Block.get_field b1 0) m2
	| Single y1, Single y2 ->
	    if y1 = y2
	    then filter1 pos m1
	    else Empty
	| Single y1, Bitmap_base bmp2 ->
	    if y1 < base && Bitmap_base.mem y1 bmp2
	    then filter1 pos m1
	    else Empty
	| Bitmap_base bmp1, Single y2 ->
	    if y2 < base && Bitmap_base.mem y2 bmp1
	    then filter2 pos m2
	    else Empty
	| Single y1, Block b2  ->
	    (try
	      let v2 = get_repr y1 m2 in
	      match filter with
	      | None -> m1
	      | Some f -> if f (pos+y1) unit v2 then m1 else Empty
	    with Not_found -> Empty)
	| Block b1, Single y2 ->
	    (try
	      let v1 = get_repr y2 m1 in
	      match filter with
	      | None -> m2
	      | Some f -> if f (pos+y2) v1 unit then m2 else Empty
	    with Not_found -> Empty)
	| Bitmap_base bmp1, Bitmap_base bmp2 ->
	    filter1 pos (Bitmap_base (Bitmap_base.inter bmp1 bmp2))
	| Bitmap_base bmp1, Block b2 ->
	    let l2 = Block.level b2 in
	    if l2 = 0
	    then
	      let mask = Bitmap_base.inter bmp1 (Block.mask b2) in
	      match filter with
	      | None -> Bitmap_base mask
	      | Some f -> Bitmap_base (Bitmap_base.filter (fun x -> f (pos+x) unit (Block.get_value b2 x)) mask)
	    else domain_inter_repr pos m1 (Block.get_field b2 0)
	| Block b1, Bitmap_base bmp2 ->
	    let l1 = Block.level b1 in
	    if l1 = 0
	    then
	      let mask = Bitmap_base.inter (Block.mask b1) bmp2 in
	      match filter with
	      | None -> Bitmap_base mask
	      | Some f -> Bitmap_base (Bitmap_base.filter (fun x -> f (pos+x) (Block.get_value b1 x) unit) mask)
	    else domain_inter_repr pos (Block.get_field b1 0) m2
	| Block b1, Block b2 ->
	    let l1, l2 = Block.level b1, Block.level b2 in
	    if l1 < l2 then domain_inter_repr pos m1 (Block.get_field b2 0)
	    else if l1 > l2 then domain_inter_repr pos (Block.get_field b1 0) m2
	    else (* l1 = l2 *)
	      let mask = Bitmap_base.inter (Block.mask b1) (Block.mask b2) in
	      if l1 = 0
	      then
		match filter with
		| None -> Bitmap_base mask
		| Some f -> Bitmap_base (Bitmap_base.filter (fun x -> f (pos+x) (Block.get_value b1 x) (Block.get_value b2 x)) mask)
	      else
		let bound = exp_base.(l1) in
		Block (Block.fields_from_mask l1 mask
			 (fun x1 -> Some (domain_inter_repr (pos + x1 * bound) (Block.get_field b1 x1) (Block.get_field b2 x1))))
      in
      repr false (domain_inter_repr 0 (obj false m1) (obj false m2)))

    let map_inter f (m1 : 'a t) (m2 : 'b t) = Common.prof "Intmap.map_inter" (fun () ->
      let map1 pos m1 = map_repr (fun x v -> f x v unit) pos m1 in
      let map2 pos m2 = map_repr (fun x v -> f x unit v) pos m2 in
      let rec map_inter_repr pos m1 m2 =
	match m1, m2 with
	| Empty, _ -> Empty
	| _, Empty -> Empty
	| Full l1, Full l2 ->
	    map1 pos (Full (min l1 l2))
	| Full l1, Single y2 ->
	    if y2 < exp_base.(l1)
	    then map2 pos m2
	    else Empty
	| Single y1, Full l2 ->
	    if y1 < exp_base.(l2)
	    then map1 pos m1
	    else Empty
	| Full l1, Bitmap_base bmp2 ->
	    map2 pos m2
	| Bitmap_base bmp1, Full l2  ->
	    map1 pos m1
	| Full l1, Block b2 ->
	    let l2 = Block.level b2 in
	    if l2 = 0
	    then
	      let mask = Block.mask b2 in
	      if Bitmap_base.is_empty mask
	      then Empty
	      else
		Block (Block.values_from_mask mask
			 (fun x -> f (pos+x) unit (Block.get_value b2 x)))
	    else
	      if l1 > l2
	      then map2 pos m2
	      else map_inter_repr pos m1 (Block.get_field b2 0)
	| Block b1, Full l2 ->
	    let l1 = Block.level b1 in
	    if l1 = 0
	    then
	      let mask = Block.mask b1 in
	      if Bitmap_base.is_empty mask
	      then Empty
	      else
		Block (Block.values_from_mask mask
			 (fun x -> f (pos+x) (Block.get_value b1 x) unit))
	    else
	      if l1 < l2
	      then map1 pos m1
	      else map_inter_repr pos (Block.get_field b1 0) m2
	| Single y1, Single y2 ->
	    if y1 = y2
	    then map1 pos m1
	    else Empty
	| Single y1, Bitmap_base bmp2 ->
	    if y1 < base && Bitmap_base.mem y1 bmp2
	    then map1 pos m1
	    else Empty
	| Bitmap_base bmp1, Single y2 ->
	    if y2 < base && Bitmap_base.mem y2 bmp1
	    then map2 pos m2
	    else Empty
	| Single y1, Block b2  ->
	    (try
	      let v2 = get_repr y1 m2 in
	      match f (pos+y1) unit v2 with
	      | None -> Empty
	      | Some v -> set_repr y1 v Empty
	    with Not_found -> Empty)
	| Block b1, Single y2 ->
	    (try
	      let v1 = get_repr y2 m1 in
	      match f (pos+y2) v1 unit with
	      | None -> Empty
	      | Some v -> set_repr y2 v Empty
	    with Not_found -> Empty)
	| Bitmap_base bmp1, Bitmap_base bmp2 ->
	    map1 pos (Bitmap_base (Bitmap_base.inter bmp1 bmp2))
	| Bitmap_base bmp1, Block b2 ->
	    let l2 = Block.level b2 in
	    if l2 = 0
	    then
	      let mask = Bitmap_base.inter bmp1 (Block.mask b2) in
	      if Bitmap_base.is_empty mask
	      then Empty
	      else
		Block (Block.values_from_mask mask
			 (fun x -> f (pos+x) unit (Block.get_value b2 x)))
	    else map_inter_repr pos m1 (Block.get_field b2 0)
	| Block b1, Bitmap_base bmp2 ->
	    let l1 = Block.level b1 in
	    if l1 = 0
	    then
	      let mask = Bitmap_base.inter (Block.mask b1) bmp2 in
	      if Bitmap_base.is_empty mask
	      then Empty
	      else
		Block (Block.values_from_mask mask
			 (fun x -> f (pos+x) (Block.get_value b1 x) unit))
	    else map_inter_repr pos (Block.get_field b1 0) m2
	| Block b1, Block b2 ->
	    let l1, l2 = Block.level b1, Block.level b2 in
	    if l1 < l2 then map_inter_repr pos m1 (Block.get_field b2 0)
	    else if l1 > l2 then map_inter_repr pos (Block.get_field b1 0) m2
	    else (* l1 = l2 *)
	      let mask = Bitmap_base.inter (Block.mask b1) (Block.mask b2) in
	      if l1 = 0
	      then
		if Bitmap_base.is_empty mask
		then Empty
		else
		  Block (Block.values_from_mask mask
			   (fun x -> f (pos+x) (Block.get_value b1 x) (Block.get_value b2 x)))
	      else
		let bound = exp_base.(l1) in
		Block (Block.fields_from_mask l1 mask
			 (fun x1 -> Some (map_inter_repr (pos + x1 * bound) (Block.get_field b1 x1) (Block.get_field b2 x1))))
      in
      repr false (map_inter_repr 0 (obj false m1) (obj false m2)))

    let domain_union ?filter (m1 : 'a t) (m2 : 'b t) = Common.prof "Intmap.domain_union" (fun () ->
      let filter1 pos m1 v2 = match filter with None -> m1 | Some f -> domain_repr ~filter:(fun x v -> f x (Some v) (v2 (x-pos))) pos m1 in
      let filter2 pos m2 v1 = match filter with None -> m2 | Some f -> domain_repr ~filter:(fun x v -> f x (v1 (x-pos)) (Some v)) pos m2 in
      let filter12 pos m v1 v2 = match filter with None -> m | Some f -> domain_repr ~filter:(fun x v -> f x (v1 (x-pos)) (v2 (x-pos))) pos m in
      let rec filter_block1 pos b1 l1 m2 =
	let bound = exp_base.(l1) in
	let mask = Bitmap_base.add 0 (Block.mask b1) in
	Block (Block.fields_from_mask l1 mask
		 (fun x1 ->
		   if x1 = 0
		   then
		     Some (domain_union_repr pos (Block.get_field b1 0) m2)
		   else
		     Some (filter1 (pos + x1 * bound) (Block.get_field b1 x1) (fun x -> None))))
      and filter_block2 pos b2 l2 m1 =
	let bound = exp_base.(l2) in
	let mask = Bitmap_base.add 0 (Block.mask b2) in
	Block (Block.fields_from_mask l2 mask
		 (fun x2 ->
		   if x2 = 0
		   then
		     Some (domain_union_repr pos m1 (Block.get_field b2 0))
		   else
		     Some (filter2 (pos + x2 * bound) (Block.get_field b2 x2) (fun x -> None))))
      and domain_union_repr pos m1 m2 =
	match m1, m2 with
	| Empty, Empty -> Empty
	| Empty, _ -> filter2 pos m2 (fun x -> None)
	| _, Empty -> filter1 pos m1 (fun x -> None)
	| Full l1, Full l2 ->
	    if l1 < l2
	    then
	      let n1 = exp_base.(l1) in
	      filter2 pos m2 (fun x -> if x < n1 then Some unit else None)
	    else
	      let n2 = exp_base.(l2) in
	      filter1 pos m1 (fun x -> if x < n2 then Some unit else None)
	| _, Single y2 ->
	    if mem_repr y2 m1
	    then filter1 pos m1 (fun x -> if x = y2 then Some unit else None)
	    else
	      let m1' = filter1 pos m1 (fun x -> None) in
	      ( match filter with
	      | None -> add_repr y2 m1'
	      | Some f ->
		  if f y2 None (Some unit)
		  then add_repr y2 m1'
		  else m1')
	| Single y1, _ ->
	    if mem_repr y1 m2
	    then filter2 pos m2 (fun x -> if x = y1 then Some unit else None)
	    else
	      let m2' = filter2 pos m2 (fun x -> None) in
	      ( match filter with
	      | None -> add_repr y1 m2'
	      | Some f ->
		  if f y1 (Some unit) None
		  then add_repr y1 m2'
		  else m2')
	| Full l1, Bitmap_base bmp2 ->
	    filter1 pos m1 (fun x -> if Bitmap_base.mem x bmp2 then Some unit else None)
	| Bitmap_base bmp1, Full l2  ->
	    filter2 pos m2 (fun x -> if Bitmap_base.mem x bmp1 then Some unit else None)
	| Full l1, Block b2 ->
	    let l2 = Block.level b2 in
	    if l1 > l2
	    then filter1 pos m1 (fun x -> try Some (get_repr x m2) with Not_found -> None)
	    else filter_block2 pos b2 l2 m1
	| Block b1, Full l2 ->
	    let l1 = Block.level b1 in
	    if l1 < l2
	    then filter2 pos m2 (fun x -> try Some (get_repr x m1) with Not_found -> None)
	    else filter_block1 pos b1 l1 m2
	| Bitmap_base bmp1, Bitmap_base bmp2 ->
	    filter12 pos (Bitmap_base (Bitmap_base.union bmp1 bmp2))
	      (fun x -> if Bitmap_base.mem x bmp1 then Some unit else None)
	      (fun x -> if Bitmap_base.mem x bmp2 then Some unit else None)
	| Bitmap_base bmp1, Block b2 ->
	    let l2 = Block.level b2 in
	    if l2 = 0
	    then
	      let mask = Bitmap_base.union bmp1 (Block.mask b2) in
	      match filter with
	      | None -> Bitmap_base mask
	      | Some f -> Bitmap_base (Bitmap_base.filter
					 (fun x -> f (pos+x)
					     (if Bitmap_base.mem x bmp1 then Some unit else None)
					     (try Some (Block.get_value b2 x) with Not_found -> None))
					 mask)
	    else filter_block2 pos b2 l2 m1
	| Block b1, Bitmap_base bmp2 ->
	    let l1 = Block.level b1 in
	    if l1 = 0
	    then
	      let mask = Bitmap_base.union (Block.mask b1) bmp2 in
	      match filter with
	      | None -> Bitmap_base mask
	      | Some f -> Bitmap_base (Bitmap_base.filter
					 (fun x -> f (pos+x)
					     (try Some (Block.get_value b1 x) with Not_found -> None)
					     (if Bitmap_base.mem x bmp2 then Some unit else None))
					 mask)
	    else filter_block1 pos b1 l1 m2
	| Block b1, Block b2 ->
	    let l1, l2 = Block.level b1, Block.level b2 in
	    if l1 < l2 then filter_block2 pos b2 l2 m1
	    else if l1 > l2 then filter_block1 pos b1 l1 m2
	    else (* l1 = l2 *)
	      let mask = Bitmap_base.union (Block.mask b1) (Block.mask b2) in
	      if l1 = 0
	      then
		match filter with
		| None -> Bitmap_base mask
		| Some f ->
		    Bitmap_base (Bitmap_base.filter
				   (fun x -> f (pos+x)
				       (try Some (Block.get_value b1 x) with Not_found -> None)
				       (try Some (Block.get_value b2 x) with Not_found -> None))
				   mask)
	      else
		let bound = exp_base.(l1) in
		Block (Block.fields_from_mask l1 mask
			 (fun x1 ->
			   let f1_opt = try Some (Block.get_field b1 x1) with Not_found -> None in
			   let f2_opt = try Some (Block.get_field b2 x1) with Not_found -> None in
			   match f1_opt, f2_opt with
			   | None, None -> None
			   | Some f1, None -> Some (filter1 (pos + x1 * bound) f1 (fun x -> None))
			   | None, Some f2 -> Some (filter2 (pos + x1 * bound) f2 (fun x -> None))
			   | Some f1, Some f2 ->
			       Some (domain_union_repr (pos + x1 * bound) f1 f2)))
      in
      repr false (domain_union_repr 0 (obj false m1) (obj false m2)))

    let map_union f (m1 : 'a t) (m2 : 'b t) = Common.prof "Intmap.map_union" (fun () ->
      let map1 pos m1 v2 = map_repr (fun x v -> f x (Some v) (v2 (x-pos))) pos m1 in
      let map2 pos m2 v1 = map_repr (fun x v -> f x (v1 (x-pos)) (Some v)) pos m2 in
      let map12 pos m v1 v2 = map_repr (fun x v -> f x (v1 (x-pos)) (v2 (x-pos))) pos m in
      let rec map_block1 pos b1 l1 m2 =
	let bound = exp_base.(l1) in
	let mask = Bitmap_base.add 0 (Block.mask b1) in
	Block (Block.fields_from_mask l1 mask
		 (fun x1 ->
		   if x1 = 0
		   then
		     Some (map_union_repr pos (Block.get_field b1 0) m2)
		   else
		     Some (map1 (pos + x1 * bound) (Block.get_field b1 x1) (fun x -> None))))
      and map_block2 pos b2 l2 m1 =
	let bound = exp_base.(l2) in
	let mask = Bitmap_base.add 0 (Block.mask b2) in
	Block (Block.fields_from_mask l2 mask
		 (fun x2 ->
		   if x2 = 0
		   then
		     Some (map_union_repr pos m1 (Block.get_field b2 0))
		   else
		     Some (map2 (pos + x2 * bound) (Block.get_field b2 x2) (fun x -> None))))
      and map_union_repr pos m1 m2 =
	match m1, m2 with
	| Empty, Empty -> Empty
	| Empty, _ -> map2 pos m2 (fun x -> None)
	| _, Empty -> map1 pos m1 (fun x -> None)
	| Full l1, Full l2 ->
	    if l1 < l2
	    then
	      let n1 = exp_base.(l1) in
	      map2 pos m2 (fun x -> if x < n1 then Some unit else None)
	    else
	      let n2 = exp_base.(l2) in
	      map1 pos m1 (fun x -> if x < n2 then Some unit else None)
	| _, Single y2 ->
	    if mem_repr y2 m1
	    then map1 pos m1 (fun x -> if x = y2 then Some unit else None)
	    else
	      let m1' = map1 pos m1 (fun x -> None) in
	      ( match f y2 None (Some unit) with
	      | None -> m1'
	      | Some v -> set_repr y2 v m1')
	| Single y1, _ ->
	    if mem_repr y1 m2
	    then map2 pos m2 (fun x -> if x = y1 then Some unit else None)
	    else
	      let m2' = map2 pos m2 (fun x -> None) in
	      ( match f y1 (Some unit) None with
	      | None -> m2'
	      | Some v -> set_repr y1 v m2')
	| Full l1, Bitmap_base bmp2 ->
	    map1 pos m1 (fun x -> if Bitmap_base.mem x bmp2 then Some unit else None)
	| Bitmap_base bmp1, Full l2  ->
	    map2 pos m2 (fun x -> if Bitmap_base.mem x bmp1 then Some unit else None)
	| Full l1, Block b2 ->
	    let l2 = Block.level b2 in
	    if l1 > l2
	    then map1 pos m1 (fun x -> try Some (get_repr x m2) with Not_found -> None)
	    else map_block2 pos b2 l2 m1
	| Block b1, Full l2 ->
	    let l1 = Block.level b1 in
	    if l1 < l2
	    then map2 pos m2 (fun x -> try Some (get_repr x m1) with Not_found -> None)
	    else map_block1 pos b1 l1 m2
	| Bitmap_base bmp1, Bitmap_base bmp2 ->
	    map12 pos (Bitmap_base (Bitmap_base.union bmp1 bmp2))
	      (fun x -> if Bitmap_base.mem x bmp1 then Some unit else None)
	      (fun x -> if Bitmap_base.mem x bmp2 then Some unit else None)
	| Bitmap_base bmp1, Block b2 ->
	    let l2 = Block.level b2 in
	    if l2 = 0
	    then
	      let mask = Bitmap_base.union bmp1 (Block.mask b2) in
	      Block (Block.values_from_mask mask
		       (fun x -> f (pos+x)
			   (if Bitmap_base.mem x bmp1 then Some unit else None)
			   (try Some (Block.get_value b2 x) with _ -> None)))
	    else map_block2 pos b2 l2 m1
	| Block b1, Bitmap_base bmp2 ->
	    let l1 = Block.level b1 in
	    if l1 = 0
	    then
	      let mask = Bitmap_base.union (Block.mask b1) bmp2 in
	      Block (Block.values_from_mask mask
		       (fun x -> f (pos+x)
			   (try Some (Block.get_value b1 x) with _ -> None)
			   (if Bitmap_base.mem x bmp2 then Some unit else None)))
	    else map_block1 pos b1 l1 m2
	| Block b1, Block b2 ->
	    let l1, l2 = Block.level b1, Block.level b2 in
	    if l1 < l2 then map_block2 pos b2 l2 m1
	    else if l1 > l2 then map_block1 pos b1 l1 m2
	    else (* l1 = l2 *)
	      let mask = Bitmap_base.union (Block.mask b1) (Block.mask b2) in
	      if l1 = 0
	      then
		Block (Block.values_from_mask mask
			 (fun x -> f (pos+x)
			     (try Some (Block.get_value b1 x) with Not_found -> None)
			     (try Some (Block.get_value b2 x) with Not_found -> None)))
	      else
		let bound = exp_base.(l1) in
		Block (Block.fields_from_mask l1 mask
			 (fun x1 ->
			   let f1_opt = try Some (Block.get_field b1 x1) with Not_found -> None in
			   let f2_opt = try Some (Block.get_field b2 x1) with Not_found -> None in
			   match f1_opt, f2_opt with
			   | None, None -> None
			   | Some f1, None -> Some (map1 (pos + x1 * bound) f1 (fun x -> None))
			   | None, Some f2 -> Some (map2 (pos + x1 * bound) f2 (fun x -> None))
			   | Some f1, Some f2 ->
			       Some (map_union_repr (pos + x1 * bound) f1 f2)))
      in
      repr false (map_union_repr 0 (obj false m1) (obj false m2)))

    let domain_diff ?filter (m1 : 'a t) (m2 : 'b t) = Common.prof "Intmap.domain_diff" (fun () ->
      let filter1 pos m1 v2 =
	match filter with
	| None -> domain_repr ~filter:(fun x v -> (v2 (x-pos)) = None) pos m1
	| Some f -> domain_repr ~filter:(fun x v -> f x v (v2 (x-pos))) pos m1 in
      let rec filter_block1 pos b1 l1 m2 =
	let bound = exp_base.(l1) in
	let mask = Block.mask b1 in
	Block (Block.fields_from_mask l1 mask
		 (fun x1 ->
		   if x1 = 0
		   then
		     Some (domain_diff_repr pos (Block.get_field b1 0) m2)
		   else
		     Some (filter1 (pos + x1 * bound) (Block.get_field b1 x1) (fun x -> None))))
      and domain_diff_repr pos m1 m2 =
	match m1, m2 with
	| Empty, _ -> Empty
	| _, Empty -> filter1 pos m1 (fun x -> None)
	| Full l1, Full l2 ->
	    if l1 < l2
	    then
	      filter1 pos m1 (fun x -> Some unit)
	    else
	      let n2 = exp_base.(l2) in
	      filter1 pos m1 (fun x -> if x < n2 then Some unit else None)
	| _, Single y2 ->
	    filter1 pos m1 (fun x -> if x = y2 then Some unit else None)
	| Single y1, _ ->
	    filter1 pos m1 (fun x -> try Some (get_repr y1 m2) with Not_found -> None)
	| Full l1, Bitmap_base bmp2 ->
	    filter1 pos m1 (fun x -> if Bitmap_base.mem x bmp2 then Some unit else None)
	| Bitmap_base bmp1, Full l2  ->
	    filter1 pos m1 (fun x -> Some unit)
	| Full l1, Block b2 ->
	    let l2 = Block.level b2 in
	    if l1 > l2
	    then filter1 pos m1 (fun x -> try Some (get_repr x m2) with Not_found -> None)
	    else domain_diff_repr pos m1 (Block.get_field b2 0)
	| Block b1, Full l2 ->
	    let l1 = Block.level b1 in
	    if l1 < l2
	    then filter1 pos m1 (fun x -> Some unit)
	    else filter_block1 pos b1 l1 m2
	| Bitmap_base bmp1, Bitmap_base bmp2 ->
	    ( match filter with
	    | None -> Bitmap_base (Bitmap_base.diff bmp1 bmp2)
	    | Some _ -> filter1 pos m1 (fun x -> if Bitmap_base.mem x bmp2 then Some unit else None))
	| Bitmap_base bmp1, Block b2 ->
	    let l2 = Block.level b2 in
	    if l2 = 0
	    then
	      match filter with
	      | None -> Bitmap_base (Bitmap_base.diff bmp1 (Block.mask b2))
	      | Some f ->
		  Bitmap_base (Bitmap_base.filter
				 (fun x -> f (pos+x)
				     unit
				     (try Some (Block.get_value b2 x) with Not_found -> None))
				 bmp1)
	    else domain_diff_repr pos m1 (Block.get_field b2 0)
	| Block b1, Bitmap_base bmp2 ->
	    let l1 = Block.level b1 in
	    if l1 = 0
	    then
	      let mask = Block.mask b1 in
	      match filter with
	      | None -> Bitmap_base (Bitmap_base.diff mask bmp2)
	      | Some f ->
		  Bitmap_base (Bitmap_base.filter
				 (fun x -> f (pos+x)
				     (Block.get_value b1 x)
				     (if Bitmap_base.mem x bmp2 then Some unit else None))
				 mask)
	    else filter_block1 pos b1 l1 m2
	| Block b1, Block b2 ->
	    let l1, l2 = Block.level b1, Block.level b2 in
	    if l1 < l2 then domain_diff_repr pos m1 (Block.get_field b2 0)
	    else if l1 > l2 then filter_block1 pos b1 l1 m2
	    else (* l1 = l2 *)
	      let mask = Block.mask b1 in
	      if l1 = 0
	      then
		match filter with
		| None -> Bitmap_base (Bitmap_base.diff mask (Block.mask b2))
		| Some f ->
		    Bitmap_base (Bitmap_base.filter
				   (fun x -> f (pos+x)
				       (Block.get_value b1 x)
				       (try Some (Block.get_value b2 x) with Not_found -> None))
				   mask)
	      else
		let bound = exp_base.(l1) in
		Block (Block.fields_from_mask l1 mask
			 (fun x1 ->
			   let f1 = Block.get_field b1 x1 in
			   let f2 = Block.get_field b2 x1 in
			   Some (domain_diff_repr (pos + x1 * bound) f1 f2)))
      in
      repr false (domain_diff_repr 0 (obj false m1) (obj false m2)))


    let map_diff f (m1 : 'a t) (m2 : 'b t) = Common.prof "Intmap.map_diff" (fun () ->
      let map1 pos m1 v2 = map_repr (fun x v -> f x v (v2 (x-pos))) pos m1 in
      let rec map_block1 pos b1 l1 m2 =
	let bound = exp_base.(l1) in
	let mask = Block.mask b1 in
	Block (Block.fields_from_mask l1 mask
		 (fun x1 ->
		   if x1 = 0
		   then
		     Some (map_diff_repr pos (Block.get_field b1 0) m2)
		   else
		     Some (map1 (pos + x1 * bound) (Block.get_field b1 x1) (fun x -> None))))
      and map_diff_repr pos m1 m2 =
	match m1, m2 with
	| Empty, _ -> Empty
	| _, Empty -> map1 pos m1 (fun x -> None)
	| Full l1, Full l2 ->
	    if l1 < l2
	    then
	      map1 pos m1 (fun x -> Some unit)
	    else
	      let n2 = exp_base.(l2) in
	      map1 pos m1 (fun x -> if x < n2 then Some unit else None)
	| _, Single y2 ->
	    map1 pos m1 (fun x -> if x = y2 then Some unit else None)
	| Single y1, _ ->
	    map1 pos m1 (fun x -> try Some (get_repr y1 m2) with Not_found -> None)
	| Full l1, Bitmap_base bmp2 ->
	    map1 pos m1 (fun x -> if Bitmap_base.mem x bmp2 then Some unit else None)
	| Bitmap_base bmp1, Full l2  ->
	    map1 pos m1 (fun x -> Some unit)
	| Full l1, Block b2 ->
	    let l2 = Block.level b2 in
	    if l1 > l2
	    then map1 pos m1 (fun x -> try Some (get_repr x m2) with Not_found -> None)
	    else map_diff_repr pos m1 (Block.get_field b2 0)
	| Block b1, Full l2 ->
	    let l1 = Block.level b1 in
	    if l1 < l2
	    then map1 pos m1 (fun x -> Some unit)
	    else map_block1 pos b1 l1 m2
	| Bitmap_base bmp1, Bitmap_base bmp2 ->
	    map1 pos m1 (fun x -> if Bitmap_base.mem x bmp2 then Some unit else None)
	| Bitmap_base bmp1, Block b2 ->
	    let l2 = Block.level b2 in
	    if l2 = 0
	    then
	      let mask = bmp1 in
	      Block (Block.values_from_mask mask
		       (fun x -> f (pos+x)
			   unit
			   (try Some (Block.get_value b2 x) with Not_found -> None)))
	    else map_diff_repr pos m1 (Block.get_field b2 0)
	| Block b1, Bitmap_base bmp2 ->
	    let l1 = Block.level b1 in
	    if l1 = 0
	    then
	      let mask = Block.mask b1 in
	      Block (Block.values_from_mask mask
		       (fun x -> f (pos+x)
			   (Block.get_value b1 x)
			   (if Bitmap_base.mem x bmp2 then Some unit else None)))
	    else map_block1 pos b1 l1 m2
	| Block b1, Block b2 ->
	    let l1, l2 = Block.level b1, Block.level b2 in
	    if l1 < l2 then map_diff_repr pos m1 (Block.get_field b2 0)
	    else if l1 > l2 then map_block1 pos b1 l1 m2
	    else (* l1 = l2 *)
	      let mask = Block.mask b1 in
	      if l1 = 0
	      then
		Block (Block.values_from_mask mask
			 (fun x -> f (pos+x)
			     (Block.get_value b1 x)
			     (try Some (Block.get_value b2 x) with Not_found -> None)))
	      else
		let bound = exp_base.(l1) in
		Block (Block.fields_from_mask l1 mask
			 (fun x1 ->
			   let f1 = Block.get_field b1 x1 in
			   let f2 = Block.get_field b2 x1 in
			   Some (map_diff_repr (pos + x1 * bound) f1 f2)))
      in
      repr false (map_diff_repr 0 (obj false m1) (obj false m2)))

    let memory_size ?(f = fun _ -> 0) m =
      let rec memory_size_repr = function
	| Empty -> 1
	| Full _ -> 1
	| Single _ -> 1
	| Bitmap_base _ -> 1
	| Block b ->
	    let level = Block.level b in
	    if level = 0
	    then 
	      Block.fold_values
		(fun res v -> res + f v)
		3 (* reference + tag + mask *)
		b
	    else
	      Block.fold_fields
		(fun res m_i -> res + memory_size_repr m_i)
		3 (* reference + tag + mask *)
		b
      in
      memory_size_repr (obj false m)


  end

(* automatic testing by comparison *)

module Comparator (A : T) (B : T) =
  struct
    exception Fail of string * string

    module Wrapper (X : T) =
      struct
	let list m = List.sort Pervasives.compare (X.fold (fun res x v -> (x,v)::res) [] m)

	let string l = "{" ^ String.concat "," (List.map (fun (x,_) -> string_of_int x) l) ^ "}"

	let list_string m = let l = list m in l, string l

	let bound = 5000

	let int_random () = Random.int bound

	let random () =
	  let m = ref X.empty (* if Random.bool () then X.empty else X.singleton (int_random ())*) in
	  let tr = Buffer.create bound in
	  try
	    for i = 1 to bound do
	      let i = int_random () in
	      if Random.bool ()
	      then begin
		Buffer.add_char tr '+'; Buffer.add_string tr (string_of_int i);
		m := X.set i () !m
	        (* m := X.add i !m *) end
	      else begin
		Buffer.add_char tr '-'; Buffer.add_string tr (string_of_int i);
		m := X.remove i !m end
	    done;
	    Buffer.contents tr, !m
	  with e -> raise (Fail (Buffer.contents tr, Printexc.to_string e))
	
	let apply name f seed =
	  Random.init seed;
	  let tr, m = random () in
(*	  let tr' = name ^ "(" ^ tr ^ ")" in *)
	  let tr' = name ^ "(" ^ string (list m) ^ ")" in
	  try
	    let res, s_res = f m in
	    tr' ^ " = " ^ s_res, res
	  with e -> raise (Fail (tr', Printexc.to_string e))

	let apply2 name f seed =
	  Random.init seed;
	  let tr1, m1 = random () in
	  let tr2, m2 = random () in
(*	  let tr = name ^ "(" ^ tr1 ^ "," ^ tr2 ^ ")" in *)
	  let tr = name ^ "(\n" ^ string (list m1) ^ ",\n" ^ string (list m2) ^ ")" in
	  try
	    let res, s_res = f m1 m2 in
	    tr ^ " =\n" ^ s_res, res
	  with e -> raise (Fail (tr, Printexc.to_string e))

	let is_even = fun i _ -> i mod 2 = 0
	let is_even2 = fun i _ _ -> i mod 2 = 0
	let is_even2_diff = fun i _ v2 -> v2 = None || i mod 2 = 0
	let if_even = fun i v -> if i mod 2 = 0 then Some v else None
	let if_even2 = fun i _ _ -> if i mod 2 = 0 then Some () else None
	let if_even2_diff = fun i _ v2 -> if v2 = None || i mod 2 = 0 then Some () else None

	let create = apply "list" list_string
	let cardinal = apply "cardinal" (fun m -> let n = X.cardinal m in n, string_of_int n)
	let is_empty = apply "is_empty" (fun m -> let b = X.is_empty m in b, string_of_bool b)
	let mem = apply "mem" (fun m -> let b = X.mem (int_random ()) m in b, string_of_bool b)
	let get = apply "get" (fun m -> (try Some (X.get (int_random ()) m) with Not_found -> None), "?")
	let domain = apply "domain" (fun m -> list_string (X.domain ~filter:is_even m))
	let map = apply "map" (fun m -> list_string (X.map if_even m))
	let subset = apply2 "subset"
	    (fun m1 m2 ->
	      let m1' = if Random.bool () then m1 else X.map_inter (fun i v1 v2 -> Some ()) m1 m2 in
	      (* to have a chance that subset returns true *)
	      let b = X.subset ~filter:is_even2 m1' m2 in
	    b, string_of_bool b)
	let domain_inter = apply2 "domain_inter" (fun m1 m2 -> list_string (X.domain_inter ~filter:is_even2 m1 m2))
	let map_inter = apply2 "map_inter" (fun m1 m2 -> list_string (X.map_inter if_even2 m1 m2))
	let domain_union = apply2 "domain_union" (fun m1 m2 -> list_string (X.domain_union ~filter:is_even2 m1 m2))
	let map_union = apply2 "map_union" (fun m1 m2 -> list_string (X.map_union if_even2 m1 m2))
	let domain_diff = apply2 "domain_diff" (fun m1 m2 -> list_string (X.domain_diff ~filter:is_even2_diff m1 m2))
	let map_diff = apply2 "map_diff" (fun m1 m2 -> list_string (X.map_diff if_even2_diff m1 m2))
      end

    module AX = Wrapper (A)
    module BX = Wrapper (B)

    let main duration =
      let comp opA opB =
	let seed = Random.int 1000000000 in
	let trA, resA = try opA seed with Fail (tr, e) -> raise (Fail ("A." ^ tr, e)) in
	let trB, resB = try opB seed with Fail (tr, e) -> raise (Fail ("B." ^ tr, e)) in
	if resA = resB
	then begin print_string "-"; flush stdout end (* print_endline "OK" *)
	else begin print_string "\nError: "; print_endline trA; print_endline trB end
      in
      while Sys.time () < duration do
	match Random.int 2 with
	| 0 -> comp AX.create BX.create
	| 1 -> comp AX.cardinal BX.cardinal
	| 2 -> comp AX.is_empty BX.is_empty
	| 3 -> comp AX.mem BX.mem
	| 4 -> comp AX.get BX.get
	| 5 -> comp AX.domain BX.domain
	| 6 -> comp AX.map BX.map
	| 7 -> comp AX.subset BX.subset
	| 8 -> comp AX.domain_inter BX.domain_inter
	| 9 -> comp AX.map_inter BX.map_inter
	| 10 -> comp AX.domain_union BX.domain_union
	| 11 -> comp AX.map_union BX.map_union
	| 12 -> comp AX.domain_diff BX.domain_diff
	| 13 -> comp AX.map_diff BX.map_diff
	| _ -> ()
      done;
      print_newline ()

  end

module Comp = Comparator (Std) (M)

(* uncomment for automatic testing *)
(* let _ = Comp.main (float_of_string Sys.argv.(1)) *)

(* for testing *)
(*
open M

(*
let x = ref empty

let ask f = f !x

let tell f = x := f !x
*)

let rec range_aux acc a b =
  if a = b
  then acc
  else range_aux (add a acc) (a+1) b
let range = range_aux empty

let rec id_aux acc a b =
  if a = b
  then acc
  else id_aux (set a a acc) (a+1) b
let id = id_aux empty

let rec range2_aux acc a b =
  if a = b
  then acc
  else range2_aux (set a (range a b) acc) (a+1) b
let range2 = range2_aux empty

let list m = fold (fun res x v -> (x,v)::res) [] m
*)
