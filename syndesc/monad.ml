
module MOption =
  struct
    type 'a t = 'a option
    let some x = Some x
    let none = None
    let bind m f =
      match m with
      | None -> None
      | Some x -> f x
    let plus m1 fm2 =
      match m1 with
      | None -> fm2 ()
      | _ -> m1
  end

module MList =
  struct
    type 'a t = 'a list
    let one x = [x]
    let zero = []
    let bind m f =
      List.concat (List.map f m)
  end

