(** Common utility functions. *)

(* space *)

let heap_size () : float = float_of_int (Gc.quick_stat ()).Gc.heap_words *. float_of_int (Sys.word_size / 8)  (* in bytes *)

let live_words () : float = float_of_int (Gc.stat ()).Gc.live_words *. float_of_int (Sys.word_size / 8)  (* in bytes *)

(* results: value or exception *)

type 'a result = Val of 'a | Exn of exn

let get_result (f : unit -> 'a) : 'a result =
  try Val (f ())
  with e -> Exn e

let result_run = function
  | Val v -> v
  | Exn e -> raise e

let result_return (v : 'a) : 'a result = Val v

let result_bind (r : 'a result) (f : 'a -> 'b result) : 'b result =
  match r with
  | Val v -> f v
  | Exn n -> r

		
(* extensions a Weak *)

let weak_get_index : 'a Weak.t ref -> int =
  fun w ->
    let l = Weak.length !w in
    let i = ref 0 in
    while !i < l && Weak.check !w !i do incr i done;
    if !i >= l then begin
      let ar = Weak.create (l + 10)
      in Weak.blit !w 0 ar 0 l; w := ar end;
    !i

let weak_add : 'a Weak.t ref -> 'a -> unit =
  fun w x ->
    let i = weak_get_index w in
    Weak.set !w i (Some x)
      
let weak_iter : 'a Weak.t -> ('a -> unit) -> unit =
  fun w f ->
    for i=0 to Weak.length w - 1 do
      match Weak.get w i with
	None -> ()
      | Some x -> f x
    done

let list_of_weak : 'a Weak.t -> 'a list =
  fun w ->
    let res = ref [] in
    for i=0 to Weak.length w - 1 do
      match Weak.get w i with
	None -> ()
      |	Some x -> res := x::!res
    done;
    !res

(* counter *)

class ['a] counter =
object
  val ht : ('a, int ref) Hashtbl.t = Hashtbl.create 11
  method add (x : 'a) : unit =
    let cpt =
      try Hashtbl.find ht x
      with Not_found ->
	let cpt = ref 0 in
	Hashtbl.add ht x cpt;
	cpt in
    incr cpt
  method count (x : 'a) : int =
    try !(Hashtbl.find ht x)
    with Not_found -> 0
  method fold : 'c. ('c -> 'a -> int -> 'c) -> 'c -> 'c =
    fun f init ->
    Hashtbl.fold
      (fun x cpt res -> f res x !cpt)
      ht init
  method most_frequents : int * 'a list =
    Hashtbl.fold
      (fun x cpt (c',xs' as res) ->
       let c = !cpt in
       if c = c' then (c', x::xs')
       else if c > c' then (c,[x])
       else res)
      ht (0,[])
end

(* memoization *)

let memoize ~(size : int) (f : 'a -> 'b) : ('a -> 'b) * (unit -> unit) =
  let ht : ('a, 'b) Hashtbl.t = Hashtbl.create size in
  let reset () = Hashtbl.clear ht in
  let memoized_f =
    fun x ->
    match Hashtbl.find_opt ht x with
    | Some y -> y
    | None ->
       let y = f x in
       Hashtbl.add ht x y;
       y
  in
  memoized_f, reset

let memoize2 ~size f2 =
  let mem_f2, reset = memoize ~size (fun (x1,x2) -> f2 x1 x2) in
  (fun x1 x2 -> mem_f2 (x1,x2)), reset
let memoize3 ~size f3 =
  let mem_f3, reset = memoize ~size (fun (x1,x2,x3) -> f3 x1 x2 x3) in
  (fun x1 x2 x3 -> mem_f3 (x1,x2,x3)), reset
let memoize4 ~size f4 =
  let mem_f4, reset = memoize ~size (fun (x1,x2,x3,x4) -> f4 x1 x2 x3 x4) in
  (fun x1 x2 x3 x4 -> mem_f4 (x1,x2,x3,x4)), reset
  
(* List functionals *)
(* ---------------- *)

let iter_option : ('a -> unit) -> 'a option -> unit =
  fun f -> function
    | None -> ()
    | Some x -> f x

let fold_option : ('a -> 'b) -> 'b -> 'a option -> 'b =
  fun f e -> function
    | None -> e
    | Some x -> f x

let flat_option : 'a -> 'a option -> 'a =
  fun e -> function
    | None -> e
    | Some x -> x

let map_option : ('a -> 'b) -> 'a option -> 'b option =
  fun f -> function
    | None -> None
    | Some x -> Some (f x)

let rec filter : 'a option list -> 'a list =
  function
    | [] -> []
    | None::l -> filter l
    | Some x::l -> x::filter l

let rec remove_first : 'a -> 'a list -> 'a list =
  fun e -> function
    | [] -> []
    | x::l ->
	if x = e
	then l
	else x::remove_first e l

let rec mapfilter : ('a -> 'b option) -> 'a list -> 'b list =
  fun f -> function
      [] -> []
    | x::l -> match f x with
	None -> mapfilter f l
      |	Some y -> y::mapfilter f l

let rec mapfind : ('a -> 'b option) -> 'a list -> 'b =
  fun f -> function
  | [] -> raise Not_found
  | x::l -> match f x with
      | None -> mapfind f l
      | Some y -> y

let rec mapfind_right : ('a -> 'b option) -> 'a list -> 'b =
  fun f -> function
    | [] -> raise Not_found
    | x::l ->
	try mapfind_right f l
	with Not_found ->
	  match f x with
	  | None -> raise Not_found
	  | Some y -> y

let rec fold_while : ('a -> 'a option) -> 'a -> 'a =
  fun f e ->
    match f e with
    | None -> e
    | Some e' -> fold_while f e'

let fold_for : (int -> 'a -> 'a) -> int -> int -> 'a -> 'a =
  fun f a b e ->
    let res = ref e in
    for x = a to b do
      res := f x !res
    done;
    !res

let fold_for_down : (int -> 'a -> 'a) -> int -> int -> 'a -> 'a =
  fun f a b e ->
    let res = ref e in
    for x = a downto b do
      res := f x !res
    done;
    !res

let rec fold_in_channel : ('b -> string -> 'b) -> 'b -> in_channel -> 'b =
  fun f e ch ->
    try fold_in_channel f (f e (input_line ch)) ch
    with End_of_file -> e

let rec insert : ('a -> 'a -> bool) -> 'a -> 'a list -> 'a list =
  fun order x ->
    function
    | [] -> [x]
    | y::ys ->
       if order x y
       then x::y::ys
       else y::insert order x ys

let rec insert_max : ('a -> 'a -> bool) -> 'a -> 'a list -> 'a list =
  fun order x ->
    function
      | [] -> [x]
      | y::ys ->
	  if order x y then y::ys
	  else if order y x then insert_max order x ys
	  else y::insert_max order x ys

let rec merge_max : ('a -> 'a -> bool) -> 'a list -> 'a list -> 'a list =
  fun order l1 l2 ->
    List.fold_left
      (fun res x2 -> insert_max order x2 res)
      l1 l2

(* fold on all ordered pairs of a list *)
let rec fold_pair : ('a -> 'a -> 'b -> 'b) -> 'a list -> 'b -> 'b =
  fun f l e ->
    match l with
    | [] -> e
    | x1::xs ->
       List.fold_right
         (fun x2 res -> f x1 x2 res)
         xs
         (fold_pair f xs e)

let compare_pair : ('a -> 'a -> int) * ('b -> 'b -> int) -> 'a * 'b -> 'a * 'b -> int =
  fun (comp1,comp2) (a1,a2) (b1,b2) ->
    match comp1 a1 b1 with
    | 0 -> comp2 a2 b2
    | c1 -> c1

let rec scramble : 'a list -> 'a list =
  function
  | [] -> []
  | x::l ->
     let l' = scramble l in
     if Random.int 2 = 0
     then x::l'
     else l'@[x]

let rec scrambles : 'a list -> int -> 'a list =
  fun l -> function
  | 0 -> l
  | n -> scrambles (scramble l) (n-1)

let list_count (pred : 'a -> bool) (l : 'a list) : int =
  List.fold_left
    (fun res x -> if pred x then 1+res else res)
    0 l
  
let rec sub_list l pos len =
  if pos = 0
  then
    if len = 0
    then []
    else
      match l with
      | [] -> []
      | x::xs -> x::sub_list xs 0 (len-1)
  else
    match l with
    | [] -> []
    | x::xs -> sub_list xs (pos-1) len

let rec list_set_nth l n x =
  match l with
  | [] -> invalid_arg "Common.list_set_nth"
  | e::l' ->
      if n = 0
      then x::l'
      else e::list_set_nth l' (n-1) x

let rec list_insert_nth l n l1 =
  match l with
  | [] -> invalid_arg "Common.list_insert_nth"
  | e::l' ->
      if n = 0
      then l1 @ l'
      else e :: list_insert_nth l' (n-1) l1

let rec list_remove_nth l n =
  match l with
  | [] -> invalid_arg "Common.list_remove_nth"
  | e::l' ->
      if n = 0
      then l'
      else e::list_remove_nth l' (n-1)

let list_pos x l =
  let rec aux pos = function
    | [] -> raise Not_found
    | y::ys -> if x=y then pos else aux (pos+1) ys
  in
  aux 0 l
	
let list_n n = (* generates list [0, .., n-1] *)
  let rec aux i acc =
    if i = 0
    then acc
    else aux (i-1) ((i-1)::acc)
  in
  aux n []

let rec list_index i = function
  | [] -> []
  | x::l -> (i,x)::list_index (i+1) l

let pivot_partition comp x l =
  List.fold_left
    (fun (lt,eq,gt) y ->
      let c = comp x y in
      if c < 0 then (y::lt,eq,gt)
      else if c = 0 then (lt,y::eq,gt)
      else (* c > 0 *) (lt,eq,y::gt))
    ([],[],[]) l
    
let rec sort_equiv (comp : 'a -> 'a -> int) : 'a list -> 'a list list = function
  | [] -> []
  | [x] -> [[x]]
  | x::l ->
    let lt, eq, gt = pivot_partition comp x l in
    let sorted_lt = sort_equiv comp lt in
    let sorted_gt = sort_equiv comp gt in
    sorted_lt @ (x::eq) :: sorted_gt

let group_by (key : 'a -> 'b) (l : 'a list) : ('b * 'a list) list =
  let ht : ('b, 'a list) Hashtbl.t =
    Hashtbl.create (List.length l) in
  List.iter
    (fun v ->
     let k = key v in
     match Hashtbl.find_opt ht k with
     | None -> Hashtbl.add ht k [v]
     | Some lv -> Hashtbl.replace ht k (v::lv))
    l;
  Hashtbl.fold
    (fun k lv res -> (k,lv)::res)
    ht []
			     
let rec extract_min (compare : 'a -> 'a -> int) : 'a list -> 'a * 'a list = function
  | [] -> invalid_arg "Common.extract_min: empty list"
  | [x] -> x, []
  | x::y::l ->
    if compare x y <= 0
    then
      let best, others = extract_min compare (x::l) in
      best, y::others
    else
      let best, others = extract_min compare (y::l) in
      best, x::others

let rec list_product (lseq : 'a list list) : 'a list list =
  match lseq with
  | [] -> [[]]
  | s::ls ->
     s
     |> List.map
          (fun x ->
            list_product ls
            |> List.map (fun lx -> x::lx))
     |> List.concat

      
(* utilities on streams *)

let rec stream_map f = parser
  | [<'x; str>] -> [<'f x; stream_map f str>]
  | [<>] -> [<>]

(* time *)

let utime () : float = (Unix.times ()).Unix.tms_utime [@@inline] (* in seconds *)

let chrono (f : unit -> 'a) : float * 'a result =
  let t1 = utime () in
  let res = get_result f in
  let t2 = utime () in
  t2 -. t1, res

exception Timeout
let sigalrm_handler = Sys.Signal_handle (fun _ -> raise Timeout)
let do_timeout (dur : int) (f : unit -> 'a) : 'a option =
   let old_behavior = Sys.signal Sys.sigalrm sigalrm_handler in
   let reset_alarm () =
     ignore (Unix.alarm 0); (* cancel running alarm *)
     Sys.set_signal Sys.sigalrm old_behavior (* restore default behaviour *)
   in
   try
     ignore (Unix.alarm dur);
     Some (Fun.protect f ~finally:reset_alarm)
   with Timeout ->
     reset_alarm ();
     None

let do_timeout_gc (dur : float (* sec *)) (f : unit -> 'a) : 'a option = (* more reliable than Sys.sigalrm *)
  let start = utime () in
  let alarm =
    Gc.create_alarm
      (fun () ->
        let time = utime () in
        if time -. start > dur then raise Timeout) in
  try Some (Fun.protect f
              ~finally:(fun () -> Gc.delete_alarm alarm))
  with Timeout -> None

(* memory *)

let do_memout (max_mem : int (* Mbytes *)) (f : unit -> 'a) : 'a option =
  let alarm =
    Gc.create_alarm
      (fun () ->
        let mem = Gc.(quick_stat ()).heap_words / 1000000 * (Sys.word_size / 8) in (* in Mbytes *)
        if mem > max_mem then raise Out_of_memory) in
  try Some (Fun.protect f
              ~finally:(fun () -> Gc.delete_alarm alarm))
  with Out_of_memory -> None

(* for profiling *)

let prof_on = ref true
	      
type prof_elem = {mutable prof_on : bool; mutable prof_nb : int; mutable prof_time : float; mutable prof_mem : int}
let tbl_prof : (string, prof_elem) Hashtbl.t = Hashtbl.create 100

let prof : string -> (unit -> 'a) -> 'a =
  fun s f ->
  if not !prof_on
  then f ()
  else	       
    let elem =
      try Hashtbl.find tbl_prof s
      with Not_found ->
	let elem = {prof_on = false; prof_nb = 0; prof_time = 0.; prof_mem = 0} in
	Hashtbl.add tbl_prof s elem;
	elem in
    let on = elem.prof_on in
(* print_string ("<"^s^":"); flush stdout; *)
    elem.prof_on <- true;
    (*let m1 = (Gc.quick_stat ()).Gc.heap_words / 8 in*)
    let d, res = chrono f in
    (*let m2 = (Gc.quick_stat ()).Gc.heap_words / 8 in*)
    elem.prof_on <- on;
    elem.prof_nb <- elem.prof_nb + 1;
    if not on then elem.prof_time <- elem.prof_time +. d;
    (*if not on then elem.prof_mem <- elem.prof_mem + (m2 - m1);*)
(* print_string (s^">\n"); flush stdout; *)
    result_run res
	       
let prerr_profiling () =
  if !prof_on then (
    Printf.eprintf "// Profiling...\n";
    Printf.eprintf "// #calls\t#calls/s\tTime (s)\tPercentage\tSection\n";
    let l, max_t =
      Hashtbl.fold
	(fun s elem (l,max_t) ->
	 (elem.prof_time, elem.prof_nb, elem.prof_mem,s)::l,
	 max max_t elem.prof_time)
	tbl_prof ([],0.) in
    let l = List.sort Stdlib.compare l in
    List.iter
      (fun (t,n,m,s) ->
       (*     Printf.eprintf "// %s: %d calls, %.1f seconds\n" s n t*)
       Printf.eprintf "// %9d\t%10.1f\t%7.1f\t%5.1f%%\t%s\n" n (float n /. t) t (100. *. t /. max_t) s)
      l;
    flush stderr
  )

(* utilities on files *)

(* found at http://pauillac.inria.fr/~remy/poly/system/camlunix/fich.html#toc13 *)
let file_copy input_name output_name =
  let buffer_size = 8192 in
  let buffer = Bytes.create buffer_size in
  let fd_in = Unix.openfile input_name [Unix.O_RDONLY] 0 in
  let fd_out = Unix.openfile output_name [Unix.O_WRONLY; Unix.O_CREAT; Unix.O_TRUNC] 0o666 in
  let rec copy_loop () =
    match Unix.read fd_in buffer 0 buffer_size with
    | 0 -> ()
    | r -> ignore (Unix.write fd_out buffer 0 r); copy_loop () in
  copy_loop ();
  Unix.close fd_in;
  Unix.close fd_out

let string_of_file (filename : string) : string =
  let ch = open_in filename in
  let buf = Buffer.create 10000 in
  fold_in_channel
    (fun () line -> Buffer.add_string buf line; Buffer.add_char buf '\n')
    () ch;
  close_in ch;
  Buffer.contents buf

(* safe rewrite of a file *)
let safe_file_update path f =
  let path_tmp = Filename.concat (Filename.dirname path) ("tmp_" ^ Filename.basename path) in
  f path_tmp;
  (try Sys.remove path with _ -> ());
  Sys.rename path_tmp path

(*
let pipe_command (cmd : string) (f : in_channel -> 'a) : 'a =
  let path = Filename.temp_file "cmd" ".txt" in
  let code = Sys.command (cmd ^ " > " ^ path) in
  if code <> 0
  then raise (Sys_error ("error while executing: " ^ cmd))
  else begin
    let ch = open_in path in
    let res = f ch in
    close_in ch;
    Sys.remove path;
    res
  end
*)

let pipe_command (cmd : string) (f : in_channel -> 'a) : 'a =
  let ch = Unix.open_process_in cmd in
  let res = f ch in
  let status = Unix.close_process_in ch in
  if status = Unix.WEXITED 0
  then res
  else raise (Sys_error ("Common.pipe_command: error while executing: " ^ cmd))

(* probabilities *)

open Num

let comb_tbl : (int*int,num) Hashtbl.t = Hashtbl.create 10000
let rec comb (k,n) =
  if k > n || n < 0 then Int 0
  else if k = n || k = 0 then Int 1
  else if k > n / 2 then comb (n-k,n)
  else
    try Hashtbl.find comb_tbl (k,n)
    with Not_found ->
      let res = comb (k,n-1) +/ comb (k-1,n-1) in
      Hashtbl.add comb_tbl (k,n) res;
      res

let chance_eq_num (r,w) (k,n) =
  comb (k,r) */ comb (n-k,w-r) // comb (n,w)

let chance_eq (r,w) (k,n) = prof "chance_eq" (fun () ->
  float_of_num (chance_eq_num (r,w) (k,n)))

let chance_ge_num (r,w) (k,n) =
  let res = ref (Int 0) in
  for tp = k to r do
    for fp = n-k downto 0 do
      res := !res +/ chance_eq_num (r,w) (tp,tp+fp)
    done
  done;
  !res

let chance_ge (r,w) (k,n) = prof "chance_ge" (fun () ->
  float_of_num (chance_ge_num (r,w) (k,n)))

(* sample reservoir *)
(* SEE: https://en.wikipedia.org/wiki/Reservoir_sampling *)

class ['a] sample_reservoir (size : int) =
object
  val sample : 'a option array = Array.make size None
  val mutable rank = 0

  method add (x : 'a) : unit =
    rank <- rank+1;
    if rank <= size
    then sample.(rank-1) <- Some x
    else
      if Random.int rank < size
      then sample.(Random.int size) <- Some x
      else ()
  method fold : 'b. ('b -> 'a -> 'b) -> 'b -> 'b =
    fun f init ->
    Array.fold_left
      (fun res ->
       function
       | None -> res
       | Some x -> f res x)
      init sample
end

class ['a] weighted_sample_reservoir_res (size: int) =
object
  val mutable sample : (float * 'a) Bintree.t = Bintree.empty

  method add ~(weight : float) (x : 'a) : unit =
    let r = Random.float 1. ** (1. /. weight) in
    let rx = (r,x) in
    if Bintree.cardinal sample < size
    then sample <- Bintree.add rx sample
    else
      let rx_min = Bintree.min_elt sample in
      if fst rx_min < r
      then sample <- Bintree.add rx (Bintree.remove_min_elt sample)
      else ()
  method fold : 'b. ('b -> 'a -> 'b) -> 'b -> 'b =
    fun f init ->
    let _, _, res =
      fold_while
	(fun (sample_rest,k,res) ->
	 if k=0 || Bintree.is_empty sample_rest
	 then None
	 else
	   let _, x = Bintree.max_elt sample_rest in
	   Some (Bintree.remove_max_elt sample_rest, k-1, f res x))
	(sample,size,init) in
    res
end

class ['a] weighted_sample_reservoir_chao (size: int) =
object
  val sample : 'a option array = Array.make size None
  val mutable rank = 0
  val mutable total_weight = 0.

  method add ~(weight : float) (x : 'a) : unit =
    rank <- rank+1;
    total_weight <- total_weight +. weight;
    if rank <= size
    then sample.(rank-1) <- Some x
    else
      let p = weight /. total_weight in
      if Random.float 1. <= p
      then sample.(Random.int size) <- Some x
      else ()
  method fold : 'b. ('b -> 'a -> 'b) -> 'b -> 'b =
    fun f init ->
    Array.fold_left
      (fun res ->
       function
       | None -> res
       | Some x -> f res x)
      init sample
end

class virtual ['a] folder =
  object
    method virtual fold : 'c. ('c -> 'a -> 'c) -> 'c -> 'c
  end
  
let sample_fold ?(size : int option)
		(folder : 'a folder)
		(f : 'b -> 'a -> 'b) (init : 'b) : 'b =
  match size with
  | None -> folder#fold f init
  | Some size ->
     let sample = new sample_reservoir size in
     let () =
       folder#fold
	 (fun () x -> sample#add x)
	 () in
     sample#fold f init

let random_choice (folder : 'a folder) : 'a option =
  let sample = new sample_reservoir 1 in
  let () =
    folder#fold
      (fun () x -> sample#add x)
      () in
  sample#fold (fun res x -> Some x) None

let list_sample ~size (l : 'a list) : 'a list =
  let sample = new sample_reservoir size in
  List.iter (fun x -> sample#add x) l;
  sample#fold (fun res x -> x::res) []
	      
(* external applications *)

let xemacs filename pattern =
  ignore (Sys.command ("xemacs -eval '(progn (find-file \""^filename^"\") (search-forward \"" ^ pattern ^ "\"))' &"))

let mozilla url =
  ignore (Sys.command ("mozilla -remote \"openurl(" ^ url ^ ")\""))
    
let gqview filename =
  ignore (Sys.command ("gqview \"" ^ String.escaped filename ^ "\" &"))

let xmms filename =
  ignore (Sys.command ("xmms \"" ^ String.escaped filename ^ "\" &"))

let cyg2win path = (* convert a Cygwin path to a Windows path *)
  match Str.split (Str.regexp "/") path with
  | "cygdrive"::drive::l -> String.uppercase_ascii drive ^ ":\\" ^ String.concat "\\" l
  | l -> "C:\\cygwin\\" ^ String.concat "\\" l

let acdsee_cygwin filename =
  ignore (Sys.command ("/cygdrive/c/Program\\ Files/ACD\\ Systems/ACDSee\\ Trial\\ Version/ACDSee.exe /v \"" ^ cyg2win filename ^ "\" &"))

let irfanview_cygwin filename =
  ignore (Sys.command ("/cygdrive/c/Program\\ Files/IrfanView/i_view32.exe \"" ^ cyg2win filename ^ "\" &"))

let irfanslideshow_cygwin filename =
  ignore (Sys.command ("/cygdrive/c/Program\\ Files/IrfanView/i_view32.exe /slideshow=\"" ^ cyg2win filename ^ "\" &"))

let winamp_cygwin filename =
   ignore (Sys.command ("/cygdrive/c/Program\\ Files/Winamp/winamp.exe \"" ^ cyg2win filename ^ "\" &"))
