(* Utilities for MDL-based approaches *)

type bits = float

let log_2 = log 2.
let log2 (x : float) : float =
  log x /. log_2

(* log2 gamma(n + 0.5) *)
let rec log2_gamma_half (n : int) : float =
  assert (n >= 0);
  if n = 0
  then log2 (sqrt Float.pi)
  else log2 (float n -. 1. +. 0.5) +. log2_gamma_half (n-1)
let log2_gamma_half, reset_log2_gamma_half =
  Common.memoize ~size:103 log2_gamma_half

(* log2 gamma(n) *)
let rec log2_gamma_round (n : int) : float =
  assert (n >= 1);
  if n = 1
  then 0.
  else log2 (float (n-1)) +. log2_gamma_round (n-1)
let log2_gamma_round, reset_log2_gamma_round =
  Common.memoize ~size:103 log2_gamma_round

(* log2 (gamma(n + 0.5k) / gamma(0.5k)) *)
let log2_gamma_ratio (n : int) (k : int) : float =
  assert (n >= 0);
  assert (k >= 1);
  let k2 = k / 2 in
  if k mod 2 = 0
  then log2_gamma_round (n + k2) -. log2_gamma_round k2
  else log2_gamma_half (n + k2) -. log2_gamma_half k2


(* sum of a list *)
let sum (lx : 'a list) (l : 'a -> bits) : bits =
  List.fold_left (fun res x -> res +. l x) 0. lx

module Code =
  struct
(* universal encoding of a natural number, starting at 0 *)
let universal_int_star (n : int) : bits =
  if n < 0 then invalid_arg "Mdl.Code.universal_int_star: negative integer";
  2. *. log2 (float (n+1)) +. 1.

(* universal encoding of a natural number, starting at 1 *)
let universal_int_plus (n : int) : bits =
  if n < 1 then invalid_arg "Mdl.Code.universal_int_plus: nul or negative integer";
  2. *. log2 (float n) +. 1.

(* encoding for uniform distribution over [n] elements *)
let uniform (n : int) : bits =
  if n < 1 then invalid_arg "Mdl.Code.uniform: nul or negative integer";
  log2 (float n)

(* encoding according to usage as a proportion/probability *)
let usage (p : float) : bits =
  if p < 0. || p > 1. then invalid_arg "Mdl.Code.usage: value out of [0,1]";
  -. log2 p

(* encoding of the selection of [k] elements among [n] *)
let comb (k : int) (n : int) : bits =
  if n < 0 || k < 0 || k > n then invalid_arg "Mdl.Code.comb: k should be between 0 and n included";
  log2 (Num.float_of_num (Common.comb (k,n)))

(* encoding a partition of N elements into K classes *)
let partition (class_counts : int list) : bits =
  let nb_classes, nb_elts =
    List.fold_left
      (fun (nb_classes,nb_elts) n -> (nb_classes+1, nb_elts+n))
      (0,0) class_counts in
  log2_gamma_ratio nb_elts nb_classes
  -. sum class_counts (fun n -> log2_gamma_ratio n 1)

let list_star (code : 'a -> bits) (lx : 'a list) : bits =
  let n, llx =
    List.fold_left
      (fun (n,llx) x -> (n+1, llx +. code x))
      (0,0.) lx in
  universal_int_star n +. llx
  
let list_plus (code : 'a -> bits) (lx : 'a list) : bits =
  assert (lx <> []);
  let n, llx =
    List.fold_left
      (fun (n,llx) x -> (n+1, llx +. code x))
      (0,0.) lx in
  universal_int_plus n +. llx

(* utility for prequential coding, i.e. coding based on previous seen occurrences of symbols*) 
class ['a] prequential ?(epsilon = 0.5) ?(init_occs : ('a * float) list option) (symbols : 'a list) =
  let k = List.length symbols in
  let index =  Hashtbl.create (2*k) in (* from symbols to array index *)
  let _ =
    List.fold_left
      (fun i x -> Hashtbl.add index x i; i+1)
      0 symbols in
  let index_of_symbol x =
    try Hashtbl.find index x
    with Not_found -> failwith "Mdl.Code.prequential#init: unexpected symbol" in
  let ref_occs, arr_occs_per_symbol =
    ref (float k *. epsilon), Array.make k epsilon in
  let _ =
    match init_occs with
    | None -> ()
    | Some l ->
       l
       |> List.iter
            (fun (x,x_occs) ->
              try
                let i = index_of_symbol x in
                ref_occs := !ref_occs +. x_occs;
                arr_occs_per_symbol.(i) <- arr_occs_per_symbol.(i) +. x_occs
              with _ -> ()) (* unexpected symbols are ignored *)
  in
  fun () -> (* for generating fresh encoders *)
  object
    val mutable occs = !ref_occs
    val occs_per_symbol = Array.copy arr_occs_per_symbol
    val mutable cum_dl = 0.
    method code (x : 'a) : bits =
      let i = index_of_symbol x in
      let dl = -. log2 (occs_per_symbol.(i) /. occs) in (* plugin code probability *)
      occs <- occs +. 1.;
      occs_per_symbol.(i) <- occs_per_symbol.(i) +. 1.;
      cum_dl <- cum_dl +. dl;
      dl
    method cumulated_dl = cum_dl
  end
  
  end

module Strategy =
  struct
    let check_compressive
	  ~(data : ?pred:('model * 'data * bits) -> 'model -> 'data option)
	  ~(code : 'model -> 'data -> bits)
          ~(pred : 'model * 'data * bits)
	  (m1 : 'model)
	: ('model * 'data * bits) option =
      match data ~pred m1 with
      | None -> None (* failure to interpret data with model *)
      | Some d1 ->
         let _, _, l0 = pred in
	 let l1 = code m1 d1 in
	 if l1 < l0
	 then Some (m1,d1,l1)
	 else None
	     
    let greedy ~(m0 : 'model)
	       ~(data : ?pred:('model * 'data * bits) -> 'model -> 'data option)
	       ~(code : 'model -> 'data -> bits)
	       ~(refinements : 'model -> 'data -> bits -> 'model Myseq.t)
	: 'model * 'data * bits =
      let rec aux m d l =
	match
	  refinements m d l
	  |> Myseq.find_map (check_compressive ~data ~code ~pred:(m,d,l))
	with
	| None -> m, d, l
	| Some ((m1,d1,l1), _next) -> aux m1 d1 l1
      in
      match data m0 with
      | Some d0 ->
	 let l0 = code m0 d0 in
	 aux m0 d0 l0
      | None -> failwith "Mdl.Strategy.greedy: bad m0, cannot parse data"

    let beam ~(timeout : int) (* maximum allowed time (s) *)
	     ~(beam_width : int) (* how many candidates retained at each generation *)
	     ~(refine_degree : int) (* how many compressive refinements considered *)
	     ~(m0 : 'model) (* initial model *)
	     ~(data : ?pred:('model * 'data * bits) -> 'model -> 'data option) (* data according to model *)
	     ~(code : 'model -> 'data -> bits) (* description length *)
	     ~(refinements : 'model -> 'data -> bits -> 'model Myseq.t) (* model refinements, heuristically ordered *)
	: ('model * 'data * bits) list * bool =
      let rec add_compressive_refinements ~pred k res refs =
	if k = 0
	then res
	else
	  match
	    refs
	    |> Myseq.find_map (check_compressive ~data ~code ~pred)
	  with
	  | None -> res
	  | Some (mdl1, next_refs) ->
	     add_compressive_refinements
	       ~pred (k-1) (mdl1::res) next_refs in
      let lm : ('model * 'data * bits) list ref =
	match data m0 with
	| Some d0 ->
	   let l0 = code m0 d0 in
	   ref [m0, d0, l0]
	| None -> failwith "Mdl.Strategy.greedy: bad m0, cannot parse data" in
      let rec loop () =
	let rev_lm1 =
	  List.fold_left
	    (fun res (m,d,l) ->
	     add_compressive_refinements
	       ~pred:(m,d,l) refine_degree res
	       (refinements m d l))
	    [] !lm in
	if rev_lm1 = [] (* no found improvement *)
	then ()
	else
	  let lm1 = (* sorting by increasing description length *)
	    List.stable_sort
	      (fun (m1,d1,l1) (m2,d2,l2) -> Stdlib.compare l1 l2)
	      (List.rev rev_lm1) in
	  let lm1 = (* at most 'beam_width' models *)
	    Common.sub_list lm1 0 beam_width in
	  lm := lm1;
	  loop ()
      in
      let res_opt = Common.do_timeout timeout loop in
      !lm, res_opt = None (* timeout reached *)
  end
