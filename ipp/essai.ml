
type t = Atom of string | Literal of string | Not of t | And of t list | Or of t * t

let rec print = ipp
  [ Atom s -> 's
  | Literal s -> "\""; ' String.escaped s; "\""
  | Not f -> "not "; print of f
  | And [] -> "true"
  | And l -> "("; LIST1 print SEP " and " of l; ")"
(*  | And (f1,f2) -> "("; print of f1; " and "; print of f2; ")" *)
  | Or (f1,f2) -> "("; print of f1; " or "; print of f2; ")"
  ]
(*
and print_and = ipp
    [ [] -> 
    | x::l -> " and "; print of x; print_and of l ]
*)

let do_print f =
  Ipp.once (ipp [f -> print of f; EOF ]) f (Printer.cursor_of_formatter Format.std_formatter) ();
  Format.pp_print_newline Format.std_formatter ()

let _ =
  do_print (Atom "x");
  do_print (Literal "toto");
  do_print (And [Atom "x"; Not (Literal "toto"); Or (Literal "a", Literal "b")])
