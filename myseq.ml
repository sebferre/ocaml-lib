(**************************************************************************)
(*                                                                        *)
(*                                 OCaml                                  *)
(*                                                                        *)
(*                 Simon Cruanes                                          *)
(*                                                                        *)
(*   Copyright 2017 Institut National de Recherche en Informatique et     *)
(*     en Automatique.                                                    *)
(*                                                                        *)
(*   All rights reserved.  This file is distributed under the terms of    *)
(*   the GNU Lesser General Public License version 2.1, with the          *)
(*   special exception on linking described in the file LICENSE.          *)
(*                                                                        *)
(**************************************************************************)

(* Module [Seq]: functional iterators *)

type +'a node =
  | Nil
  | Cons of 'a * 'a t

and 'a t = unit -> 'a node

class ['a] iterator (seq : 'a t) =
(* turns a sequence into a stateful iterator *)
object
  (* TODO: record when empty to optimize repeating pops when empty *)
  val mutable s = seq
  method pop : 'a option =
    match s () with
    | Nil -> None
    | Cons (x,next) -> s <- next; Some x
end

let rec memoize (s : 'a t) : 'a t =
  (* for computationally costly sequences that are traversed multiple times *)
  let mem =
    lazy (match s () with
	  | Nil -> Nil
	  | Cons (x,next) -> Cons (x, memoize next)) in
  fun () -> Lazy.force mem

          
let empty () = Nil

let return x () = Cons (x, empty)

let rec const x () = Cons (x, const x)
                
let effect f () = f (); Nil (* empty sequence with effect upon access *)

let rec counter i () = Cons (i, counter (i+1))
                  
let rec map f seq () = match seq() with
  | Nil -> Nil
  | Cons (x, next) -> Cons (f x, map f next)

let rec mapi f seq () =
  let rec aux f i seq () =
    match seq () with
    | Nil -> Nil
    | Cons (x, next) -> Cons (f i x, aux f (i+1) next)
  in
  aux f 0 seq

let rec filter_map f seq () = match seq() with
  | Nil -> Nil
  | Cons (x, next) ->
      match f x with
        | None -> filter_map f next ()
        | Some y -> Cons (y, filter_map f next)

let rec filter f seq () = match seq() with
  | Nil -> Nil
  | Cons (x, next) ->
      if f x
      then Cons (x, filter f next)
      else filter f next ()

let rec flat_map f seq () = match seq () with
  | Nil -> Nil
  | Cons (x, next) ->
    flat_map_app f (f x) next ()

(* this is [append seq (flat_map f tail)] *)
and flat_map_app f seq tail () = match seq () with
  | Nil -> flat_map f tail ()
  | Cons (x, next) ->
    Cons (x, flat_map_app f next tail)

let fold_left f acc seq =
  let rec aux f acc seq = match seq () with
    | Nil -> acc
    | Cons (x, next) ->
        let acc = f acc x in
        aux f acc next
  in
  aux f acc seq

let iter f seq =
  let rec aux seq = match seq () with
    | Nil -> ()
    | Cons (x, next) ->
        f x;
        aux next
  in
  aux seq

(* my own additions *)
      
let cons x seq () = Cons (x, seq)

let rec const x = fun () -> Cons (x, const x)
                  
let from_bool (b : bool) : 'a t =
  (fun () ->
    if b
    then Cons ((), empty)
    else Nil)
  
let from_option (x_opt : 'a option) : 'a t =
  (fun () ->
    match x_opt with
    | None -> Nil
    | Some x -> Cons (x, empty))
                  
let from_result (x_opt : ('a,'e) Result.t) : 'a t =
  (fun () ->
    match x_opt with
    | Result.Error _ -> Nil
    | Result.Ok x -> Cons (x, empty))
                  
let rec from_list (l : 'a list) : 'a t =
  (fun () ->
   match l with
   | [] -> Nil
   | x::r -> Cons (x, from_list r))

let rec from_stream (str : 'a Stream.t) : 'a t =
  let def_x_seq1 = lazy (Stream.next str, from_stream str) in
  (* using a lazy value so that computations on the stream are done once *)
  (fun () ->
   try
     let x, seq1 = Lazy.force def_x_seq1 in
     Cons (x, seq1)
   with Stream.Failure -> Nil)

let rec from_seq (seq : 'a Stdlib.Seq.t) : 'a t =
  (fun () ->
    match seq () with
    | Stdlib.Seq.Nil -> Nil
    | Stdlib.Seq.Cons (x,next) -> Cons (x, from_seq next))

let to_rev_list (s : 'a t) : 'a list =
  let rec aux s acc = (* to make it tail-recursive *)
    match s () with
    | Nil -> acc
    | Cons (x,next) -> aux next (x::acc) in
  aux s []

let to_list (s : 'a t) : 'a list =
  List.rev (to_rev_list s)

let rec to_stream (s : 'a t) : 'a Stream.t =
  match s () with
  | Nil -> [< >]
  | Cons (x,next) -> [< 'x; to_stream next >]
	   
let hd_opt (s : 'a t) : 'a option =
  match s () with
  | Nil -> None
  | Cons (x,next) -> Some x

let lift (s : 'a t) : 'a t t =
  s |> map (fun x -> return x)

let flatten (ss : 'a t t) : 'a t =
  ss |> flat_map (fun s -> s)
             
let rec append (s1 : 'a t) (s2 : 'a t) : 'a t =
  (fun () ->
    match s1 () with
    | Nil -> s2 ()
    | Cons (x,next) -> Cons (x, append next s2))

let append_if_not_empty s1 s2 =
  (fun () ->
    match s1 () with
    | Nil -> Nil
    | Cons (x,next) -> Cons (x, append next s2))

let if_not_empty_else s1 s2 =
  (fun () ->
    match s1 () with
    | Nil -> s2 ()
    | Cons (x1,next1) -> Cons (x1,next1))
  
let rec concat (ls : 'a t list) : 'a t =
  match ls with
  | [] -> empty
  | s::r -> concat_app s r
and concat_app s ls =
  (fun () ->
   match s () with
   | Nil -> concat ls ()
   | Cons (x,next) -> Cons (x, concat_app next ls))

let rec from_rev_list (l : 'a list) : 'a t =
  (fun () ->
    match l with
    | [] -> Nil
    | x::r -> append (from_rev_list r) (return x) ())

let rec from_bintree (s : 'a Bintree.t) : 'a t =
  (fun () ->
    match s with
    | Empty -> Nil
    | Node (l, v, r, _) -> append (from_bintree l) (cons v (from_bintree r)) ())
  
let concat_rev_list_seq (revl : 'a list) (seq : 'a t) : 'a t =
  (* generates elements in revl in reverse, then elements from seq *)
  (fun () ->
    List.fold_left
      (fun seq x -> cons x seq)
      seq revl
      ())

let rec interleave (ls : 'a t list) : 'a t =
  match ls with
  | [] -> empty
  | [s] -> s
  | _ -> interleave_aux [] ls
and interleave_aux ll s_lr =
  (fun () ->
    match s_lr with
    | [] -> (* finished turn *)
       if ll = []
       then Nil
       else interleave_aux [] (List.rev ll) ()
    | s::lr ->
       match s () with
       | Nil -> interleave_aux ll lr ()
       | Cons (x,next) -> Cons (x, interleave_aux (next::ll) lr))
  
let rec range a b =
  if a > b
  then empty
  else (fun () -> Cons (a, range (a+1) b))

let rec downward_range a b =
  if a < b
  then empty
  else (fun () -> Cons (a, range (a-1) b))

let is_empty (s : 'a t) : bool =
  s () = Nil

let case ~(nil: unit -> 'b) ~(cons: 'a -> 'a t -> 'b) (s : 'a t) : 'b =
  match s () with
  | Nil -> nil ()
  | Cons (x,next) -> cons x next
			  
let rec length (s : 'a t) : int =
  let rec aux s acc =
    match s () with
    | Nil -> acc
    | Cons (x,next) -> aux next (acc+1) in
  aux s 0

let rec bind (s : 'a t) (f : 'a -> 'b t) : 'b t =
  fun () ->
  match s () with
  | Nil -> Nil
  | Cons (x,next) -> concat [f x; bind next f] ()

let rec bind_interleave_at_most (k : int) (s : 'a t) (f : 'a -> 'b t) : 'b t =
  (* binds at most k 'a elements for which [f x] is not empty *)
  fun () ->
  if k > 0
  then
    match s () with
    | Nil -> Nil
    | Cons (x,next) ->
       match f x () with
       | Nil -> bind_interleave_at_most k next f ()
       | Cons (x2,next2) -> Cons (x2, interleave [bind_interleave_at_most (k-1) next f; next2])
  else Nil
                   
let rec find_map (f : 'a -> 'b option) (s : 'a t) : ('b * 'a t) option =
  match s () with
  | Nil -> None
  | Cons (x,next) ->
     match f x with
     | Some y -> Some (y,next)
     | None -> find_map f next

let failsafe (s : 'a t) : 'a option t =
  fun () ->
  match s () with
  | Nil -> Cons (None, empty)
  | Cons (x,next) -> Cons (Some x, map (fun x -> Some x) next)
             
let rec product (lseq : 'a t list) : 'a list t =
  match lseq with
  | [] -> return []
  | s::ls ->
     s
     |> flat_map
          (fun x ->
            product ls
            |> map (fun lx -> x::lx))
             
let rec star_dependent_max (depseqs : ('state -> ('a * 'state) t) t) : 'state -> ('a list * 'state) t =
  (* generates lists [x0; ...; xN] and state[N+1], given state0 and a sequence of dependent sequences.
     x0, state1 = depseq0(state0), x1, state2 = depseq1(state1), ...
     Returns longest matching lists. *)
  match depseqs () with
  | Nil ->
     (fun state -> return ([], state))
  | Cons (p,next) ->
     (fun state ->
       if_not_empty_else
         (bind
            (p state)
            (fun (x,state) ->
              let pl = star_dependent_max next in
              bind
                (pl state)
                (fun (l,state) ->
                  return (x::l, state))))
         (return ([],state)))

let pair_dependent_fair ?max_relaxation_level
      (depseq1 : ('state -> ('a * 'state) t)) (depseq2 : ('state -> ('b * 'state) t))
    : 'state -> (('a * 'b) * 'state) t =
  (* generates lists (x1,x2) and state2, given state0 and 2 dependent sequences.
     x1, state1 = depseq1(state0), x2, state2 = depseq2(state1), ...
     Pairs (solutions) are generated in fair order, by increasing relaxation level.
     The relaxation level k = k1+k2, where kI is 0 for the first element of depseqI, 1 for the second, etc. *)
  (* generation of solutions generated by depseqs, starting at relaxation level [k] *)
  let aux1 all_empty revl2_iterators iter1 =
    concat_rev_list_seq
      !revl2_iterators
      (return iter1
       |> filter_map (fun iter1 ->
              match iter1#pop with
              | None -> None
              | Some (x1,state1) ->
                 let new_iter2 =
                   new iterator
                     (depseq2 state1
                      |> map (fun (x2,state2) -> (x1,x2), state2)) in
                 revl2_iterators := new_iter2 :: !revl2_iterators;
                 all_empty := false;
                 Some new_iter2))
    |> filter_map (fun iter2 ->
           match iter2#pop with (* QUICK *)
           | None -> None
           | Some res ->
              all_empty := false;
              Some res) in
  let rec aux2_k k revl2_iterators (iter1 : ('a * 'state) iterator) : (('a * 'b) * 'state) t = (* iteration on relaxation level [k] *)
    let all_empty = ref true in (* to know if there was any iterator or solution generated for this [k] *)
    append
      (aux1 all_empty revl2_iterators iter1)
      (fun () ->
        if !all_empty (* STOP when nothing more to generate *)
           || (match max_relaxation_level with
               | None -> false
               | Some kmax -> k >= kmax) (* max relaxation level reached *)
        then Nil
        else aux2_k (k+1) revl2_iterators iter1 ())
  in
  fun state ->
  let iter1 = (* iterator for the first depseq *)
    new iterator (depseq1 state) in
  let revl2_iterators = ref [] in (* iterators for depseq2 *)
  aux2_k 0 revl2_iterators iter1

(* recursion through depseqs and their iterators *)
let (* private *) level_dependent_fair (* technical aux function for X_dependent_fair functions *)
      (k : int) (all_empty : bool ref)
      (revl_depseq_iterators : (('state -> ('a * 'state) t) * ('a list * 'state) iterator list ref) list)
      (iter0 : ('a list * 'state) iterator)
    : ('a list * 'state) t (* sets all_empty to false if there remains something to generate *) =
  let rec aux revl : ('a list * 'state) iterator t =
    match revl with
    | [] -> return iter0
    | (depseq, iterators)::revl1 ->
       concat_rev_list_seq
         !iterators
         (aux revl1
          |> filter_map (fun iter1 ->
                 match iter1#pop with
                 | None -> None
                 | Some (rev_lx1,state1) ->
                    let new_iter =
                      new iterator
                        (depseq state1
                         |> map (fun (x2,state2) ->
                                x2::rev_lx1, state2)) in
                    iterators := new_iter :: !iterators;
                    all_empty := false;
                    Some new_iter))
  in
  aux revl_depseq_iterators
  |> filter_map (fun iter ->
         match iter#pop with (* QUICK *)
         | None -> None
         | Some (rev_lx, state) ->
            let lx = List.rev rev_lx in
            all_empty := false;
            Some (lx, state))

let product_dependent_fair ?max_relaxation_level (depseqs : ('state -> ('a * 'state) t) list) : 'state -> ('a list * 'state) t =
  (* generates lists [x0; ...; xN] and state[N+1], given state0 and N dependent sequences.
     x0, state1 = depseq0(state0), x1, state2 = depseq1(state1), ...
     Lists (solutions) are generated in fair order, by increasing relaxation level.
     The relaxation level k = k0+k1+k2+.., where kI is 0 for the first element of depseqI, 1 for the second, etc. *)
  (* generation of solutions generated by depseqs, starting at relaxation level [k] *)
  (* BEWARE to keep mutables states inside functions!!! *)
  let rec aux_k k revl_n (iter0 : ('a list * 'state) iterator) : ('a list * 'state) t = (* iteration on relaxation level [k] *)
    let all_empty = ref true in (* to know if there was any iterator or solution generated for this [k] *)
    append
      (level_dependent_fair k all_empty revl_n iter0)
      (fun () ->
        if !all_empty (* STOP when nothing more to generate *)
           || (match max_relaxation_level with
               | None -> false
               | Some kmax -> k >= kmax) (* max relaxation level reached *)
        then Nil
        else aux_k (k+1) revl_n iter0 ())
  in
  fun state ->
  match depseqs with
  | [] -> return ([], state)
  | depseq0::depseqs1 ->
     let revl1_depseq_iterators = (* mapping dependent seqs to a mutable list of iterators *)
       List.rev_map (fun depseq -> depseq, ref []) depseqs1 in
     let iter0 = (* iterator for the first depseq *)
       new iterator
         (depseq0 state
          |> map (fun (x0,state0) -> ([x0], state0))) in
     aux_k 0 revl1_depseq_iterators iter0

let product_fair ?max_relaxation_level (seqs : 'a t list) : 'a list t = (* TO BE tested *)
  product_dependent_fair
    ?max_relaxation_level
    (List.map
       (fun seq ->
         let seq_state = memoize (seq |> map (fun x -> (x,()))) in
         (fun () -> seq_state))
       seqs)
    ()
  |> map (fun (lx,()) -> lx)

let star_dependent_fair ?max_relaxation_level (depseqs : ('state -> ('a * 'state) t) t) : 'state -> ('a list * 'state) t =
  (* generates lists [x0; ...; xN] and state[N+1], given state0 and a sequence of dependent sequences.
     x0, state1 = depseq0(state0), x1, state2 = depseq1(state1), ...
     Lists (solutions) are generated in fair order, by increasing relaxation level.
     The relaxation level k = N+k0+k1+k2+..+kN, where kI is 0 for the first element of depseqI, 1 for the second, etc. *)
  let rec aux_p k revl_powers : ('a list * 'state) t = (* iteration on power [p] *)
    (* revl_powers holds iteration state for each power, in reverse order *)
    List.fold_left
      (fun res (all_empty,revl_n,iter0) ->
        append
          (level_dependent_fair k all_empty revl_n iter0) (* small powers come first *)
          res)
      empty
      revl_powers in
  let rec aux_k k revl1_depseq seq0 next_depseqs revl_powers : ('a list * 'state) t = (* iteration on relaxation level *)
    assert (List.length revl1_depseq + 1 <= k); (* k determines maximum power *)
    assert (List.for_all (fun (all_empty,_,_) -> !all_empty) revl_powers);
    append_if_not_empty
      (aux_p k revl_powers) (* small relaxation levels come first *)
      (fun () ->
        let revl_powers =
          List.filter
            (fun (all_empty,_,_) -> not !all_empty)
            revl_powers in
        if revl_powers = []
           || (match max_relaxation_level with
               | None -> false
               | Some kmax -> k >= kmax)
        then Nil
        else
          let _ =
            List.iter (fun (all_empty,_,_) -> all_empty := true) revl_powers in
          match next_depseqs () with
          | Nil ->
             aux_k (k+1) revl1_depseq seq0 next_depseqs revl_powers ()
          | Cons (depseq1,next_depseqs1) ->
             let revl_powers1 =
               let revl_n1 =
                 List.map (* resetting iterators *)
                   (fun depseq -> (depseq, ref []))
                   (depseq1::revl1_depseq) in (* extended power *)
               (ref true, revl_n1, new iterator seq0) :: revl_powers in
             aux_k (k+1) (depseq1::revl1_depseq) seq0 next_depseqs1 revl_powers1 ())
  in
  match depseqs () with
  | Nil ->
     fun state -> return ([],state)
  | Cons (depseq0,next_depseqs) ->
     fun state ->
     let seq0 = (* solutions for the first depseq *)
       depseq0 state
       |> map (fun (x0,state0) -> ([x0], state0))
       |> memoize in (* because recomputed for each power *)
     let revl1_depseq = [] in
     let revl_powers = [(ref true, [], new iterator seq0)] in (* powers up to n=1 *)
     append
       (return ([],state)) (* for k = 0 *)
       (aux_k 1 revl1_depseq seq0 next_depseqs revl_powers) (* for k >= 1, until max_relaxation_level *)

(* let _ = (* TEST *)
  let rec depseq i state = fun () -> Cons ((i,state), depseq (i+1) state) in
  star_dependent_fair ~max_relaxation_level:16 (const (depseq 0)) ()
  |> fold_left (fun rank (lx,state) -> print_endline (string_of_int rank ^ " - " ^ String.concat " " (List.map string_of_int lx)); rank+1) 0 *)
  
let slice ?(offset : int = 0) ?(limit : int option) (s : 'a t) : 'a t =
  let rec aux offset limit s =
    (fun () ->
     if limit = 0
     then Nil
     else
       match s () with
       | Nil -> Nil
       | Cons (x,next) ->
	  if offset = 0
	  then Cons (x, aux offset (if limit<0 then limit else limit-1) next)
	  else aux (offset-1) limit next ())
  in
  aux offset (match limit with None -> -1 | Some l -> l) s

let with_position ?(init = 0) (s : 'a t) : (int * 'a) t =
  let rec aux pos s =
    fun () ->
    match s () with
    | Nil -> Nil
    | Cons (x,next) -> Cons ((pos,x), aux (pos+1) next)
  in
  aux init s
  
let rec take (n : int) (s : 'a t) : 'a list * 'a t option =
  if n = 0
  then [], (if is_empty s then None else Some s)
  else
    match s () with
    | Nil -> [], None
    | Cons (x,next) ->
       let l, r_opt = take (n-1) next in
       x::l, r_opt

let unique (s : 'a t) : 'a t =
  let rec aux seen s =
    fun () ->
    match s () with
    | Nil -> Nil
    | Cons (x,next) ->
       if Bintree.mem x seen
       then aux seen next ()
       else Cons (x, aux (Bintree.add x seen) next) in
  aux Bintree.empty s

let sort (comp : 'a -> 'a -> int) (s : 'a t) : 'a t =
  (* CAUTION: exhausts [s] *)
  let l = to_rev_list s in
  let sorted = List.sort comp l in
  from_list sorted

let sort_uniq (comp : 'a -> 'a -> int) (s : 'a t) : 'a t =
  (* CAUTION: exhausts [s] *)
  let l = to_rev_list s in
  let sorted = List.sort_uniq comp l in
  from_list sorted

let rec zip (s1 : 'a t) (s2 : 'b t) : ('a * 'b) t =
  fun () ->
  match s1 (), s2 () with
  | Nil, _ | _, Nil -> Nil
  | Cons (x1,next1), Cons (x2,next2) -> Cons ((x1,x2), zip next1 next2)
  
let rec zip_sorted (cmp : 'a -> 'a -> int) (s1 : 'a t) (s2 : 'a t) : ('a option * 'a option) t =
  (* assumes s1 and s2 are sorted according to cmp *)
  fun () ->
  match s1 (), s2 () with
  | Nil, Nil -> Nil
  | Nil, Cons (x2,next2) ->
     Cons ((None, Some x2), next2 |> map (fun x2 -> (None, Some x2)))
  | Cons (x1,next1), Nil ->
     Cons ((Some x1, None), next1 |> map (fun x1 -> (Some x1, None)))
  | (Cons (x1,next1) as n1), (Cons (x2,next2) as n2) ->
     let c = cmp x1 x2 in
     if c < 0 (* x1 < x2 *) then
       Cons ((Some x1, None), zip_sorted cmp next1 (fun () -> n2))
     else if c > 0 (* x1 > x2 *) then
       Cons ((None, Some x2), zip_sorted cmp (fun () -> n1) next2)
     else (* x1 equiv x2 *)
       Cons ((Some x1, Some x2), zip_sorted cmp next1 next2)

let rec breakable (break : bool ref) (s : 'a t) : 'a t =
  fun () ->
  if !break
  then Nil
  else
    match s () with
    | Nil -> Nil
    | Cons (x,next) -> Cons (x, breakable break next)

let rec introspect (s : 'a t) : ('a * 'a t) t =
  fun () ->
  match s () with
  | Nil -> Nil
  | Cons (x,next) -> Cons ((x,next), introspect next)
                       
let rec prof (name : string) (s : 'a t) : 'a t =
  fun () ->
  match Common.prof name s with
  | Nil -> Nil
  | Cons (x,next) -> Cons (x, prof name next)
             

