OCAMLFIND=ocamlfind
OCAMLLEX=ocamllex
OCAMLC=$(OCAMLFIND) ocamlc
OCAMLOPT=$(OCAMLFIND) ocamlopt
INCLUDES= -package yojson -I camljava/lib          # all relevant -I options here
#OCAMLFLAGS= -pp "camlp4o pa_op.cmo" -thread $(INCLUDES)    # add other options for ocamlc here
OCAMLFLAGS= -pp camlp5o -thread $(INCLUDES)    # add other options for ocamlc here

.SUFFIXES: .mll .ml .mli .cmo .cmi

# Make all
all: bintree.cmo common.cmo threads_common.cmo xprint.cmo myseq.cmo mymap.cmo unicode.cmo mdl.cmo range.cmo lSet.cmo cis.cmi cis.cmo iterator.cmo intmap.cmo intset.cmo intrel.cmo intrel2.cmo intreln.cmo kgraph.cmo text_index.cmo setset.cmo cache.cmi cache.cmo ext.cmo suffix_tree.cmo index.cmo seq.cmo find_merge.cmo
	echo

# the following modules depend on dbm, hence NDBM
# persindex.cmo genid.cmo persintset.cmo persintset.cmo stringset.cmo seqset.cmo

all-java: all jni_common.cmo
	echo

# archiving
lib.tgz:
	tar cfz lib.tgz make* *,v *.mll *.ml *.mli */config* */make* */Make* */*.c */*.h */*.ml */*.mli  

# Common rules

%.ml: %.mll
	$(OCAMLLEX) $<

%.cmo: %.ml
	$(OCAMLC) $(OCAMLFLAGS) -c $<
	$(OCAMLOPT) $(OCAMLFLAGS) -c $<

%.cmi: %.mli
	$(OCAMLC) $(OCAMLFLAGS) -c $<


# Clean up
clean:
	rm -f *.cm[ioax]
