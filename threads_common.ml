(** Common definitions for threads. *)

(** {2 Mutex on global data structures} *)

    let m = Mutex.create ()

    let owner = ref None

    let mutex f =
      match !owner with
      | Some id when id = Thread.self () -> f ()
      |	_ ->
	  Mutex.lock m;
	  owner := Some (Thread.self ());
	  let res =
            try f ()
	    with e -> owner := None; Mutex.unlock m; raise e in
	  owner := None;
	  Mutex.unlock m;
	  res
