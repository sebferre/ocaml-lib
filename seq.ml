
module type T =
  sig
    type elt
    type t
    val eos : elt
    val get : t -> int -> elt
    val length : t -> int
    val empty : t
    val equal : t -> t -> bool
    val sub : t -> int -> int -> t
    val append : t -> t -> t
    val of_string_eos : string -> t (* the result sequence is ended by eos *)
    val of_string : string -> t
    val to_string : t -> string
    val compare : t -> t -> int
  end

module Char : T =
  struct
    type elt = char
    type t = string
    let eos = '\000'
    let get = String.get
    let length = String.length
    let empty = ""
    let equal = (=)
    let sub = String.sub
    let append = (^)
    let of_string_eos x = x ^ String.make 1 eos
    let of_string x = x
    let to_string x = x
    let compare = Stdlib.compare
  end

module Word =
  struct
    type elt = string
    type t = string array

    let eos = ""

    let get = Array.get

    let length = Array.length

    let empty = [||]

    let equal = (=)

    let sub = Array.sub

    let append = Array.append

    let of_string_aux add_eos x =
      let l = Str.split (Str.regexp "[ ,;:.!?]+") x in
      let n = List.length l in
      let a = Array.make (if add_eos then n + 1 else n) eos in
      ignore (
      List.fold_left
	(fun i x -> a.(i) <- x; i+1)
	0 l);
      a

    let of_string_eos = of_string_aux true
    let of_string = of_string_aux false

    let to_string a =
      let n = Array.length a in
      let n = if n <> 0 && a.(n-1) = eos then n-1 else n in
      if n = 0
      then ""
      else begin
	let buf = Buffer.create (10 * n) in
	for i = 0 to n-2 do
	  Buffer.add_string buf a.(i);
	  Buffer.add_char buf ' '
	done;
	Buffer.add_string buf a.(n-1);
	Buffer.contents buf
      end

    let compare = Stdlib.compare
  end    
