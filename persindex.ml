
module Serialize =
  struct
    class type ['a] t =
      object
	method to_string : 'a -> string
	method from_string : string -> 'a
      end

    let marshal =
      object
	method to_string x = Marshal.to_string x []
	method from_string s = Marshal.from_string s 0
      end

    let unit : unit t =
      object
	method to_string () = ""
	method from_string s = ()
      end

    let int : int t =
      object
	method to_string = string_of_int
	method from_string = int_of_string
      end

    let string : string t =
      object
	method to_string s = s
	method from_string s = s
      end

    let index (create : unit -> ('a,'b) Index.index) : ('a,'b) Index.index t =
      object
	method to_string idx = idx # to_string
	method from_string s =
	  let idx = create () in
	  idx # from_string s;
	  idx
      end

(*
    let obj =
      object
	method to_string x = (x # to_string : string)
	method from_string x (s : string) = x # from_string s; x
      end
*)
  end


(** Prototype of databases. *)
class database =
  object (self)
    method locate : string -> string -> unit =
      fun table field -> ()
	  (** [locate t f] makes sure the database contains a table [t] with field [f]. *)
    method get : string -> string -> string -> string =
      fun table field key -> raise Not_found
	  (** [get t f k] returns the field [f] associated to key [k] in table [t]. Raises [Not_found] if there is no such value. *)
    method set : string -> string -> string -> string -> unit =
      fun table field key data -> ()
	  (** [set t f k d] sets the field [f] associated to key [k] in table [t] as data [d]. *)
    method reset : string -> string -> string -> unit =
      fun table field key -> ()
	  (** [reset t f k] removes the field [f] associated to key [k] in table [t]. *)
    method close : unit = ()
	(** [close] closes the database. *)
  end

(** Implementation of a database with GDBM. *)
class dbm (file : string) =
  let string_of_key (t,f,k) = String.concat "-" [t; f; k] in
  object (self)
    inherit database
	
    val db = Dbm.opendbm file [Dbm.Dbm_create; Dbm.Dbm_rdwr] 0o664

    method get t f k = Dbm.find db (string_of_key (t,f,k))
	
    method set t f k v = Dbm.replace db (string_of_key (t,f,k)) v
	
    method reset t f k = try Dbm.remove db (string_of_key (t,f,k)) with _ -> ()
	
    method close = Dbm.close db
  end

(** Persistent version of an index (in memory) w.r.t. some database.
    The index then plays the role of a cache for the database. *)
class ['a,'b] index ?(keys : 'a Serialize.t = Serialize.marshal) ?(vals : 'b Serialize.t = Serialize.marshal) (idx : ('a,'b) Index.index) (init : 'a -> 'b) (db : database) =
  object (self)
    val mutable table = ""
    val mutable field = ""
    val mutable modified : 'a Bintree.t = Bintree.empty

    method locate : string -> string -> string -> unit =
      (** [locate d s n] locates this index as the field of a table of the database.
	 - [d] specifies the domain of keys, hence the table
	 - [n] specifies the name of the index, under some scope [s], which compose the field *)
      fun domain scope name ->
	table <- domain;
	field <- String.concat ":" [scope; name];
	db # locate table field

    method sync : unit =
      (** [sync] forces the synchronization of the database w.r.t. the cache. *)
      assert (not (List.mem "" [table; field]));
      Bintree.iter
	(fun k ->
	  try
	    let v = idx # get k in
	    db # set table field (keys # to_string k) (vals # to_string v)
	  with Not_found ->
	    db # reset table field (keys # to_string k))
	modified;
      modified <- Bintree.empty

    method unload : int -> unit =
      (** [unload p] randomly unloads p% of index entries from memory into the database. *)
      fun p ->
	idx # iter
	  (fun k v ->
	    if Random.int 100 < p then begin
	      db # set table field (keys # to_string k) (vals # to_string v);
	      idx # reset k;
	      modified <- Bintree.remove k modified
	    end)

    method get : 'a -> 'b =
      (** [get k] retrieves the value associated to the key [k].
	 Firstly in the cache, then in the database.
	 In the latter case, the value is added into the cache. *)
      fun k ->
	try idx # get k
	with Not_found ->
	  let v =
	    try vals # from_string (db # get table field (keys # to_string k))
	    with Not_found -> init k in
	  idx # set k v;
	  modified <- Bintree.add k modified;
	  v

    method set : 'a -> 'b -> unit =
      (** [set k v] associates the value [v] to the key [k].
	  Accesses to the database are delayed until [sync] or [unload] is called. *)
      fun k v ->
	idx # set k v;
	modified <- Bintree.add k modified

    method update : 'a -> ('b -> 'b) -> unit =
      (** [update k f] updates the current value of [k] by applying the function [f].
	  Assumes the current value already exists.
          Write accesses to the database are delayed. *)
      fun k f ->
	let v =
	  try idx # get k
	  with Not_found ->
	    try vals # from_string (db # get table field (keys # to_string k))
	    with Not_found -> init k in
	idx # set k (f v);
	modified <- Bintree.add k modified
(*
	let v' = f (self # get k) in
	self # set k v'
*)

    method reset : 'a -> unit =
      (** [reset k] removes the entry associated to the key [k], if it exists.
	  Accesses to the database are delayed. *)
      fun k ->
	idx # reset k;
	modified <- Bintree.add k modified
  end

let init_fail = fun _ -> raise Not_found

class ['a,'b] hashtbl ?keys ?vals (size : int) (init : 'a -> 'b) = ['a,'b] index ?keys ?vals (new Index.hashtbl size init_fail) init

class ['a] vector ?vals (size : int) (null : 'a) (init : int -> 'a) = [int,'a] index ~keys:Serialize.int ?vals (new Index.vector size null init_fail) init

class ['a] vector_opt ?vals (size : int) (init : int -> 'a) = [int,'a] index ~keys:Serialize.int ?vals (new Index.vector_opt size init_fail) init

class ['a] var ?vals (null : 'a) (init : unit -> 'a) = [unit,'a] index ~keys:Serialize.unit ?vals (new Index.var null init_fail) init

class ['a] var_opt ?vals (init : unit -> 'a) = [unit,'a] index ~keys:Serialize.unit ?vals (new Index.var_opt init_fail) init

class ['a] varray ?vals (size : int) (null : 'a) (init : int -> 'a) = [int,'a] index ~keys:Serialize.int ?vals (new Index.varray size null init_fail) init

class ['a] varray_opt ?vals (size : int) (init : int -> 'a) = [int,'a] index ~keys:Serialize.int ?vals (new Index.varray_opt size init_fail) init


class ['a,'b] index_atom (idx0 : ('a,'b) Index.index) (db : database) =
  let key = "" in
  object (self)
    val mutable table = ""
    val mutable field = ""
    val mutable idx : ('a,'b) Index.index = idx0

    method locate : string -> string -> string -> unit =
      fun domain scope name ->
	table <- scope;
	field <- name;
	try idx # from_string (db # get table field key)
	with Not_found -> db # locate table field

    method sync : unit =
      assert (not (List.mem "" [table; field]));
      db # set table field key (idx # to_string)

    method unload : int -> unit =
      fun p -> ()

    method get = idx # get

    method set = idx # set

    method update = idx # update

    method reset = idx # reset
  end

class ['a,'b] hashtbl_atom (size : int) (init : 'a -> 'b) = ['a,'b] index_atom (new Index.hashtbl size init)

class ['a] vector_atom (size : int) (null : 'a) (init : int -> 'a) = [int,'a] index_atom (new Index.vector size null init)

class ['a] vector_opt_atom (size : int) (init : int -> 'a) = [int,'a] index_atom (new Index.vector_opt size init)

class ['a] var_atom (null : 'a) (init : unit -> 'a) = [unit,'a] index_atom (new Index.var null init)

class ['a] var_opt_atom (init : unit -> 'a) = [unit,'a] index_atom (new Index.var_opt init)

class ['a] varray_atom (size : int) (null : 'a) (init : int -> 'a) = [int,'a] index_atom (new Index.varray size null init)

class ['a] varray_opt_atom (size : int) (init : int -> 'a) = [int,'a] index_atom (new Index.varray_opt size init)


class ['a,'a1,'a2,'b] index2
    ?(keys : 'a1 Serialize.t = Serialize.marshal)
    ?(vals : 'b Serialize.t = Serialize.marshal) (* for compatiblity with other classes *)
    (key12 : 'a -> 'a1 * 'a2)
    (idx : ('a1, ('a2,'b) Index.index) Index.index)
    (init : 'a1 -> ('a2,'b) Index.index)
    (db : database) : ['a,'b] index =
  object (self)
    val mutable table = ""
    val mutable field = ""
    val mutable modified : 'a1 Bintree.t = Bintree.empty

    method locate : string -> string -> string -> unit =
      (** [locate d s n] locates this index as the field of a table of the database.
	 - [d] specifies the domain of keys, hence the table
	 - [n] specifies the name of the index, under some scope [s], which compose the field *)
      fun domain scope name ->
	table <- domain;
	field <- String.concat ":" [scope; name];
	db # locate table field

    method sync : unit =
      (** [sync] forces the synchronization of the database w.r.t. the cache. *)
      assert (not (List.mem "" [table; field]));
      Bintree.iter
	(fun k1 ->
	  try
	    let idx2 = idx # get k1 in
	    db # set table field (keys # to_string k1) (idx2 # to_string)
	  with Not_found ->
	    db # reset table field (keys # to_string k1))
	modified;
      modified <- Bintree.empty

    method unload : int -> unit =
      (** [unload p] randomly unloads p% of index entries from memory into the database. *)
      fun p ->
	idx # iter
	  (fun k1 idx2 ->
	    if Random.int 100 < p then begin
	      db # set table field (keys # to_string k1) (idx2 # to_string);
	      idx # reset k1;
	      modified <- Bintree.remove k1 modified
	    end)

    method private get2 : 'a1 -> ('a2,'b) Index.index =
      fun k1 ->
	try idx # get k1
	with Not_found ->
	  let idx2 = init k1 in
	  (try idx2 # from_string (db # get table field (keys # to_string k1)) with Not_found -> ());
	  idx # set k1 idx2;
	  modified <- Bintree.add k1 modified;
	  idx2

    method get : 'a -> 'b =
      (** [get k] retrieves the value associated to the key [k].
	 Firstly in the cache, then in the database.
	 In the latter case, the value is added into the cache. *)
      fun k ->
	let k1, k2 = key12 k in
	(self # get2 k1) # get k2

    method set : 'a -> 'b -> unit =
      (** [set k v] associates the value [v] to the key [k].
	  Accesses to the database are delayed until [sync] or [unload] is called. *)
      fun k v ->
	let k1, k2 = key12 k in
	(self # get2 k1) # set k2 v;
	modified <- Bintree.add k1 modified

    method update : 'a -> ('b -> 'b) -> unit =
      (** [update k f] updates the current value of [k] by applying the function [f].
	  Assumes the current value already exists.
          Write accesses to the database are delayed. *)
      fun k f ->
	let k1, k2 = key12 k in
	(self # get2 k1) # update k2 f;
	modified <- Bintree.add k1 modified
(*
	let v' = f (self # get k) in
	self # set k v'
*)

    method reset : 'a -> unit =
      (** [reset k] removes the entry associated to the key [k], if it exists.
	  Accesses to the database are delayed. *)
      fun k ->
	let k1, k2 = key12 k in
	(self # get2 k1) # reset k2;
	modified <- Bintree.add k1 modified
  end

let chunk = 2048 (* seems the best value for size2 *)

class ['a] varray_vector ?vals (size1 : int) (size2 : int) (null : 'a) (init : int -> 'a) (db : database) =
  let create () = new Index.hashtbl size2 (fun (k1,k2) -> init (k1 * size2  +  k2)) in
  let _init1 = fun k1 -> create () in
  [int,int,int,'a] index2
    ~keys:Serialize.int
    ?vals
    (fun k -> k / size2, k mod size2)
    (new Index.varray_opt size1 (fun _ -> raise Not_found))
    (fun k1 ->
      let k0 = k1 * size2 in
      new Index.vector size2 null (fun k2 -> init (k0 + k2)))
    db

class ['a] varray_vector_opt ?vals (size1 : int) (size2 : int) (init : int -> 'a) (db : database) =
  [int,int,int,'a] index2
    ~keys:Serialize.int
    ?vals
    (fun k -> k / size2, k mod size2)
    (new Index.varray_opt size1 (fun _ -> raise Not_found))
    (fun k1 ->
      let k0 = k1 * size2 in
      new Index.vector_opt size2 (fun k2 -> init (k0 + k2)))
    db

(* A REVOIR

class ['a,'a1,'a2,'b] index2
    (key12 : 'a -> 'a1 * 'a2)
    (pidx : ('a1, ('a1 * 'a2,'b) Index.index) index)
    (db : database) =
  object (self)

    method locate = pidx # locate

    method sync = pidx # sync

    method unload = pidx # unload

    method get : 'a -> 'b =
      fun k ->
	let k1, k2 = key12 k in
	(pidx # get k1) # get (k1,k2)

    method set : 'a -> 'b -> unit =
      fun k v ->
	let k1, k2 = key12 k in
	let v1 = pidx # get k1 in
	v1 # set (k1,k2) v;
	pidx # set k1 v1 (* to notify pidx of the change on v1 *)

    method update : 'a -> ('b -> 'b) -> unit =
      fun k f ->
	let v' = f (self # get k) in
	self # set k v'

    method reset : 'a -> unit =
      fun k ->
	let k1, k2 = key12 k in
	let v1 = pidx # get k1 in
	v1 # reset (k1,k2);
	pidx # set k1 v1 (* to notify pidx of the change on v1 *)
  end


let chunk = 2048 (* seems the best value for size2 *)

class ['a] hashtbl_hashtbl (size1 : int) (size2 : int) (null : 'a) (init : int -> 'a) (db : database) =
  let create () = new Index.hashtbl size2 (fun (k1,k2) -> init (k1 * size2  +  k2)) in
  let init1 = fun k1 -> create () in
  [int,int,int,'a] index2
    (fun k -> k / size2, k mod size2)
    (new hashtbl ~keys:Serialize.int ~vals:(Serialize.index create) (* obj *) size1 init1 db)
    db

class ['a] hashtbl_vector_opt (size1 : int) (size2 : int) (init : int -> 'a) (db : database) =
  let init1 =
    fun k1 ->
      let k0 = k1 * size2 in
      new Index.vector_opt size2 (fun k2 -> init (k0 + k2)) in
  [int,int,int,'a] index2
    (fun k -> k / size2, k mod size2)
    (new hashtbl ~keys:Serialize.int ~vals:Serialize.marshal (* obj *) size1 init1 db)
    db

 A REVOIR *)
