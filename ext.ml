type t = int * int LSet.t

let empty = 0, [] (* LSet.empty () *)

let is_empty (s,_) = s=0

let cardinal (s,_) = s

let mem nid (_,l) = LSet.mem nid l

let contains (_,l1) (_,l2) = LSet.contains l1 l2

let add nid (_,l) = let l' = LSet.add nid l in LSet.cardinal l', l'

let remove nid (_,l) = let l' = LSet.remove nid l in LSet.cardinal l', l'

let union (_,l1) (_,l2) = let l' = LSet.union l1 l2 in LSet.cardinal l', l'

let union_r exts = let l' = LSet.union_r (List.map snd exts) in LSet.cardinal l', l'

let inter (_,l1) (_,l2) = let l' = LSet.inter l1 l2 in LSet.cardinal l', l'

let inter_r exts = let l' = LSet.inter_r (List.map snd exts) in LSet.cardinal l', l'

let subtract (_,l1) (_,l2) = let l' = LSet.subtract l1 l2 in LSet.cardinal l', l'

let inter_difsym (_,l1) (_,l2) =
  LSet.fold
    (fun ((s1,l1), (s2,l2), (s3,l3)) (w,x) ->
       match w with
       | LSet.Infst -> (s1+1,LSet.add x l1), (s2,l2), (s3,l3)
       | LSet.Inboth -> (s1,l1), (s2+1,LSet.add x l2), (s3,l3)
       | LSet.Insnd -> (s1,l1), (s2,l2), (s3+1,LSet.add x l3))
    ((0,LSet.empty ()) , (0,LSet.empty ()), (0,LSet.empty ()))
    l1 l2

let iter f (_,l) = List.iter f l

let fold_left f e (_,l) = List.fold_left f e l

let fold_right f (_,l) e = List.fold_right f l e
